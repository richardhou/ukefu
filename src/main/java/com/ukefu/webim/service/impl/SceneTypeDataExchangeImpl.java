package com.ukefu.webim.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ukefu.util.extra.DataExchangeInterface;
import com.ukefu.webim.service.repository.SceneTypeRepository;
import com.ukefu.webim.web.model.SceneType;

@Service("scenetype")
public class SceneTypeDataExchangeImpl implements DataExchangeInterface{
	@Autowired
	private SceneTypeRepository sceneTypeRes ;
	
	public SceneType getDataByIdAndOrgi(String id, String orgi){
		return sceneTypeRes.findByIdAndOrgi(id, orgi) ;
	}

	@Override
	public List<SceneType> getListDataByIdAndOrgi(String id , String creater, String orgi) {
		return sceneTypeRes.findByOrgi(orgi) ;
	}
	
	public void process(Object data , String orgi) {
		
	}
}
