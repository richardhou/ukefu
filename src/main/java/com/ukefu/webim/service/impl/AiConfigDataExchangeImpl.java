package com.ukefu.webim.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ukefu.util.ai.AiUtils;
import com.ukefu.util.extra.DataExchangeInterface;
import com.ukefu.webim.web.model.AiConfig;

@Service("aiconfig")
public class AiConfigDataExchangeImpl implements DataExchangeInterface{
	
	
	public AiConfig getDataByIdAndOrgi(String id, String orgi){
		return AiUtils.initAiConfig(id , orgi);
	}

	@Override
	public List<AiConfig> getListDataByIdAndOrgi(String id , String creater, String orgi) {
		List<AiConfig> configList = new ArrayList<AiConfig>();
		configList.add(AiUtils.initAiConfig(id,orgi)) ;
		return configList;
	}
	
	public void process(Object data , String orgi) {
		
	}
}
