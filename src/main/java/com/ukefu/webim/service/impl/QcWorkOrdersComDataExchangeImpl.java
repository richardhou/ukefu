package com.ukefu.webim.service.impl;

import java.util.List;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ukefu.util.extra.QualityExchangeInterface;
import com.ukefu.webim.service.es.OrdersCommentRepository;
import com.ukefu.webim.web.model.OrdersComment;

@Service("qcworkorderscomment")
public class QcWorkOrdersComDataExchangeImpl implements QualityExchangeInterface{
	@Autowired
	private OrdersCommentRepository ordersCommentRes ;
	
	public OrdersComment getDataByIdAndOrgi(String id, String orgi){
		return null;
	}

	@Override
	public Page<?> getPageDataByOrgi(Object query, Pageable page) {
		return ordersCommentRes.findByQuery((BoolQueryBuilder) query, page);
	}

	@Override
	public List<?> getListDataByOrgi(Object query) {
		return null;
	}

	@Override
	public void process(Object data) {
		ordersCommentRes.save((OrdersComment)data);
	}

	@Override
	public void processList(Object data) {
		// TODO Auto-generated method stub
		
	}
	
	
}
