package com.ukefu.webim.service.cache;


public interface CacheInstance {
	/**
	 * 坐席状态
	 * @return
	 */
	public CacheBean getAgentStatusCacheBean() ;
	
	
	/**
	 * 服务中用户
	 * @return
	 */
	public CacheBean getAgentUserCacheBean();
	
	/**
	 * 服务中用户
	 * @return
	 */
	public CacheBean getAiUserCacheBean();
	
	
	/**
	 * 在线用户
	 * @return
	 */
	public CacheBean getOnlineCacheBean();
	
	/**
	 * 系统缓存
	 * @return
	 */
	public CacheBean getSystemCacheBean();
	
	
	/**
	 * IMR指令
	 * @return
	 */
	public CacheBean getIMRCacheBean();
	
	/**
	 * IMR指令
	 * @return
	 */
	public CacheBean getCallCenterCacheBean();
	
	/**
	 * IMR指令
	 * @return
	 */
	public CacheBean getCallCenterAgentCacheBean();
	
	/**
	 * IMR指令
	 * @return
	 */
	public CacheBean getApiUserCacheBean();
	
	/**
	 * IMR指令
	 * @return
	 */
	public CacheBean getJobCacheBean();
	
	/**
	 * 外呼
	 * @return
	 */
	public CacheBean getCallOutCacheBean();
	
	
	/**
	 * QC队列
	 * @return
	 */
	public CacheBean getQcQueueBean() ;
	
	/**
	 * ASR队列
	 * @return
	 */
	public CacheBean getAsrCacheBean() ;
	
	/**
	 * 分机信息
	 * @return
	 */
	public CacheBean getExtentionCacheBean() ;
	
	/**
	 * 分机信息
	 * @return
	 */
	public CacheBean getRobotCacheBean() ;
	
}