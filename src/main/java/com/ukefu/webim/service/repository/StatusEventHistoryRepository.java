package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventHistory;

public interface StatusEventHistoryRepository extends JpaRepository<StatusEventHistory, String> {

	public StatusEventHistory findById(String id);
	
	public List<StatusEventHistory> findByCoreuuid(String coreuuid);
	
	public StatusEventHistory findByIdOrBridgeid(String id,String bridgeid);
	
	public Page<StatusEventHistory> findByAniAndOrgi(String ani , String orgi, Pageable page) ;
	
	public List<StatusEventHistory> findByCode(String ani) ;
	
	public Page<StatusEventHistory> findByNameid(String nameid , Pageable page) ;
	
	public Page<StatusEventHistory> findByDataidAndOrgi(String dataid ,String orgi , Pageable page) ;
	
	public Page<StatusEventHistory> findByOrgi(String orgi , Pageable page) ;
	
	public Page<StatusEventHistory> findByServicestatusAndOrgi(String servicestatus ,String orgi , Pageable page) ;
	
	public Page<StatusEventHistory> findByMisscallAndOrgi(boolean misscall ,String orgi , Pageable page) ;
	
	public Page<StatusEventHistory> findByRecordAndOrgi(boolean record ,String orgi , Pageable page) ;
	
	public Page<StatusEventHistory> findByRecord(boolean record, Pageable page) ;
	
	public Page<StatusEventHistory> findByCalledAndOrgi(String voicemail ,String orgi , Pageable page) ;
	
	public Page<StatusEventHistory> findAll(Specification<StatusEventHistory> spec, Pageable pageable);  //分页按条件查询 

	public int countByAgentAndOrgi(String agent ,String orgi) ;
	
	public int countByOrgiAndAniOrCalled(String orgi ,String ani,String called);
	
	public int countByAniAndOrgi(String ani ,String orgi);
	
	public int countByCalledAndOrgi(String called ,String orgi);
	
	public int countByIdAndOrgi(String id ,String orgi);
	
	public int countById(String id);
	
	public Page<StatusEventHistory> findByRecordAndUseridAndOrgi(boolean record ,String userid,String orgi , Pageable page) ;
	
	public List<StatusEventHistory> findByTemplateidAndQualitystatusAndOrgi(String templateid,String qualitystatus ,String orgi) ;
	
	public List<StatusEventHistory> findByConferenceAndConferenceidAndOrgiAndCreaterNotAndConferenceinitiatorNot(boolean conference,String conferenceid ,String orgi , String creater , boolean includeInit) ;
	
	public StatusEventHistory findByIdAndOrgi(String id, String orgi);
	
	public List<StatusEventHistory> findAll(Specification<StatusEventHistory> spec);
	
	public List<StatusEventHistory> findByQualitydisuserAndQualitystatusAndOrgi(String qualitydisuser,String qualitystatus ,String orgi) ;
	
	public List<StatusEventHistory> findByDataidAndOrgi(String dataid ,String orgi) ;

	public Page<StatusEventHistory> findByOssstatusAndRecordAndOrgiAndRecordfileNotNull(String ossstatus ,boolean record,String orgi , Pageable page) ;
	
	public List<StatusEventHistory> findByDataidAndOrgiAndQualityresultIsNull(String dataid ,String orgi) ;
	
	public List<StatusEventHistory> findByMembersessionidAndOrgi(String membersessionid,String orgi);
}
