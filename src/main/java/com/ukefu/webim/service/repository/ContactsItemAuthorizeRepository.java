package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.ContactsItemAuthorize;

public abstract interface ContactsItemAuthorizeRepository extends JpaRepository<ContactsItemAuthorize, String>
{
  public abstract ContactsItemAuthorize findByIdAndOrgiAndDatastatus(String id, String orgi,boolean datastatus);
  
  public abstract List<ContactsItemAuthorize> findByUseridAndOrgiAndDatastatus(String userid, String orgi,boolean datastatus);
  
  public abstract List<ContactsItemAuthorize> findByItemidAndOrgiAndDatastatus(String itemid, String orgi,boolean datastatus);
  
  public abstract List<ContactsItemAuthorize> findByItemidAndOrgi(String itemid, String orgi);
  
  public abstract List<ContactsItemAuthorize> findByItemidInAndOrgi(List<String> itemid, String orgi);
  
  public abstract List<ContactsItemAuthorize> findByOrgiAndDatastatus(String orgi,boolean datastatus);
  
  public abstract ContactsItemAuthorize findByItemidAndUseridAndOrgiAndDatastatus(String itemid,String userid, String orgi,boolean datastatus);
  
  public abstract ContactsItemAuthorize findByItemidAndUseridAndOrgi(String itemid,String userid, String orgi);
  
  public abstract List<ContactsItemAuthorize> findByUseridInAndItemidAndOrgiAndDatastatus(List<String> userid,String itemid, String orgi,boolean datastatus);
}
