package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.ReportAuth;

public interface ReportAuthRepository extends JpaRepository<ReportAuth, String> {
	
	public ReportAuth findByIdAndOrgi(String id , String orgi);
	
	public List<ReportAuth> findByTypeAndOrgi(String type , String orgi);
	
	public ReportAuth findByDicidAndOrgi(String dicid , String orgi);
	
	public ReportAuth findByReportAndOrgi(String report , String orgi);
	
	public List<ReportAuth> findByDatadicidAndTypeAndOrgi(String datadicid ,String type , String orgi);
}
