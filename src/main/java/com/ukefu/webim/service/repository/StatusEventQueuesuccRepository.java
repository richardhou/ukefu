package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventQueuesucc;

public interface StatusEventQueuesuccRepository extends JpaRepository<StatusEventQueuesucc, String> {
	public StatusEventQueuesucc findById(String id);
}
