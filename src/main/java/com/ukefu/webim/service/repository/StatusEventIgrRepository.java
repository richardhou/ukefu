package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventIgr;

public interface StatusEventIgrRepository extends JpaRepository<StatusEventIgr, String> {

}
