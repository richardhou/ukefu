package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventIvr;

public interface StatusEventIvrRepository extends JpaRepository<StatusEventIvr, String> {
	public StatusEventIvr findById(String id);
	
}
