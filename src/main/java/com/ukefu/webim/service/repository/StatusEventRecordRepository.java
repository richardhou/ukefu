package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventRecord;

public interface StatusEventRecordRepository extends JpaRepository<StatusEventRecord, String> {

}
