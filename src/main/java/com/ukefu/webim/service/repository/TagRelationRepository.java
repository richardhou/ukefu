package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.TagRelation;

public abstract interface TagRelationRepository  extends JpaRepository<TagRelation, String>
{
	
	public abstract TagRelation findByUseridAndTagidAndOrgi(String userid , String tagid, String orgi);
	
	public abstract List<TagRelation> findByUseridAndOrgi(String userid, String orgi);
}

