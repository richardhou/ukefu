package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.StatusEventProcessMedia;

public interface StatusEventProcessMediaRepository extends JpaRepository<StatusEventProcessMedia, String> {
	public StatusEventProcessMedia findById(String id);
	
}
