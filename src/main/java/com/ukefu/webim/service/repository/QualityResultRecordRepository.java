package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.QualityResultRecord;

public abstract interface QualityResultRecordRepository extends JpaRepository<QualityResultRecord,String>{
	
	public abstract Page<QualityResultRecord> findByOrgi(String orgi , Pageable page) ;
	
	public abstract List<QualityResultRecord> findByOrgi(String orgi) ;
	
	public abstract QualityResultRecord findByIdAndOrgi(String id, String orgi);
	
	public abstract List<QualityResultRecord> findByDataidAndOrgi(String dataid, String orgi);
	
	public abstract List<QualityResultRecord> findByDataidAndOrgi(String dataid, String orgi,Sort sort);
	
	public abstract List<QualityResultRecord> findByOrgiAndDataidAndChecktype(String orgi,String dataid,String checktype,Sort sort) ;
	
}
