package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.StatusEventRobot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusEventRobotRepository extends JpaRepository<StatusEventRobot, String> {

}
