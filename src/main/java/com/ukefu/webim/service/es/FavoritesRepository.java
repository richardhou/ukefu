package com.ukefu.webim.service.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ukefu.webim.web.model.Favorites;

public interface FavoritesRepository extends  ElasticsearchRepository<Favorites, String> , FavoritesEsCommonRepository {

}
