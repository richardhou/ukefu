package com.ukefu.webim.service.es;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ukefu.webim.web.model.RequestLog;

public abstract interface RequestLogEsRepository  extends ElasticsearchRepository<RequestLog, String>{
	public abstract Page<RequestLog> findByOrgi(String orgi, Pageable page);
	public abstract Page<RequestLog> findByOrgiAndUsername(String orgi , String username, Pageable page);
}
