package com.ukefu.webim.service.es;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.ukefu.webim.web.model.OnlineUser;

public abstract interface OnlineSimpleCommonRepository{
	
	public abstract Page<OnlineUser> findAll(Pageable page);
}
