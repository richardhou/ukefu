package com.ukefu.webim.service.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ukefu.webim.web.model.OnlineUser;

public interface OnlineSimpleRepository extends  ElasticsearchRepository<OnlineUser, String> , OnlineSimpleCommonRepository {

}
