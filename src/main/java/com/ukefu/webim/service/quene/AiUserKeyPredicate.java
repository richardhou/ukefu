package com.ukefu.webim.service.quene;

import java.util.List;

import com.hazelcast.mapreduce.KeyPredicate;
import com.ukefu.webim.web.model.AiConfig;

@SuppressWarnings("deprecation")
public class AiUserKeyPredicate implements KeyPredicate<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<AiConfig> configs ;
	public AiUserKeyPredicate(List<AiConfig> configs) {
		this.configs = configs ;
	}
	@Override
	public boolean evaluate(String aiUser) {
		for(AiConfig config : configs) {
			System.out.println("---:"+this.getClass());
		}
		return false;
	}

}
