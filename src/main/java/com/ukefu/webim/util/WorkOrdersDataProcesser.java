package com.ukefu.webim.util;

import org.apache.commons.lang.StringUtils;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

import com.ukefu.webim.service.repository.InnerDataProcesser;
import com.ukefu.webim.service.repository.UKResultMapper;
import com.ukefu.webim.web.model.Favorites;
import com.ukefu.webim.web.model.WorkOrders;

public class WorkOrdersDataProcesser implements InnerDataProcesser{
	@Override
	public void process(UKResultMapper mapper, Object data, SearchHit hit) {
		WorkOrders workOrders = (WorkOrders) data  ;
		if(hit.getInnerHits()!=null && hit.getInnerHits().get("uk_favorites")!=null){
			SearchHits searchHit = hit.getInnerHits().get("uk_favorites") ;
			if(searchHit.getHits().length>0){
				SearchHit favHit = searchHit.getHits()[0] ;
				Favorites fav = null ;
				if (StringUtils.isNotBlank(favHit.sourceAsString())) {
					fav =mapper.mapEntity(favHit.sourceAsString() , favHit , Favorites.class);
				} else {
					fav = mapper.mapEntity(favHit.getFields().values() , favHit , Favorites.class);
				}
				workOrders.setFav(fav);
			}
		}
	}
}
