package com.ukefu.webim.util;

import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

import org.apache.commons.lang.StringUtils;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.util.server.message.NewRequestMessage;
import com.ukefu.webim.util.weixin.builder.TextBuilder;
import com.ukefu.webim.web.model.BlackEntity;
import com.ukefu.webim.web.model.Instruction;
import com.ukefu.webim.web.model.WeiXinUser;

public class UKWeiXinTools {
	public static WxMpXmlOutMessage processInstruction(Instruction ins , WeiXinUser user, WxMpXmlMessage wxMessage, WxMpService weixinService,boolean isAgent) throws Exception{
		WxMpXmlOutMessage msg = null ;
		BlackEntity black = (BlackEntity) CacheHelper.getSystemCacheBean().getCacheObject(user.getOpenid(), UKDataContext.SYSTEM_ORGI) ;
		if(ins.getAdapter() != null && ins.getAdapter().equals(UKDataContext.AdapterType.AGENT.toString()) && black == null && isAgent){
			NewRequestMessage newRequestMessage = OnlineUserUtils.newRequestMessage(null ,user.getOpenid() , user.getNickname(), user.getOrgi(), user.getOpenid(), user.getSnsid(),  user.getHeadimgurl(), user.getCountry() , user.getProvince() , user.getCity() , UKDataContext.ChannelTypeEnum.WEIXIN.toString() , !StringUtils.isBlank(ins.getInterfaceparam()) && UKDataContext.AgentInterType.SKILL.toString().equals(ins.getInterfacetype()) ? ins.getInterfaceparam() : null , !StringUtils.isBlank(ins.getInterfaceurl()) && UKDataContext.AgentInterType.AGENT.toString().equals(ins.getInterfacetype())  ? ins.getInterfaceurl() : null , UKDataContext.ChatInitiatorType.USER.toString() , null) ;
			msg = new TextBuilder().build(newRequestMessage.getMessage(), wxMessage, weixinService) ;
		}else if(ins.getAdapter() != null && ins.getAdapter().equals(UKDataContext.AdapterType.XIAOE.toString())){
	    	
			UKTools.ai(MessageUtils.createMessage(wxMessage.getContent() , 0 , null , UKDataContext.ChannelTypeEnum.WEIXIN.toString(), UKDataContext.MediaTypeEnum.TEXT.toString(),user.getOpenid() , user.getNickname(), user.getSnsid(), user.getOrgi() , null , ins.getPlugin()));
		}else if(ins.getAdapter() != null && ins.getAdapter().equals(UKDataContext.AdapterType.TEXT.toString())){
			msg = new TextBuilder().build(ins.getMemo(), wxMessage, weixinService) ;
		}else if(ins.getAdapter() != null && ins.getAdapter().equals(UKDataContext.AdapterType.INTER.toString())){
//			msg = new TextBuilder().build(ins.getMemo(), wxMessage, weixinService) ;
		}
		return msg ;
	}
}
