package com.ukefu.webim.util;

import java.util.ArrayList;
import java.util.List;

import com.ukefu.core.UKDataContext;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.ExtentionRepository;
import com.ukefu.webim.service.repository.MediaRepository;
import com.ukefu.webim.service.repository.MetadataRepository;
import com.ukefu.webim.service.repository.QueSurveyAnswerRepository;
import com.ukefu.webim.service.repository.QueSurveyProcessRepository;
import com.ukefu.webim.service.repository.QueSurveyQuestionRepository;
import com.ukefu.webim.service.repository.SalesMultianswerRepository;
import com.ukefu.webim.service.repository.SalesPatterLevelRepository;
import com.ukefu.webim.service.repository.SalesPatterMediaRepository;
import com.ukefu.webim.service.repository.SalesPatterPointRepository;
import com.ukefu.webim.service.repository.SalesPatterRepository;
import com.ukefu.webim.service.repository.StatusEventRepository;
import com.ukefu.webim.web.model.AiCallOutProcess;
import com.ukefu.webim.web.model.Extention;
import com.ukefu.webim.web.model.Media;
import com.ukefu.webim.web.model.MetadataTable;
import com.ukefu.webim.web.model.QueSurveyAnswer;
import com.ukefu.webim.web.model.QueSurveyQuestion;
import com.ukefu.webim.web.model.SalesMultianswer;
import com.ukefu.webim.web.model.SalesPatterLevel;
import com.ukefu.webim.web.model.SalesPatterMedia;
import com.ukefu.webim.web.model.SalesPatterPoint;
import com.ukefu.webim.web.model.StatusEvent;

public class RobotTools {
	/**
	 * 从缓存中获取分机数据
	 * @param extno
	 * @return
	 */
	public synchronized static Extention getExtention(String extno) {
		Extention extention = (Extention) CacheHelper.getRobotCacheBean().getCacheObject(extno, UKDataContext.SYSTEM_ORGI) ;
		if(extention == null) {
			ExtentionRepository extentionRes = UKDataContext.getContext().getBean(ExtentionRepository.class) ;
			List<Extention> extentionList = extentionRes.findByExtention(extno);
			if (extentionList != null && extentionList.size() > 0) {
				CacheHelper.getRobotCacheBean().put(extno, extention = extentionList.get(0) , UKDataContext.SYSTEM_ORGI) ;
			}
		}
		return extention ;
	}
	
	/**
	 * 从缓存中获取分机数据
	 * @param extno
	 * @return
	 */
	public static void putExtention(String extno , Extention ex) {
		if(ex!=null) {
			CacheHelper.getRobotCacheBean().put(extno, ex , UKDataContext.SYSTEM_ORGI) ;
		}else {
			CacheHelper.getRobotCacheBean().delete(extno, UKDataContext.SYSTEM_ORGI) ;
		}
	}
	
	/**
	 * 从缓存中获取分机数据
	 * @param extno
	 * @return
	 */
	public static boolean extention(String extno ) {
		return CacheHelper.getRobotCacheBean().getCacheObject(extno, UKDataContext.SYSTEM_ORGI)!=null;
	}
	
	/**
	 * 从缓存中获取分机数据
	 * @param extno
	 * @return
	 */
	public synchronized static StatusEvent getStatusEvent(String id , String orgi) {
		StatusEvent statusEvent = (StatusEvent) CacheHelper.getCallCenterCacheBean().getCacheObject(id, UKDataContext.SYSTEM_ORGI) ;
		if(statusEvent == null) {
			StatusEventRepository statusEventRes = UKDataContext.getContext().getBean(StatusEventRepository.class) ;
			statusEvent = statusEventRes.findByIdAndOrgi(id, orgi);
		}
		return statusEvent ;
	}
	
	/**
	 * 从缓存中获取
	 * @param extno
	 * @return
	 */
	public synchronized static AiCallOutProcess getProcess(String bussid, String orgi) {
		AiCallOutProcess process = (AiCallOutProcess) CacheHelper.getRobotCacheBean().getCacheObject(bussid, UKDataContext.SYSTEM_ORGI) ;
		if(process == null) {
			QueSurveyProcessRepository queSurveryRes = UKDataContext.getContext().getBean(QueSurveyProcessRepository.class) ;
			process = queSurveryRes.findByIdAndOrgi(bussid,orgi);
			if (process!=null) {
				CacheHelper.getRobotCacheBean().put(bussid, process, UKDataContext.SYSTEM_ORGI) ;
			}
		}
		return process ;
	}
	
	/**
	 * 从缓存中获取
	 * @param extno
	 * @return
	 */
	public synchronized static QueSurveyQuestion getQuestion(String orgi , String qusid) {
		QueSurveyQuestion question = (QueSurveyQuestion) CacheHelper.getRobotCacheBean().getCacheObject(qusid, UKDataContext.SYSTEM_ORGI) ;
		if(question == null) {
			QueSurveyQuestionRepository queSurveryQuesRes = UKDataContext.getContext().getBean(QueSurveyQuestionRepository.class) ;
			question = queSurveryQuesRes.findByOrgiAndId(orgi , qusid);
			if (question!=null) {
				CacheHelper.getRobotCacheBean().put(qusid, question, UKDataContext.SYSTEM_ORGI) ;
			}
		}
		return question ;
	}
	
	
	/**
	 * 从缓存中获取分机数据
	 * @param extno
	 * @return
	 */
	public static void putQuestion(String qusid , QueSurveyQuestion question) {
		if(question!=null) {
			CacheHelper.getRobotCacheBean().put(qusid, question , UKDataContext.SYSTEM_ORGI) ;
		}else {
			CacheHelper.getRobotCacheBean().delete(qusid, UKDataContext.SYSTEM_ORGI) ;
		}
	}
	
	/**
	 * 从缓存中获取分机数据
	 * @param extno
	 * @return
	 */
	public static boolean question(String qusid ) {
		return CacheHelper.getRobotCacheBean().getCacheObject(qusid, UKDataContext.SYSTEM_ORGI)!=null;
	}
	
	/**
	 * 加载公共答案，需要缓存
	 * @param processid 话术id
	 * @param orgi
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public synchronized static List<QueSurveyAnswer> loadPubAnswer(String processid ,String orgi,Boolean interrupt){
		List<QueSurveyAnswer> publicAnsList = (List<QueSurveyAnswer>) CacheHelper.getRobotCacheBean().getCacheObject(processid + UKDataContext.SYSTEM_CACHE_SPT_PUB_ANSWER_KEY, orgi) ;
		if(publicAnsList == null || publicAnsList.size() == 0){
			QueSurveyAnswerRepository queSurveryAnswerRes = UKDataContext.getContext().getBean(QueSurveyAnswerRepository.class) ;
			publicAnsList = queSurveryAnswerRes.findByOrgiAndProcessidAndAnstypeOrderBySortindexAsc(orgi,
					processid, "1");
			CacheHelper.getRobotCacheBean().put(processid + UKDataContext.SYSTEM_CACHE_SPT_PUB_ANSWER_KEY,publicAnsList, orgi) ;
		}
		List<QueSurveyAnswer> ansList = new ArrayList<>();
		if(interrupt != null && publicAnsList != null && publicAnsList.size() > 0){
			for(QueSurveyAnswer answer : publicAnsList ){
				if(interrupt == answer.isInterrupt()){
					ansList.add(answer);
				}
			}
		}else{
			return publicAnsList;
		}
		return ansList ;
	}
	
	
	/**
	 * 加载答案（需要缓存）
	 * @param questionid 问题id
	 * @param orgi
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public synchronized static List<QueSurveyAnswer> loadAnswer(String questionid ,String orgi){
		List<QueSurveyAnswer> ansList = (List<QueSurveyAnswer>) CacheHelper.getRobotCacheBean().getCacheObject(questionid + UKDataContext.SYSTEM_CACHE_SPT_ANSWER_KEY, orgi) ;
		if(ansList == null || ansList.size() == 0){
			QueSurveyAnswerRepository queSurveryAnswerRes = UKDataContext.getContext().getBean(QueSurveyAnswerRepository.class) ;
			ansList = queSurveryAnswerRes.findByOrgiAndQuestionidAndAnstypeOrderBySortindexAsc(orgi,
					questionid, "0");
			CacheHelper.getRobotCacheBean().put(questionid + UKDataContext.SYSTEM_CACHE_SPT_ANSWER_KEY,ansList, orgi) ;
		}
		return ansList ;
	}
	
	/**
	 * 加载问题，需要缓存
	 * @param orgi
	 * @param processid 话术id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public synchronized static List<QueSurveyQuestion> loadQuestion(String orgi , String processid ){
		List<QueSurveyQuestion> quesList = (List<QueSurveyQuestion>) CacheHelper.getRobotCacheBean().getCacheObject(processid + UKDataContext.SYSTEM_CACHE_SPT_QUES_KEY, orgi) ;
		if(quesList == null || quesList.size() == 0){
			QueSurveyQuestionRepository queSurveryQuesRes = UKDataContext.getContext().getBean(QueSurveyQuestionRepository.class) ;
			quesList = queSurveryQuesRes.findByOrgiAndProcessidOrderBySortindexAsc(orgi, processid);
			CacheHelper.getRobotCacheBean().put(processid + UKDataContext.SYSTEM_CACHE_SPT_QUES_KEY,quesList, orgi) ;
		}

		return quesList ;
	}
	
	/**
	 * 从缓存中获取
	 * @param extno
	 * @return
	 */
	public synchronized static MetadataTable  getMetadataTable(String metaname) {
		MetadataTable table = (MetadataTable) CacheHelper.getRobotCacheBean().getCacheObject(metaname, UKDataContext.SYSTEM_ORGI) ;
		if(table == null) {
			MetadataRepository metadataRes = UKDataContext.getContext().getBean(MetadataRepository.class) ;
			table = metadataRes.findByTablenameIgnoreCase(metaname);
			if (table!=null) {
				CacheHelper.getRobotCacheBean().put(metaname, table, UKDataContext.SYSTEM_ORGI) ;
			}
		}
		return table ;
	}
	
	/**
	 * 从缓存中获取
	 * @param extno
	 * @return
	 */
	public synchronized static AiCallOutProcess getAiProcess(String proid, String orgi) {
		AiCallOutProcess process = (AiCallOutProcess) CacheHelper.getRobotCacheBean().getCacheObject(proid, UKDataContext.SYSTEM_ORGI) ;
		if(process == null) {
			SalesPatterRepository salesPatterRes = UKDataContext.getContext().getBean(SalesPatterRepository.class) ;
			process = salesPatterRes.findByIdAndOrgi(proid,orgi);
			if (process!=null) {
				CacheHelper.getRobotCacheBean().put(proid, process, UKDataContext.SYSTEM_ORGI) ;
			}
		}
		return process ;
	}
	
	/**
	 * 从缓存中获取
	 * @param extno
	 * @return
	 */
	public static void putAiProcess(String proid , AiCallOutProcess process) {
		if(process!=null) {
			CacheHelper.getRobotCacheBean().put(proid, process , UKDataContext.SYSTEM_ORGI) ;
		}else {
			CacheHelper.getRobotCacheBean().delete(proid, UKDataContext.SYSTEM_ORGI) ;
		}
	}
	
	/**
	 * 从缓存中获取
	 * @param extno
	 * @return
	 */
	public static boolean process(String proid ) {
		return CacheHelper.getRobotCacheBean().getCacheObject(proid, UKDataContext.SYSTEM_ORGI)!=null;
	}
	
	/**
	 * 加载答案（需要缓存）
	 * @param questionid 问题id
	 * @param orgi
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public synchronized static List<SalesPatterPoint> getSalesPatterPoint(String questionid ,String orgi){
		List<SalesPatterPoint> pointList = (List<SalesPatterPoint>) CacheHelper.getRobotCacheBean().getCacheObject(questionid + UKDataContext.SYSTEM_CACHE_POINT_ANSWER_KEY, orgi) ;
		if(pointList == null || pointList.size() == 0){
			SalesPatterPointRepository salesPatterPointRes = UKDataContext.getContext().getBean(SalesPatterPointRepository.class) ;
			pointList = salesPatterPointRes.findByQuestionidAndOrgi(questionid,orgi);
			CacheHelper.getRobotCacheBean().put(questionid + UKDataContext.SYSTEM_CACHE_POINT_ANSWER_KEY,pointList, orgi) ;
		}
		return pointList ;
	}
	
	/**
	 * 加载答案（需要缓存）
	 * @param questionid 问题id
	 * @param orgi
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public synchronized static List<SalesPatterLevel> getSalesPatterLevel(String processid ,String orgi){
		List<SalesPatterLevel> levelList = (List<SalesPatterLevel>) CacheHelper.getRobotCacheBean().getCacheObject(processid + UKDataContext.SYSTEM_CACHE_LEVEL_PROCESS_KEY, orgi) ;
		if(levelList == null || levelList.size() == 0){
			SalesPatterLevelRepository salesPatterLevelRepository = UKDataContext.getContext().getBean(SalesPatterLevelRepository.class) ;
			levelList = salesPatterLevelRepository.findByProcessidAndOrgi(processid,orgi);
			CacheHelper.getRobotCacheBean().put(processid + UKDataContext.SYSTEM_CACHE_LEVEL_PROCESS_KEY,levelList, orgi) ;
		}
		return levelList ;
	}
	
	/**
	 * 从缓存中获取
	 * @param extno
	 * @return
	 */
	public synchronized static void putSalesPatterLevel(String processid , List<SalesPatterLevel> levelList) {
		if(levelList!=null && levelList.size() > 0) {
			CacheHelper.getRobotCacheBean().put(processid + UKDataContext.SYSTEM_CACHE_LEVEL_PROCESS_KEY, levelList , UKDataContext.SYSTEM_ORGI) ;
		}else {
			CacheHelper.getRobotCacheBean().delete(processid + UKDataContext.SYSTEM_CACHE_LEVEL_PROCESS_KEY, UKDataContext.SYSTEM_ORGI) ;
		}
	}
	
	/**
	 * 从缓存中获取
	 * @param extno
	 * @return
	 */
	public static boolean salesPatterLevel(String processid ) {
		return CacheHelper.getRobotCacheBean().getCacheObject(processid + UKDataContext.SYSTEM_CACHE_LEVEL_PROCESS_KEY, UKDataContext.SYSTEM_ORGI)!=null;
	}
	
	
	/**
	 * 从缓存中获取
	 * @param extno
	 * @return
	 */
	public synchronized static SalesPatterMedia getSalesMedia(String voice, String orgi) {
		SalesPatterMedia media = (SalesPatterMedia) CacheHelper.getRobotCacheBean().getCacheObject(voice+"_sales_media", UKDataContext.SYSTEM_ORGI) ;
		if(media == null) {
			SalesPatterMediaRepository salesPatterMediaRepository = UKDataContext.getContext().getBean(SalesPatterMediaRepository.class) ;
			media = salesPatterMediaRepository.findByIdAndOrgi(voice,orgi);
			if (media!=null) {
				CacheHelper.getRobotCacheBean().put(voice+"_sales_media", media, UKDataContext.SYSTEM_ORGI) ;
			}
		}
		return media ;
	}
	
	/**
	 * 从缓存中获取
	 * @param extno
	 * @return
	 */
	public synchronized static Media getMedia(String voice, String orgi) {
		Media media = (Media) CacheHelper.getRobotCacheBean().getCacheObject(voice+"_ans_media", UKDataContext.SYSTEM_ORGI) ;
		if(media == null) {
			MediaRepository mediaRepository = UKDataContext.getContext().getBean(MediaRepository.class) ;
			media = mediaRepository.findByIdAndOrgi(voice,orgi);
			if (media!=null) {
				CacheHelper.getRobotCacheBean().put(voice+"_ans_media", media, UKDataContext.SYSTEM_ORGI) ;
			}
		}
		return media ;
	}
	
	/**
	 * 加载答案（需要缓存）| 已在保存的时候更新
	 * @param questionid 问题id
	 * @param orgi
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public synchronized static List<SalesMultianswer> getMultianswer(String orgi ,String ansid){
		List<SalesMultianswer> salesMultianswerList = (List<SalesMultianswer>) CacheHelper.getRobotCacheBean().getCacheObject(ansid + UKDataContext.SYSTEM_CACHE_MULTI_ANSWER_KEY, orgi) ;
		if(salesMultianswerList == null || salesMultianswerList.size() == 0){
			SalesMultianswerRepository salesMultianswerRes = UKDataContext.getContext().getBean(SalesMultianswerRepository.class) ;
			salesMultianswerList = salesMultianswerRes.findByOrgiAndAnsweridOrderBySortindexAsc(orgi,ansid);
			CacheHelper.getRobotCacheBean().put(ansid + UKDataContext.SYSTEM_CACHE_MULTI_ANSWER_KEY,salesMultianswerList, orgi) ;
		}
		return salesMultianswerList ;
	}
}
