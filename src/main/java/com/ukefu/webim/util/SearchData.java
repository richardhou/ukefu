package com.ukefu.webim.util;

public class SearchData implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String discaller;
	private String discalled;
	private String begin;
	private String end;
	private String direction;
	private String userid;
	private String organ;
	private String ringdurbegin;
	private String ringdurend;
	private String incallbegin;
	private String incallend;
	private String record;
	private String misscall;
	private String calltype;
	private String recordbegin;
	private String recordend;
	private String quene;
	private String orgi;
	private String id;
	private String extnum;
	private String satisfaction;
	private String qualityscore;
	
	private String qualitytimebegin;
	private String qualitytimeend;
	private String qualityuser;
	private String qualitystatus;
	
	
	
	public String getDiscaller() {
		return discaller;
	}
	public void setDiscaller(String discaller) {
		this.discaller = discaller;
	}
	public String getDiscalled() {
		return discalled;
	}
	public void setDiscalled(String discalled) {
		this.discalled = discalled;
	}
	public String getBegin() {
		return begin;
	}
	public void setBegin(String begin) {
		this.begin = begin;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getOrgan() {
		return organ;
	}
	public void setOrgan(String organ) {
		this.organ = organ;
	}
	public String getRingdurbegin() {
		return ringdurbegin;
	}
	public void setRingdurbegin(String ringdurbegin) {
		this.ringdurbegin = ringdurbegin;
	}
	public String getRingdurend() {
		return ringdurend;
	}
	public void setRingdurend(String ringdurend) {
		this.ringdurend = ringdurend;
	}
	public String getIncallbegin() {
		return incallbegin;
	}
	public void setIncallbegin(String incallbegin) {
		this.incallbegin = incallbegin;
	}
	public String getIncallend() {
		return incallend;
	}
	public void setIncallend(String incallend) {
		this.incallend = incallend;
	}
	public String getRecord() {
		return record;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public String getMisscall() {
		return misscall;
	}
	public void setMisscall(String misscall) {
		this.misscall = misscall;
	}
	public String getCalltype() {
		return calltype;
	}
	public void setCalltype(String calltype) {
		this.calltype = calltype;
	}
	public String getRecordbegin() {
		return recordbegin;
	}
	public void setRecordbegin(String recordbegin) {
		this.recordbegin = recordbegin;
	}
	public String getRecordend() {
		return recordend;
	}
	public void setRecordend(String recordend) {
		this.recordend = recordend;
	}
	public String getQuene() {
		return quene;
	}
	public void setQuene(String quene) {
		this.quene = quene;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getExtnum() {
		return extnum;
	}
	public void setExtnum(String extnum) {
		this.extnum = extnum;
	}
	public String getSatisfaction() {
		return satisfaction;
	}
	public void setSatisfaction(String satisfaction) {
		this.satisfaction = satisfaction;
	}
	public String getQualityscore() {
		return qualityscore;
	}
	public void setQualityscore(String qualityscore) {
		this.qualityscore = qualityscore;
	}
	public String getQualitytimebegin() {
		return qualitytimebegin;
	}
	public void setQualitytimebegin(String qualitytimebegin) {
		this.qualitytimebegin = qualitytimebegin;
	}
	public String getQualitytimeend() {
		return qualitytimeend;
	}
	public void setQualitytimeend(String qualitytimeend) {
		this.qualitytimeend = qualitytimeend;
	}
	public String getQualityuser() {
		return qualityuser;
	}
	public void setQualityuser(String qualityuser) {
		this.qualityuser = qualityuser;
	}
	public String getQualitystatus() {
		return qualitystatus;
	}
	public void setQualitystatus(String qualitystatus) {
		this.qualitystatus = qualitystatus;
	}
	
	
}
