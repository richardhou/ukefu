package com.ukefu.webim.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ukefu.util.UKTools;
import com.ukefu.webim.web.model.SystemConfig;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.ukefu.core.UKDataContext;

public class CrossInterceptorHandler extends HandlerInterceptorAdapter {
	
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
    	response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with,accept,authorization,content-type");
        response.setHeader("Access-Control-Allow-Credentials", "true");
//        response.setHeader("X-Frame-Options", "SAMEORIGIN");
        return true ; 
    }

    public void postHandle(HttpServletRequest arg0, HttpServletResponse response, Object arg2,
            ModelAndView view) throws Exception {
    	if(view!=null){
    		view.addObject("models", UKDataContext.model) ;
            SystemConfig systemConfig = UKTools.getSystemConfig();
            if(systemConfig != null){
                view.addObject("systemConfig", systemConfig)  ;
            }else{
                view.addObject("systemConfig", new SystemConfig())  ;
            }
            Logger logger = LogManager.getLogger("com.ukefu.webim.web.handler.apps.internet.UCKeFuWeiXinController") ;
            if(logger!=null && logger.getLevel() != null){
                systemConfig.setLoglevel(logger.getLevel().toString());
            }
    	}
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
    }

}