package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author ricy Tenant.java 2010-3-17
 * 
 */
@Entity
@Table(name = "uk_tenant")
@org.hibernate.annotations.Proxy(lazy = false)
public class Tenant implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id ;
	private String datasourceid;
	private String tenantname;
	private String tenantcode;
	private boolean systemtenant ;
	private boolean inited ;		//是否已经初始化
	private boolean inites ;
	private boolean initdb ;
	private String adminuser ;		
	private String remark;//备注
	
	
	private Date lastmenutime;
	private Date lastbasetime;
	private String tenantlogo;
	private String tenantvalid;//0未认证，1邮箱认证，2手机认证
	private Date createtime  = new Date();//创建时间
	private String password ;
	private String genpasstype ;//密码生成方式
	private String sign;
	private String orgid ;	//企业ID


	private Integer agentnum;//文本坐席数量 -1 不限制
	private Integer callcenteragentnum;//呼叫中心坐席数量 -1不限制
	private Integer robotagentnum;//电销机器人数量 -1不限制

	private String models;//授权模块 ， 分割

	private boolean datastatus ;//数据状态，是否已删除 商户状态

	private String province ;//省份
	private String city ;	//城市
	private String area;//区 县
	private String addr ;	//地址
	private String mobile;//联系电话
	private String mail;//邮箱
	private String qq;//qq
	private String wechat;//微信号码
	private String aliwangwang;//阿里旺旺号码
	private String tag;//商户标签
	private Date servcieperiod;//服务有效期
	private String description;//店铺描述
	private String type;//店铺类型
	private String category;//店铺分类
	private String grouping;//店铺分组
	private Double scorestar;//评价星级
	private Double level;//店铺等级
	private String mainbusiness;//主营业务
	private String pcurl;//pc店铺地址
	private String h5url;//移动端店铺地址
	
	private Date validtime;//有效期
	private String scorestars;//评价星级
	private String levels;//店铺等级
	
	private String serviceweb;//平台服务网站
	
	private String externalid;//外部id
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDatasourceid() {
		return datasourceid;
	}
	public void setDatasourceid(String datasourceid) {
		this.datasourceid = datasourceid;
	}
	public String getTenantname() {
		return tenantname;
	}
	public void setTenantname(String tenantname) {
		this.tenantname = tenantname;
	}
	public String getTenantcode() {
		return tenantcode;
	}
	public void setTenantcode(String tenantcode) {
		this.tenantcode = tenantcode;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getLastmenutime() {
		return lastmenutime;
	}
	public void setLastmenutime(Date lastmenutime) {
		this.lastmenutime = lastmenutime;
	}
	public Date getLastbasetime() {
		return lastbasetime;
	}
	public void setLastbasetime(Date lastbasetime) {
		this.lastbasetime = lastbasetime;
	}
	public String getTenantlogo() {
		return tenantlogo;
	}
	public void setTenantlogo(String tenantlogo) {
		this.tenantlogo = tenantlogo;
	}
	public String getTenantvalid() {
		return tenantvalid;
	}
	public void setTenantvalid(String tenantvalid) {
		this.tenantvalid = tenantvalid;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public boolean isSystemtenant() {
		return systemtenant;
	}
	public void setSystemtenant(boolean systemtenant) {
		this.systemtenant = systemtenant;
	}
	public boolean isInited() {
		return inited;
	}
	public void setInited(boolean inited) {
		this.inited = inited;
	}
	public String getAdminuser() {
		return adminuser;
	}
	public void setAdminuser(String adminuser) {
		this.adminuser = adminuser;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGenpasstype() {
		return genpasstype;
	}
	public void setGenpasstype(String genpasstype) {
		this.genpasstype = genpasstype;
	}
	public boolean isInites() {
		return inites;
	}
	public void setInites(boolean inites) {
		this.inites = inites;
	}
	public boolean isInitdb() {
		return initdb;
	}
	public void setInitdb(boolean initdb) {
		this.initdb = initdb;
	}
	public String getOrgid() {
		return orgid;
	}
	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public Integer getAgentnum() {
		return agentnum;
	}

	public void setAgentnum(Integer agentnum) {
		this.agentnum = agentnum;
	}

	public Integer getCallcenteragentnum() {
		return callcenteragentnum;
	}

	public void setCallcenteragentnum(Integer callcenteragentnum) {
		this.callcenteragentnum = callcenteragentnum;
	}

	public String getModels() {
		return models;
	}

	public void setModels(String models) {
		this.models = models;
	}

	public boolean isDatastatus() {
		return datastatus;
	}

	public void setDatastatus(boolean datastatus) {
		this.datastatus = datastatus;
	}

	public Integer getRobotagentnum() {
		return robotagentnum;
	}

	public void setRobotagentnum(Integer robotagentnum) {
		this.robotagentnum = robotagentnum;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public String getAliwangwang() {
		return aliwangwang;
	}

	public void setAliwangwang(String aliwangwang) {
		this.aliwangwang = aliwangwang;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Date getServcieperiod() {
		return servcieperiod;
	}

	public void setServcieperiod(Date servcieperiod) {
		this.servcieperiod = servcieperiod;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getGrouping() {
		return grouping;
	}

	public void setGrouping(String grouping) {
		this.grouping = grouping;
	}

	public Double getScorestar() {
		return scorestar;
	}

	public void setScorestar(Double scorestar) {
		this.scorestar = scorestar;
	}

	public Double getLevel() {
		return level;
	}

	public void setLevel(Double level) {
		this.level = level;
	}

	public String getMainbusiness() {
		return mainbusiness;
	}

	public void setMainbusiness(String mainbusiness) {
		this.mainbusiness = mainbusiness;
	}

	public String getPcurl() {
		return pcurl;
	}

	public void setPcurl(String pcurl) {
		this.pcurl = pcurl;
	}

	public String getH5url() {
		return h5url;
	}

	public void setH5url(String h5url) {
		this.h5url = h5url;
	}
	public Date getValidtime() {
		return validtime;
	}
	public void setValidtime(Date validtime) {
		this.validtime = validtime;
	}
	public String getScorestars() {
		return scorestars;
	}
	public void setScorestars(String scorestars) {
		this.scorestars = scorestars;
	}
	public String getLevels() {
		return levels;
	}
	public void setLevels(String levels) {
		this.levels = levels;
	}
	public String getServiceweb() {
		return serviceweb;
	}
	public void setServiceweb(String serviceweb) {
		this.serviceweb = serviceweb;
	}
	public String getExternalid() {
		return externalid;
	}
	public void setExternalid(String externalid) {
		this.externalid = externalid;
	}
	
}
