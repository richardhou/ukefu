package com.ukefu.webim.web.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import com.ukefu.util.event.UserEvent;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventQueuesucc implements Serializable,UserEvent{


	/**
	 * 
	 */
	private static final long serialVersionUID = 3287306484943407517L;
	
	private String id ;
	private boolean queuesucc;//是否排队成功转人工坐席

	

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isQueuesucc() {
		return queuesucc;
	}
	public void setQueuesucc(boolean queuesucc) {
		this.queuesucc = queuesucc;
	}

}
