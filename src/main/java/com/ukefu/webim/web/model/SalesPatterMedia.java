package com.ukefu.webim.web.model;

import com.ukefu.util.UKTools;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * 话术媒体资源
 * @author
 */
@Entity
@Table(name = "uk_spt_media")
@org.hibernate.annotations.Proxy(lazy = false)
public class SalesPatterMedia implements java.io.Serializable{

	private static final long serialVersionUID = -8967497160296898982L;
	private String id = UKTools.getUUID();
	private String name;
	private String orgi;
	private String creater ;
	private String type;
	private Date createtime = new Date();
	private Date updatetime = new Date();
	private String filename ;
	private int filelength;
	private String content;
	
	private String bitrate ;		//码流
	private String samplingrate ;	//采样频率
	private String tracklength ;	//时长
	private String channelmode ;	//是否立体声
	private int channels ;		
	private int tracklengthtime ;
	

	private String salespatterid;

	//资源文件的bytes
	private byte[] bytes;

	@Transient
	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public int getFilelength() {
		return filelength;
	}
	public void setFilelength(int filelength) {
		this.filelength = filelength;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public String getSalespatterid() {
		return salespatterid;
	}

	public void setSalespatterid(String salespatterid) {
		this.salespatterid = salespatterid;
	}

	public String getBitrate() {
		return bitrate;
	}

	public void setBitrate(String bitrate) {
		this.bitrate = bitrate;
	}

	public String getSamplingrate() {
		return samplingrate;
	}

	public void setSamplingrate(String samplingrate) {
		this.samplingrate = samplingrate;
	}

	public String getTracklength() {
		return tracklength;
	}

	public void setTracklength(String tracklength) {
		this.tracklength = tracklength;
	}

	public String getChannelmode() {
		return channelmode;
	}

	public void setChannelmode(String channelmode) {
		this.channelmode = channelmode;
	}

	public int getChannels() {
		return channels;
	}

	public void setChannels(int channels) {
		this.channels = channels;
	}

	public int getTracklengthtime() {
		return tracklengthtime;
	}

	public void setTracklengthtime(int tracklengthtime) {
		this.tracklengthtime = tracklengthtime;
	}
}
