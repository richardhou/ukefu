package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "uk_callcenter_pbxhost")
@org.hibernate.annotations.Proxy(lazy = false)
public class PbxHost implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3932323765657445180L;
	private String id;
	private String name;
	private String hostname ;	//host name
	private String ipaddr ;		//IP
	private int port ;
	
	private int sipport = 5060;		//SIP 服务端口， 默认 5060,
	private String password ;	//pbx host password
	
	private String blacklist ;	//拦截字符串
	private String whitelist ;	//请求头中必须包含的字符串，无特殊字符串即被拦截
	
	private boolean conference ;	//启用会议功能
	private boolean initleave ;		//创建人挂断电话 关闭会议
	private String conferencenum ;	//会议默认号码
	
	private boolean connected ;
	
	private boolean callcenter ;
	private boolean hidephonenum ;//是否启用隐藏号码
	private String distype ;		//号码隐藏方式
	
	private boolean orgtrans ;		//启用挂断转满意度
	private String orgtransdest ;	//外呼转满意度号码
	private String orgtranstype ;	//转满意度号码呼叫类型

	private boolean billing ;	//强制启用计费
	
	private boolean enablesync ;	//链接建立的时候同时呼叫中心数据
	private boolean enablesyncgw ;	//链接建立的时候同时网关数据
	
	private String recordpath ;	//录音文件存储路径
	
	private String asrrecordpath ;	//ASR结果文件存储路径
	private String ttsrecordpath ;	//TTS结果文件存储路径
	private String ivrpath ;	//IVR文件路径
	private String fspath ;		//FreeSwitch安装路径: 修改用处，用于找回录音的规则
	private String device ;	//设备厂商
	
	private boolean afterprocess ;	//启用坐席后处理功能
	private boolean autoready ;	//启用坐席自动转就绪
	private String afterstatus ;	//挂机后状态
	
	private boolean loginready;		//登录自动就绪
	private boolean logoutready ;	//离线自动就绪 ， （话机有效，WebRTC无效）
	
	private String orgi;
	
	private boolean autoanswer ;
	
	private boolean sipautoanswer ;
	
	private String abscodec = "PCMA";	//默认的 呼叫编码 
	
	private String callbacktype ;	//回呼送号 号码
	private String callbacknumber ;
	
	private String creater ;
	
	private String enableai ;
	private String aiid ;
	private String sceneid ;
	
	private String welcomemsg ;		//欢迎提示语
	private String waitmsg ;		//等待提示语
	private String tipmessage ;		//识别完成提示语
	
	private boolean enablewebrtc ;	//启用WEBRTC
	private String webrtcaddress; 	//WebRTC访问地址
	private String webrtcport; 		//WebRTC端口
	private boolean webrtcssl;		//启用SSL
	
	private String webrtcring ;		//来电铃声
	private String previewring ;	//预览铃声
	
	private boolean dissipphone ;	//外呼的时候，是否隐藏SIP话机上的号码
	
	private int maxnumlength ;		//主叫/被叫号码最大长度（用于ACL拦截，超过最大长度即被拦截）
	private int minnumlength ;		//主叫/被叫号码最小长度（用于ACL拦截，超过最小长度即被拦截）
	
	private String ipregionblack ;	//发起呼叫的IP黑名单地区
	private String ipregionwhite ;	//发起护甲的IP白名单地区
	
	private boolean savekillevent ;	//保持被拦截的通话记录
	
	
	private boolean enableacl ;		//是否启用通话拦截
	private boolean enablereplace ;	//是否启用字符串替换
	private String replacereg ;		//字符替换规则
	private String replacestr ;		//替换字符
	
	private Date createtime = new Date();
	private Date updatetime = new Date();

	private String gwlua;//修改网关 的lua脚本名称

	private boolean defaultpbx;//是否默认语音服务器 （租户分配时 分配的语音服务器）
	
	private boolean bussop ;		//触发业务操作
	private String busslist ;		//触发的业务操作列表
	
	private String bussexeclist ;		//触发的业务操作列表
	
	private String igrtype ;			//性别年龄识别引擎（讯飞：xfigr）
	private boolean igr;				//性别年龄识别 - 开关
	
	private String igrappid ;			//性别年龄识别 - APPID
	private String igrapisec ;		//性别年龄识别 - APISecret
	private String igrapikey ;		//性别年龄识别 - APIKey
	private String igrhost ;			//性别年龄识别 - 接口地址 WebSocket（https://ws-api.xfyun.cn/v2/igr）
	
	private boolean savesip; //是否保存sip消息
	
	private boolean enableice;		//启用ICE
	private boolean enablestun;		//启用STUN
	private String stunserver;		//STUN Server
	private String stunserverport;	//STUN PORT
	private boolean enableturn ;	//启用TURN
	private String turnserver;		//TURN IP
	private String turnserverport;	//TURN Port
	private String turntransport;	//TURN Transport ,TCP/UDP
	private String turnusername;	//用户名
	private String turncredential;	//密码
	
	private boolean opensips; //是否开启Opensips兼容
	private String opensipsurl; //Opensips兼容访问地址
	private boolean opensipsdomain; //是否区分domain Opensips兼容-是否开启域
	private String opensipsport; //Opensips兼容访问端口  变更用处 ： opensipsport 是 invite)req_uri地址
	private boolean enablemwebrtc;//是否启用media_webrtc
	private boolean enableinviteuri;//是否启用Inviteuri
	private boolean enablerouteuri;//是否启用routeuri
	private String opensipsparam;//Opensips其他参数
	private String domainname;//Opensips兼容-域名称
	
	private boolean enablecleanqueue;//是否启用清理呼叫中心队列功能
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	@Transient
	public boolean isConnected() {
		return connected;
	}
	public void setConnected(boolean connected) {
		this.connected = connected;
	}
	public boolean isAutoanswer() {
		return autoanswer;
	}
	public void setAutoanswer(boolean autoanswer) {
		this.autoanswer = autoanswer;
	}
	public String getCallbacknumber() {
		return callbacknumber;
	}
	public void setCallbacknumber(String callbacknumber) {
		this.callbacknumber = callbacknumber;
	}
	public boolean isCallcenter() {
		return callcenter;
	}
	public void setCallcenter(boolean callcenter) {
		this.callcenter = callcenter;
	}
	public String getRecordpath() {
		return recordpath;
	}
	public void setRecordpath(String recordpath) {
		this.recordpath = recordpath;
	}
	public String getIvrpath() {
		return !StringUtils.isBlank(ivrpath) ? ivrpath : "internal";
	}
	public void setIvrpath(String ivrpath) {
		this.ivrpath = ivrpath;
	}
	public String getFspath() {
		return fspath;
	}
	public void setFspath(String fspath) {
		this.fspath = fspath;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getCallbacktype() {
		return callbacktype;
	}
	public void setCallbacktype(String callbacktype) {
		this.callbacktype = callbacktype;
	}
	public boolean isSipautoanswer() {
		return sipautoanswer;
	}
	public void setSipautoanswer(boolean sipautoanswer) {
		this.sipautoanswer = sipautoanswer;
	}
	public String getAbscodec() {
		return abscodec;
	}
	public void setAbscodec(String abscodec) {
		this.abscodec = abscodec;
	}
	public String getEnableai() {
		return enableai;
	}
	public void setEnableai(String enableai) {
		this.enableai = enableai;
	}
	public String getAiid() {
		return aiid;
	}
	public void setAiid(String aiid) {
		this.aiid = aiid;
	}
	public String getSceneid() {
		return sceneid;
	}
	public void setSceneid(String sceneid) {
		this.sceneid = sceneid;
	}
	public String getWelcomemsg() {
		return welcomemsg;
	}
	public void setWelcomemsg(String welcomemsg) {
		this.welcomemsg = welcomemsg;
	}
	public String getWaitmsg() {
		return waitmsg;
	}
	public void setWaitmsg(String waitmsg) {
		this.waitmsg = waitmsg;
	}
	public String getTipmessage() {
		return tipmessage;
	}
	public void setTipmessage(String tipmessage) {
		this.tipmessage = tipmessage;
	}
	public String getAsrrecordpath() {
		return asrrecordpath;
	}
	public void setAsrrecordpath(String asrrecordpath) {
		this.asrrecordpath = asrrecordpath;
	}
	public String getTtsrecordpath() {
		return ttsrecordpath;
	}
	public void setTtsrecordpath(String ttsrecordpath) {
		this.ttsrecordpath = ttsrecordpath;
	}
	public boolean isAfterprocess() {
		return afterprocess;
	}
	public void setAfterprocess(boolean afterprocess) {
		this.afterprocess = afterprocess;
	}
	public boolean isEnablewebrtc() {
		return enablewebrtc;
	}
	public void setEnablewebrtc(boolean enablewebrtc) {
		this.enablewebrtc = enablewebrtc;
	}
	public String getWebrtcaddress() {
		return webrtcaddress;
	}
	public void setWebrtcaddress(String webrtcaddress) {
		this.webrtcaddress = webrtcaddress;
	}
	public String getWebrtcport() {
		return webrtcport;
	}
	public void setWebrtcport(String webrtcport) {
		this.webrtcport = webrtcport;
	}
	public boolean isWebrtcssl() {
		return webrtcssl;
	}
	public void setWebrtcssl(boolean webrtcssl) {
		this.webrtcssl = webrtcssl;
	}
	public boolean isDissipphone() {
		return dissipphone;
	}
	public void setDissipphone(boolean dissipphone) {
		this.dissipphone = dissipphone;
	}
	public int getSipport() {
		return sipport;
	}
	public void setSipport(int sipport) {
		this.sipport = sipport;
	}
	public String getBlacklist() {
		return blacklist;
	}
	public void setBlacklist(String blacklist) {
		this.blacklist = blacklist;
	}
	public String getWebrtcring() {
		return webrtcring;
	}
	public void setWebrtcring(String webrtcring) {
		this.webrtcring = webrtcring;
	}
	public String getWhitelist() {
		return whitelist;
	}
	public void setWhitelist(String whitelist) {
		this.whitelist = whitelist;
	}
	public int getMaxnumlength() {
		return maxnumlength;
	}
	public void setMaxnumlength(int maxnumlength) {
		this.maxnumlength = maxnumlength;
	}
	public int getMinnumlength() {
		return minnumlength;
	}
	public void setMinnumlength(int minnumlength) {
		this.minnumlength = minnumlength;
	}
	public String getIpregionblack() {
		return ipregionblack;
	}
	public void setIpregionblack(String ipregionblack) {
		this.ipregionblack = ipregionblack;
	}
	public String getIpregionwhite() {
		return ipregionwhite;
	}
	public void setIpregionwhite(String ipregionwhite) {
		this.ipregionwhite = ipregionwhite;
	}
	public boolean isSavekillevent() {
		return savekillevent;
	}
	public void setSavekillevent(boolean savekillevent) {
		this.savekillevent = savekillevent;
	}

	public String getGwlua() {
		return gwlua;
	}

	public void setGwlua(String gwlua) {
		this.gwlua = gwlua;
	}

	public boolean isDefaultpbx() {
		return defaultpbx;
	}

	public void setDefaultpbx(boolean defaultpbx) {
		this.defaultpbx = defaultpbx;
	}
	public boolean isOrgtrans() {
		return orgtrans;
	}
	public void setOrgtrans(boolean orgtrans) {
		this.orgtrans = orgtrans;
	}
	public String getOrgtransdest() {
		return orgtransdest;
	}
	public void setOrgtransdest(String orgtransdest) {
		this.orgtransdest = orgtransdest;
	}
	public boolean isEnablesync() {
		return enablesync;
	}
	public void setEnablesync(boolean enablesync) {
		this.enablesync = enablesync;
	}
	public boolean isEnablesyncgw() {
		return enablesyncgw;
	}
	public void setEnablesyncgw(boolean enablesyncgw) {
		this.enablesyncgw = enablesyncgw;
	}
	public boolean isEnableacl() {
		return enableacl;
	}
	public void setEnableacl(boolean enableacl) {
		this.enableacl = enableacl;
	}
	public boolean isEnablereplace() {
		return enablereplace;
	}
	public void setEnablereplace(boolean enablereplace) {
		this.enablereplace = enablereplace;
	}
	public String getReplacestr() {
		return replacestr;
	}
	public void setReplacestr(String replacestr) {
		this.replacestr = replacestr;
	}
	public String getReplacereg() {
		return replacereg;
	}
	public void setReplacereg(String replacereg) {
		this.replacereg = replacereg;
	}
	public boolean isConference() {
		return conference;
	}
	public void setConference(boolean conference) {
		this.conference = conference;
	}
	public boolean isInitleave() {
		return initleave;
	}
	public void setInitleave(boolean initleave) {
		this.initleave = initleave;
	}
	public String getConferencenum() {
		return conferencenum;
	}
	public void setConferencenum(String conferencenum) {
		this.conferencenum = conferencenum;
	}
	public boolean isAutoready() {
		return autoready;
	}
	public void setAutoready(boolean autoready) {
		this.autoready = autoready;
	}
	public String getAfterstatus() {
		return afterstatus;
	}
	public void setAfterstatus(String afterstatus) {
		this.afterstatus = afterstatus;
	}
	public String getOrgtranstype() {
		return orgtranstype;
	}
	public void setOrgtranstype(String orgtranstype) {
		this.orgtranstype = orgtranstype;
	}
	public boolean isLoginready() {
		return loginready;
	}
	public void setLoginready(boolean loginready) {
		this.loginready = loginready;
	}
	public boolean isLogoutready() {
		return logoutready;
	}
	public void setLogoutready(boolean logoutready) {
		this.logoutready = logoutready;
	}
	public boolean isBilling() {
		return billing;
	}
	public void setBilling(boolean billing) {
		this.billing = billing;
	}
	public String getPreviewring() {
		return previewring;
	}
	public void setPreviewring(String previewring) {
		this.previewring = previewring;
	}
	public boolean isBussop() {
		return bussop;
	}
	public void setBussop(boolean bussop) {
		this.bussop = bussop;
	}
	public String getBusslist() {
		return busslist;
	}
	public void setBusslist(String busslist) {
		this.busslist = busslist;
	}
	public String getBussexeclist() {
		return bussexeclist;
	}
	public void setBussexeclist(String bussexeclist) {
		this.bussexeclist = bussexeclist;
	}

	public String getIgrtype() {
		return igrtype;
	}
	public void setIgrtype(String igrtype) {
		this.igrtype = igrtype;
	}
	public boolean isIgr() {
		return igr;
	}
	public void setIgr(boolean igr) {
		this.igr = igr;
	}
	public String getIgrappid() {
		return igrappid;
	}
	public void setIgrappid(String igrappid) {
		this.igrappid = igrappid;
	}

	public String getIgrapikey() {
		return igrapikey;
	}
	public void setIgrapikey(String igrapikey) {
		this.igrapikey = igrapikey;
	}
	public String getIgrhost() {
		return igrhost;
	}
	public void setIgrhost(String igrhost) {
		this.igrhost = igrhost;
	}
	public String getIgrapisec() {
		return igrapisec;
	}
	public void setIgrapisec(String igrapisec) {
		this.igrapisec = igrapisec;
	}
	public boolean isHidephonenum() {
		return hidephonenum;
	}
	public void setHidephonenum(boolean hidephonenum) {
		this.hidephonenum = hidephonenum;
	}
	public String getDistype() {
		return distype;
	}
	public void setDistype(String distype) {
		this.distype = distype;
	}
	public boolean isSavesip() {
		return savesip;
	}
	public void setSavesip(boolean savesip) {
		this.savesip = savesip;
	}
	public boolean isEnableice() {
		return enableice;
	}
	public void setEnableice(boolean enableice) {
		this.enableice = enableice;
	}
	public boolean isEnablestun() {
		return enablestun;
	}
	public void setEnablestun(boolean enablestun) {
		this.enablestun = enablestun;
	}
	public String getStunserver() {
		return stunserver;
	}
	public void setStunserver(String stunserver) {
		this.stunserver = stunserver;
	}
	public String getStunserverport() {
		return stunserverport;
	}
	public void setStunserverport(String stunserverport) {
		this.stunserverport = stunserverport;
	}
	public boolean isEnableturn() {
		return enableturn;
	}
	public void setEnableturn(boolean enableturn) {
		this.enableturn = enableturn;
	}
	public String getTurnserver() {
		return turnserver;
	}
	public void setTurnserver(String turnserver) {
		this.turnserver = turnserver;
	}
	public String getTurnserverport() {
		return turnserverport;
	}
	public void setTurnserverport(String turnserverport) {
		this.turnserverport = turnserverport;
	}
	public String getTurntransport() {
		return turntransport;
	}
	public void setTurntransport(String turntransport) {
		this.turntransport = turntransport;
	}
	public String getTurnusername() {
		return turnusername;
	}
	public void setTurnusername(String turnusername) {
		this.turnusername = turnusername;
	}
	public String getTurncredential() {
		return turncredential;
	}
	public void setTurncredential(String turncredential) {
		this.turncredential = turncredential;
	}
	public boolean isOpensips() {
		return opensips;
	}
	public void setOpensips(boolean opensips) {
		this.opensips = opensips;
	}
	public String getOpensipsurl() {
		return opensipsurl;
	}
	public void setOpensipsurl(String opensipsurl) {
		this.opensipsurl = opensipsurl;
	}
	public boolean isOpensipsdomain() {
		return opensipsdomain;
	}
	public void setOpensipsdomain(boolean opensipsdomain) {
		this.opensipsdomain = opensipsdomain;
	}
	public String getOpensipsport() {
		return opensipsport;
	}
	public void setOpensipsport(String opensipsport) {
		this.opensipsport = opensipsport;
	}
	public boolean isEnablemwebrtc() {
		return enablemwebrtc;
	}
	public void setEnablemwebrtc(boolean enablemwebrtc) {
		this.enablemwebrtc = enablemwebrtc;
	}
	public boolean isEnableinviteuri() {
		return enableinviteuri;
	}
	public void setEnableinviteuri(boolean enableinviteuri) {
		this.enableinviteuri = enableinviteuri;
	}
	public boolean isEnablerouteuri() {
		return enablerouteuri;
	}
	public void setEnablerouteuri(boolean enablerouteuri) {
		this.enablerouteuri = enablerouteuri;
	}
	public String getOpensipsparam() {
		return opensipsparam;
	}
	public void setOpensipsparam(String opensipsparam) {
		this.opensipsparam = opensipsparam;
	}
	public String getDomainname() {
		return domainname;
	}
	public void setDomainname(String domainname) {
		this.domainname = domainname;
	}
	public boolean isEnablecleanqueue() {
		return enablecleanqueue;
	}
	public void setEnablecleanqueue(boolean enablecleanqueue) {
		this.enablecleanqueue = enablecleanqueue;
	}
}
