package com.ukefu.webim.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import com.ukefu.util.event.UserEvent;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventAnswer implements Serializable,UserEvent{

	private static final long serialVersionUID = 4164059970184274883L;

	private String id ;

	private Date updatetime = new Date() ;
	

	private String creater ;		//变更用处，标识是否已接通
	
	private String answer ;//应答时间

	private String servicestatus ;	//通话状态
	
	private Date answertime ;//应答时间
	private int ringduration ;//振铃时长
	
	private String touser ;//目标用户
	private String extention ;//联系人ID
	
	private String userid;
	private String username;

	private String channelstatus ;	//通道状态
	
	private boolean aitrans;		//AI转接
	private String aiext ;			//AI号码
	
	private String aitransqus;	//转接问题ID
	private Date aitranstime ;	//转接时间
	private long aitransduration ;	//转接时通话时长
	

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getServicestatus() {
		return servicestatus;
	}

	public void setServicestatus(String servicestatus) {
		this.servicestatus = servicestatus;
	}

	public Date getAnswertime() {
		return answertime;
	}

	public void setAnswertime(Date answertime) {
		this.answertime = answertime;
	}

	public int getRingduration() {
		return ringduration;
	}

	public void setRingduration(int ringduration) {
		this.ringduration = ringduration;
	}

	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getExtention() {
		return extention;
	}

	public void setExtention(String extention) {
		this.extention = extention;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getChannelstatus() {
		return channelstatus;
	}

	public void setChannelstatus(String channelstatus) {
		this.channelstatus = channelstatus;
	}
	public boolean isAitrans() {
		return aitrans;
	}
	public void setAitrans(boolean aitrans) {
		this.aitrans = aitrans;
	}
	public String getAiext() {
		return aiext;
	}
	public void setAiext(String aiext) {
		this.aiext = aiext;
	}
	public String getAitransqus() {
		return aitransqus;
	}
	public void setAitransqus(String aitransqus) {
		this.aitransqus = aitransqus;
	}
	public Date getAitranstime() {
		return aitranstime;
	}
	public void setAitranstime(Date aitranstime) {
		this.aitranstime = aitranstime;
	}
	public long getAitransduration() {
		return aitransduration;
	}
	public void setAitransduration(long aitransduration) {
		this.aitransduration = aitransduration;
	}
}
