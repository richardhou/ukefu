package com.ukefu.webim.web.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.SysDicRepository;

public class UKeFuDic<K,V> extends HashMap<K,V>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2110217015030751243L;
	private static UKeFuDic<Object, Object> uKeFuDic = new UKeFuDic<Object, Object>();
	
	public static UKeFuDic<?, ?> getInstance(){
		return uKeFuDic ;
	}
	
	@SuppressWarnings("unchecked")
	public List<SysDic> getSysDic(String key){
		return (List<SysDic>) CacheHelper.getSystemCacheBean().getCacheObject(key, UKDataContext.SYSTEM_ORGI)  ;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public V get(Object key) {
		Object obj = CacheHelper.getSystemCacheBean().getCacheObject(String.valueOf(key), UKDataContext.SYSTEM_ORGI) ;
		if(obj!=null && obj instanceof List){
			obj = getDic((String) key) ;
		}else if(obj == null && (String.valueOf(key)).endsWith(".subdic") && (String.valueOf(key)).lastIndexOf(".subdic") > 0){
			String id = (String.valueOf(key)).substring(0  , (String.valueOf(key)).lastIndexOf(".subdic")) ;
			SysDic dic = (SysDic) CacheHelper.getSystemCacheBean().getCacheObject(id, UKDataContext.SYSTEM_ORGI) ;
			if(dic!=null){
				SysDic sysDic = (SysDic) CacheHelper.getSystemCacheBean().getCacheObject(dic.getDicid(), UKDataContext.SYSTEM_ORGI) ;
				obj = getDic(sysDic.getCode(), dic.getParentid()) ;
			}
		}else if(obj == null && (String.valueOf(key)).endsWith(".subdiccode") && (String.valueOf(key)).lastIndexOf(".subdiccode") > 0){
			//以code翻译
			String codeStr = (String.valueOf(key)).substring(0  , (String.valueOf(key)).lastIndexOf(".subdiccode")) ;
			String parentStr = codeStr.substring(0  , codeStr.lastIndexOf(".")) ;
			String subdiccode = codeStr.substring(codeStr.lastIndexOf(".") +1 , codeStr.length()) ;
			List<SysDic> dicList = (List<SysDic>) CacheHelper.getSystemCacheBean().getCacheObject(parentStr, UKDataContext.SYSTEM_ORGI) ;
			if(dicList!=null){
				for(SysDic s : dicList){
					if(s.getCode().equals(subdiccode)){
						obj = s;
					}
				}
			}
		}else if(!StringUtils.isBlank(String.valueOf(key)) && (String.valueOf(key)).endsWith(".subdicid")){
			//根据字典项ID查询其下的子字典项
			String parentStr = String.valueOf(key).substring(0  , String.valueOf(key).lastIndexOf(".")) ;
			SysDic dic = (SysDic) CacheHelper.getSystemCacheBean().getCacheObject(parentStr, UKDataContext.SYSTEM_ORGI) ;
			if(dic != null){
				SysDicRepository sysDicRes = UKDataContext.getContext().getBean(SysDicRepository.class);
				obj = sysDicRes.findByParentid(dic.getId());
			}
		}else if(!StringUtils.isBlank(String.valueOf(key)) && (String.valueOf(key)).endsWith(".obtaincode")){
			//根据字典项code查询其下的子字典项
			String parentStr = String.valueOf(key).substring(0  , String.valueOf(key).lastIndexOf(".")) ;
			SysDicRepository sysDicRes = UKDataContext.getContext().getBean(SysDicRepository.class);
			List<SysDic> list = sysDicRes.findByCodeAndOrgi(parentStr, UKDataContext.SYSTEM_ORGI);
			if(list != null && !list.isEmpty() ){
				obj = sysDicRes.findByParentid(list.get(0).getId());
			}
		}
		return (V) obj;
	}
	
	@SuppressWarnings("unchecked")
	public List<SysDic> getDic(String code){
		List<SysDic> dicList = new ArrayList<SysDic>() ;
		List<SysDic> sysDicList = (List<SysDic>) CacheHelper.getSystemCacheBean().getCacheObject(code, UKDataContext.SYSTEM_ORGI)  ;
		if(sysDicList!=null){
			for(SysDic dic : sysDicList){
				if(dic.getParentid().equals(dic.getDicid())){
					dicList.add(dic) ;
				}
			}
		}
		return dicList ;
	}
	
	@SuppressWarnings("unchecked")
	public List<SysDic> getDic(String code , String id){
		List<SysDic> dicList = new ArrayList<SysDic>() ;
		List<SysDic> sysDicList = (List<SysDic>) CacheHelper.getSystemCacheBean().getCacheObject(code, UKDataContext.SYSTEM_ORGI)  ;
		if(sysDicList!=null){
			for(SysDic dic : sysDicList){
				if(dic.getParentid().equals(id)){
					dicList.add(dic) ;
				}
			}
		}
		return dicList ;
	}
	
	
	
	public List<SysDic> getEpt(){
		return new ArrayList<SysDic>() ;
	}
	
	public SysDic getDicItem(String id){
		return (SysDic) CacheHelper.getSystemCacheBean().getCacheObject(id, UKDataContext.SYSTEM_ORGI) ;
	}
	
	public String md5(String value) {
		return !StringUtils.isBlank( value) ? UKTools.md5(value) : "";
	}
}
