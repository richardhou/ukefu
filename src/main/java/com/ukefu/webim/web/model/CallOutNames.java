package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.ukefu.util.UKTools;

@Entity
@Table(name = "uk_act_callnames")
@org.hibernate.annotations.Proxy(lazy = false)
public class CallOutNames implements java.io.Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id  = UKTools.getUUID();
	private String orgi ;		//租户ID
	private String organ ;		//创建部门
	private String creater ;	//创建人
	private String batid ;		//导入 批次ID
	
	private boolean forecast ;	//是否预测式外呼
	
	private boolean transcon ;		//条件转人工
	private String transconid ;		//转人工条件
	
	private boolean ai ;		//是否机器人呼叫
	private boolean aicollect ;	//是否是对机器人通话挂断后质检
	private String aieventid ;	//机器人呼叫ID
	private int asrtimes ;		//asr次数
	private int ttstimes ;		//tts次数
	private int timeouttimes ;		//tts次数
	private int errortimes ;		//tts次数
	private int nmlinetimes ;		//tts次数
	
	private String calltype ;	//缓存里的临时字段， 人工外呼和机器人外呼，不存储进数据库
	
	@Transient
	private boolean transfaild ; //转人工失败
	
	private Date createtime = new Date();	//创建时间
	
	private Date updatetime = new Date();
	
	private String membersessionid ;	//转接前ID	
	
	private String datastatus;	//数据状态（逻辑删除）
	private String status ;		//状态
	private int calls;			//拨打次数
	private int faildcalls ;	//失败拨打次数
	
	private boolean invalid ;	//多次未接通名单（6次以上）
	private boolean failed ;	//无效名单
	
	private boolean aitrans ;	//机器人转接
	private String aiext ;		//机器人号码
	
	private String aitransqus;	//转接问题ID
	private Date aitranstime ;	//转接时间
	private long aitransduration ;	//转接时通话时长
	
	private String aitransans ;	//答案ID
	
	private String workstatus ;	//名单状态
	
	private boolean reservation ;	//是否预约
	private Date optime ;		//预约的下次拨打时间
	private String memo ;		//预约备注
	
	private String batname ;
	private String taskname ;
	
	private String servicetype ;	//服务类型标签
	
	private int leavenum ;		//剩余名单数量
	
	private String metaname ;	//表名
	private String conid ;	
	
	private boolean callsuccess;
	private String callresult;
	
	private String owneruser ;	//分配 坐席
	private String ownerdept ;	//分配 部门
	private String ownerai ;	//分配 机器人
	private String ownerforecast ;	//分配 队列
	private String dataid ;		//UKDataBean对象ID
	private String taskid ;		//任务ID
	private String filterid;	//筛选ID
	private String actid ;		//活动ID
	
	private String name ;		//名单名称	
	private String phonenumber;	//电话号码
	private String distype ;	//号码隐藏
	
	private int previewtimes ;	//预览次数
	private int previewtime ;	//预览时长
	
	private int duration ;//通话时长
	
	@Transient
	private int waittime ;//坐席等待时长
	
	private Date firstcalltime ;	//首次拨打时间
	private String firstcallstatus;	//首次拨打状态
	
	private boolean privatefield ;	
	private String priphonenumber ;	
	
	@Transient
	private long ringtime;//开始拨打时间
	
	@Transient
	private long answertime;//接听时间
	
	@Transient
	private String callstatus ;		//状态 ring振铃  answered通话中
	
	private String meta;
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	public String getOrgan() {
		return organ;
	}

	public void setOrgan(String organ) {
		this.organ = organ;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getDatastatus() {
		return datastatus;
	}

	public void setDatastatus(String datastatus) {
		this.datastatus = datastatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCalls() {
		return calls;
	}

	public void setCalls(int calls) {
		this.calls = calls;
	}

	public boolean isInvalid() {
		return invalid;
	}

	public void setInvalid(boolean invalid) {
		this.invalid = invalid;
	}

	public boolean isFailed() {
		return failed;
	}

	public void setFailed(boolean failed) {
		this.failed = failed;
	}

	public String getBatid() {
		return batid;
	}

	public void setBatid(String batid) {
		this.batid = batid;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getOwneruser() {
		return owneruser;
	}

	public void setOwneruser(String owneruser) {
		this.owneruser = owneruser;
	}

	public String getOwnerdept() {
		return ownerdept;
	}

	public void setOwnerdept(String ownerdept) {
		this.ownerdept = ownerdept;
	}

	public String getDataid() {
		return dataid;
	}

	public void setDataid(String dataid) {
		this.dataid = dataid;
	}

	public String getTaskid() {
		return taskid;
	}

	public void setTaskid(String taskid) {
		this.taskid = taskid;
	}

	public String getFilterid() {
		return filterid;
	}

	public void setFilterid(String filterid) {
		this.filterid = filterid;
	}

	public String getActid() {
		return actid;
	}

	public void setActid(String actid) {
		this.actid = actid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getWorkstatus() {
		return workstatus;
	}

	public void setWorkstatus(String workstatus) {
		this.workstatus = workstatus;
	}

	public Date getOptime() {
		return optime;
	}

	public void setOptime(Date optime) {
		this.optime = optime;
	}

	public String getBatname() {
		return batname;
	}

	public void setBatname(String batname) {
		this.batname = batname;
	}

	public String getTaskname() {
		return taskname;
	}

	public void setTaskname(String taskname) {
		this.taskname = taskname;
	}

	public int getLeavenum() {
		return leavenum;
	}

	public void setLeavenum(int leavenum) {
		this.leavenum = leavenum;
	}

	public int getFaildcalls() {
		return faildcalls;
	}

	public void setFaildcalls(int faildcalls) {
		this.faildcalls = faildcalls;
	}

	public String getMetaname() {
		return metaname;
	}

	public void setMetaname(String metaname) {
		this.metaname = metaname;
	}

	public String getDistype() {
		return distype;
	}

	public void setDistype(String distype) {
		this.distype = distype;
	}

	public int getPreviewtimes() {
		return previewtimes;
	}

	public void setPreviewtimes(int previewtimes) {
		this.previewtimes = previewtimes;
	}

	public int getPreviewtime() {
		return previewtime;
	}

	public void setPreviewtime(int previewtime) {
		this.previewtime = previewtime;
	}

	public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}

	public boolean isReservation() {
		return reservation;
	}

	public void setReservation(boolean reservation) {
		this.reservation = reservation;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Date getFirstcalltime() {
		return firstcalltime;
	}

	public void setFirstcalltime(Date firstcalltime) {
		this.firstcalltime = firstcalltime;
	}

	public String getFirstcallstatus() {
		return firstcallstatus;
	}

	public void setFirstcallstatus(String firstcallstatus) {
		this.firstcallstatus = firstcallstatus;
	}
	/**
	 * 
	 * @return
	 */
	public String getCalltype() {
		return calltype;
	}

	public void setCalltype(String calltype) {
		this.calltype = calltype;
	}

	public String getCallresult() {
		return callresult;
	}

	public void setCallresult(String callresult) {
		this.callresult = callresult;
	}

	public boolean isCallsuccess() {
		return callsuccess;
	}

	public void setCallsuccess(boolean callsuccess) {
		this.callsuccess = callsuccess;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getOwnerai() {
		return ownerai;
	}

	public void setOwnerai(String ownerai) {
		this.ownerai = ownerai;
	}

	public String getOwnerforecast() {
		return ownerforecast;
	}

	public void setOwnerforecast(String ownerforecast) {
		this.ownerforecast = ownerforecast;
	}

	public boolean isAitrans() {
		return aitrans;
	}

	public void setAitrans(boolean aitrans) {
		this.aitrans = aitrans;
	}

	public String getAiext() {
		return aiext;
	}

	public void setAiext(String aiext) {
		this.aiext = aiext;
	}

	public String getAitransqus() {
		return aitransqus;
	}

	public void setAitransqus(String aitransqus) {
		this.aitransqus = aitransqus;
	}

	public Date getAitranstime() {
		return aitranstime;
	}

	public void setAitranstime(Date aitranstime) {
		this.aitranstime = aitranstime;
	}

	public long getAitransduration() {
		return aitransduration;
	}

	public void setAitransduration(long aitransduration) {
		this.aitransduration = aitransduration;
	}

	public String getMembersessionid() {
		return membersessionid;
	}

	public void setMembersessionid(String membersessionid) {
		this.membersessionid = membersessionid;
	}

	public boolean isAi() {
		return ai;
	}

	public void setAi(boolean ai) {
		this.ai = ai;
	}

	public String getAieventid() {
		return aieventid;
	}

	public void setAieventid(String aieventid) {
		this.aieventid = aieventid;
	}

	public int getAsrtimes() {
		return asrtimes;
	}

	public void setAsrtimes(int asrtimes) {
		this.asrtimes = asrtimes;
	}

	public int getTtstimes() {
		return ttstimes;
	}

	public void setTtstimes(int ttstimes) {
		this.ttstimes = ttstimes;
	}

	public int getTimeouttimes() {
		return timeouttimes;
	}

	public void setTimeouttimes(int timeouttimes) {
		this.timeouttimes = timeouttimes;
	}

	public int getErrortimes() {
		return errortimes;
	}

	public void setErrortimes(int errortimes) {
		this.errortimes = errortimes;
	}

	public int getNmlinetimes() {
		return nmlinetimes;
	}

	public void setNmlinetimes(int nmlinetimes) {
		this.nmlinetimes = nmlinetimes;
	}

	public boolean isPrivatefield() {
		return privatefield;
	}

	public void setPrivatefield(boolean privatefield) {
		this.privatefield = privatefield;
	}

	public String getPriphonenumber() {
		return priphonenumber;
	}

	public void setPriphonenumber(String priphonenumber) {
		this.priphonenumber = priphonenumber;
	}

	@Transient
	public long getRingtime() {
		return ringtime;
	}

	public void setRingtime(long ringtime) {
		this.ringtime = ringtime;
	}

	@Transient
	public long getAnswertime() {
		return answertime;
	}

	public void setAnswertime(long answertime) {
		this.answertime = answertime;
	}

	@Transient
	public String getCallstatus() {
		return callstatus;
	}

	public void setCallstatus(String callstatus) {
		this.callstatus = callstatus;
	}

	@Transient
	public int getWaittime() {
		return waittime;
	}

	public void setWaittime(int waittime) {
		this.waittime = waittime;
	}

	public boolean isTransfaild() {
		return transfaild;
	}

	public void setTransfaild(boolean transfaild) {
		this.transfaild = transfaild;
	}

	public boolean isTranscon() {
		return transcon;
	}

	public void setTranscon(boolean transcon) {
		this.transcon = transcon;
	}

	public String getTransconid() {
		return transconid;
	}

	public void setTransconid(String transconid) {
		this.transconid = transconid;
	}
	@Transient
	public boolean isAicollect() {
		return aicollect;
	}

	public void setAicollect(boolean aicollect) {
		this.aicollect = aicollect;
	}
	@Transient
	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public String getAitransans() {
		return aitransans;
	}

	public void setAitransans(String aitransans) {
		this.aitransans = aitransans;
	}

	public String getConid() {
		return conid;
	}

	public void setConid(String conid) {
		this.conid = conid;
	}

	public boolean isForecast() {
		return forecast;
	}

	public void setForecast(boolean forecast) {
		this.forecast = forecast;
	}
}	
