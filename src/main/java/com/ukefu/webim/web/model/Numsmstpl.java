package com.ukefu.webim.web.model;

public class Numsmstpl implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4839215474511319999L;
	
	private String number;
	private String smshugup;	//通话挂机短信模板
	private String noanswer;	//呼入未接听短信模板
	private String smsorgnoanswer;//呼出未接听短信模板
	public String getSmshugup() {
		return smshugup;
	}
	public void setSmshugup(String smshugup) {
		this.smshugup = smshugup;
	}
	public String getNoanswer() {
		return noanswer;
	}
	public void setNoanswer(String noanswer) {
		this.noanswer = noanswer;
	}
	public String getSmsorgnoanswer() {
		return smsorgnoanswer;
	}
	public void setSmsorgnoanswer(String smsorgnoanswer) {
		this.smsorgnoanswer = smsorgnoanswer;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	
}
