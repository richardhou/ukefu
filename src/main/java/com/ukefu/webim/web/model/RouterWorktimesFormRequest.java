package com.ukefu.webim.web.model;

public class RouterWorktimesFormRequest implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3006124841945825670L;
	
	private String[] datetype;
	private String[] worktype;
	private int[] beginhours;
	private int[] beginmins;
	private int[] endhours ;	
	private int[] endmins ;
	private String[] action ;
	private String[] num ;
	private String[] voice;
	
	public String[] getDatetype() {
		return datetype;
	}
	public void setDatetype(String[] datetype) {
		this.datetype = datetype;
	}
	public String[] getWorktype() {
		return worktype;
	}
	public void setWorktype(String[] worktype) {
		this.worktype = worktype;
	}
	public int[] getBeginhours() {
		return beginhours;
	}
	public void setBeginhours(int[] beginhours) {
		this.beginhours = beginhours;
	}
	public int[] getBeginmins() {
		return beginmins;
	}
	public void setBeginmins(int[] beginmins) {
		this.beginmins = beginmins;
	}
	public int[] getEndhours() {
		return endhours;
	}
	public void setEndhours(int[] endhours) {
		this.endhours = endhours;
	}
	public int[] getEndmins() {
		return endmins;
	}
	public void setEndmins(int[] endmins) {
		this.endmins = endmins;
	}
	public String[] getAction() {
		return action;
	}
	public void setAction(String[] action) {
		this.action = action;
	}
	public String[] getNum() {
		return num;
	}
	public void setNum(String[] num) {
		this.num = num;
	}
	public String[] getVoice() {
		return voice;
	}
	public void setVoice(String[] voice) {
		this.voice = voice;
	}
	
	
}
