package com.ukefu.webim.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import com.ukefu.util.UKTools;

@Entity
@Table(name = "uk_servicesummary")
@Proxy(lazy = false)
public class AgentServiceSummary implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1435057931574776533L;
	private String agentusername;
	private String agentno;//(呼叫中心：接听人（坐席ID）)
	private String status;
	private long times;
	private Date servicetime;
	private Date createtime ;

	private String agentserviceid ;
	private String userid;
	
	private String statuseventid ;
	private String contactsid ;
	
	private String orgi;
	private String id = UKTools.getUUID();
	
	private String creater ;
	private String username;
	private String channel;
	private Date logindate;
	
	private String servicetype ;
	private boolean reservation ;
	private String reservtype ;
	
	private Date reservtime ;
	private String email ;
	private String phonenumber;
	
	private String ani ;
	private String caller;
	private String called ;
	private String agent ;
	
	private String summary ;	//服务小结 ， 备注

	private boolean process ;	//已处理
	private String updateuser ;	//处理人
	private Date updatetime ;	//处理时间
	private String processmemo;	//处理备注
	
	private boolean complaints;//是否投诉
	private String businesstype;//业务类型
	private String province ;	//省份
	private String city ;		//城市
	private String area ;		//县级
	private String cusname;//客户名称
	private Date dispatchtime;//派单时间
	private String ansorgan;//回复部门
	private String ansagent;//回复人
	private Date anstime;//回复时间
	private String anscontent;//回复内容
	private boolean finish;//是否已完结
	
	private String processstatus;//处理状态（业务）
	private String conductor;//处理人
	private String conductoruname;//处理人展示名
	
	public String getAgentusername() {
		return agentusername;
	}

	public void setAgentusername(String agentusername) {
		this.agentusername = agentusername;
	}

	public String getAgentno() {
		return agentno;
	}

	public void setAgentno(String agentno) {
		this.agentno = agentno;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getTimes() {
		return times;
	}

	public void setTimes(long times) {
		this.times = times;
	}

	public Date getServicetime() {
		return servicetime;
	}

	public void setServicetime(Date servicetime) {
		this.servicetime = servicetime;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Date getLogindate() {
		return logindate;
	}

	public void setLogindate(Date logindate) {
		this.logindate = logindate;
	}

	public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}

	public boolean isReservation() {
		return reservation;
	}

	public void setReservation(boolean reservation) {
		this.reservation = reservation;
	}

	public String getReservtype() {
		return reservtype;
	}

	public void setReservtype(String reservtype) {
		this.reservtype = reservtype;
	}

	public Date getReservtime() {
		return reservtime;
	}

	public void setReservtime(Date reservtime) {
		this.reservtime = reservtime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAgentserviceid() {
		return agentserviceid;
	}

	public void setAgentserviceid(String agentserviceid) {
		this.agentserviceid = agentserviceid;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getStatuseventid() {
		return statuseventid;
	}

	public void setStatuseventid(String statuseventid) {
		this.statuseventid = statuseventid;
	}

	public String getContactsid() {
		return contactsid;
	}

	public void setContactsid(String contactsid) {
		this.contactsid = contactsid;
	}

	public String getAni() {
		return ani;
	}

	public void setAni(String ani) {
		this.ani = ani;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getCalled() {
		return called;
	}

	public void setCalled(String called) {
		this.called = called;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public boolean isProcess() {
		return process;
	}

	public void setProcess(boolean process) {
		this.process = process;
	}

	public String getUpdateuser() {
		return updateuser;
	}

	public void setUpdateuser(String updateuser) {
		this.updateuser = updateuser;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getProcessmemo() {
		return processmemo;
	}

	public void setProcessmemo(String processmemo) {
		this.processmemo = processmemo;
	}

	public boolean isComplaints() {
		return complaints;
	}

	public void setComplaints(boolean complaints) {
		this.complaints = complaints;
	}

	public String getBusinesstype() {
		return businesstype;
	}

	public void setBusinesstype(String businesstype) {
		this.businesstype = businesstype;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCusname() {
		return cusname;
	}

	public void setCusname(String cusname) {
		this.cusname = cusname;
	}

	public Date getDispatchtime() {
		return dispatchtime;
	}

	public void setDispatchtime(Date dispatchtime) {
		this.dispatchtime = dispatchtime;
	}

	public String getAnsorgan() {
		return ansorgan;
	}

	public void setAnsorgan(String ansorgan) {
		this.ansorgan = ansorgan;
	}

	public String getAnsagent() {
		return ansagent;
	}

	public void setAnsagent(String ansagent) {
		this.ansagent = ansagent;
	}

	public Date getAnstime() {
		return anstime;
	}

	public void setAnstime(Date anstime) {
		this.anstime = anstime;
	}

	public String getAnscontent() {
		return anscontent;
	}

	public void setAnscontent(String anscontent) {
		this.anscontent = anscontent;
	}

	public boolean isFinish() {
		return finish;
	}

	public void setFinish(boolean finish) {
		this.finish = finish;
	}

	public String getProcessstatus() {
		return processstatus;
	}

	public void setProcessstatus(String processstatus) {
		this.processstatus = processstatus;
	}

	public String getConductor() {
		return conductor;
	}

	public void setConductor(String conductor) {
		this.conductor = conductor;
	}

	public String getConductoruname() {
		return conductoruname;
	}

	public void setConductoruname(String conductoruname) {
		this.conductoruname = conductoruname;
	}
}
