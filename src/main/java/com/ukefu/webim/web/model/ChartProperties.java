
package com.ukefu.webim.web.model;

import org.apache.commons.lang.StringUtils;

public class ChartProperties implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private String chartype ;
	
	private boolean noborder ;		//不显示图标边框
	
	private String bgcolor ;
	//private String id ;
	private boolean showtitle = false;
	private String title;
	
	private String chartcolor;	//图例颜色
	private String chartnum ;	//图例数量
	private String chartsize ;	//图例尺寸
	private boolean chartext ;	//显示图例 文字
	
	private String chartnamecolor ;	//图例指标名称颜色
	private String chartvaluecolor ;//图例数值颜色	
	
	private String titlebold ;	//粗体
	
	private boolean titleborder;	//显示标题下划线
	
	private String subtitlecontent ;
	private boolean subtitle ;
	
	private String titlesize ;
	private String titlecolor ;
	private String titlebg ;
	
	private String subtitlesize ;
	private String subtitlecolor ;
	private String subtitlebg ;
	
	private String contentsize ;
	private String contentcolor;
	private String contentvaluecolor;
	private String minuscolor = "#eb0a0a";
	private String contentbg ;
	
	private String ctnamesize = "14" ;		//正文指标名称字体大小
	private String ctvaluesize = "24";	//正文指标数值字体大小
	
	private String contentlineheight ;	//行高
	
	private String xaxis ;	
	private String xaxis_title = "key";
	private String xaxis_label ;//x label format
	
	private String format;
	
	private String time ;			//刷新频率
	
	private String yaxis ;
	private String yaxis_title ="value";
	private String yaxis_label ;
	
	private boolean linkdata ;		//启用关联数据
	private String linkdatatitle ;	//关联数据标题
	private int linkcolspan ;	//合并单元格数量
	private boolean linksplit	;	//合并数据指标
	
	private boolean legen = false;//显示图例
	private String legenalign = "bottom";
	
	private boolean enablelink ;	    //启用关联加载数据
	private String linkdatajson ;		//关联数据的 序列化对象
	

	/*private boolean openPageSize = false;
	private int pageSize = 50;*/
	
	private boolean credits ;
	private boolean exporting=false;
	private boolean dataview = false;//显示数值
	private boolean theme = false;
	private String themename;
	public String getChartype() {
		return chartype;
	}
	public void setChartype(String chartype) {
		this.chartype = chartype;
	}
	/*public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}*/
	public boolean isShowtitle() {
		return showtitle;
	}
	public void setShowtitle(boolean showtitle) {
		this.showtitle = showtitle;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getXaxis() {
		return xaxis;
	}
	public void setXaxis(String xaxis) {
		this.xaxis = xaxis;
	}
	public String getXaxis_title() {
		return xaxis_title;
	}
	public void setXaxis_title(String xaxis_title) {
		this.xaxis_title = xaxis_title;
	}
	public String getXaxis_label() {
		return xaxis_label;
	}
	public void setXaxis_label(String xaxis_label) {
		this.xaxis_label = xaxis_label;
	}
	public String getYaxis() {
		return yaxis;
	}
	public void setYaxis(String yaxis) {
		this.yaxis = yaxis;
	}
	public String getYaxis_title() {
		return yaxis_title;
	}
	public void setYaxis_title(String yaxis_title) {
		this.yaxis_title = yaxis_title;
	}
	public String getYaxis_label() {
		return yaxis_label;
	}
	public void setYaxis_label(String yaxis_label) {
		this.yaxis_label = yaxis_label;
	}
	public boolean isLegen() {
		return legen;
	}
	public void setLegen(boolean legen) {
		this.legen = legen;
	}
	public String getLegenalign() {
		return legenalign;
	}
	public void setLegenalign(String legenalign) {
		this.legenalign = legenalign;
	}
	public boolean isCredits() {
		return credits;
	}
	public void setCredits(boolean credits) {
		this.credits = credits;
	}
	public boolean isExporting() {
		return exporting;
	}
	public void setExporting(boolean exporting) {
		this.exporting = exporting;
	}
	public boolean isDataview() {
		return dataview;
	}
	public void setDataview(boolean dataview) {
		this.dataview = dataview;
	}
	public boolean isTheme() {
		return theme;
	}
	public void setTheme(boolean theme) {
		this.theme = theme;
	}
	public String getThemename() {
		return themename;
	}
	public void setThemename(String themename) {
		this.themename = themename;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getTitlesize() {
		return titlesize;
	}
	public void setTitlesize(String titlesize) {
		this.titlesize = titlesize;
	}
	public String getTitlecolor() {
		return titlecolor;
	}
	public void setTitlecolor(String titlecolor) {
		this.titlecolor = titlecolor;
	}
	public String getTitlebg() {
		return titlebg;
	}
	public void setTitlebg(String titlebg) {
		this.titlebg = titlebg;
	}
	public String getSubtitlesize() {
		return subtitlesize;
	}
	public void setSubtitlesize(String subtitlesize) {
		this.subtitlesize = subtitlesize;
	}
	public String getSubtitlecolor() {
		return subtitlecolor;
	}
	public void setSubtitlecolor(String subtitlecolor) {
		this.subtitlecolor = subtitlecolor;
	}
	public String getSubtitlebg() {
		return subtitlebg;
	}
	public void setSubtitlebg(String subtitlebg) {
		this.subtitlebg = subtitlebg;
	}
	public String getContentsize() {
		return contentsize;
	}
	public void setContentsize(String contentsize) {
		this.contentsize = contentsize;
	}
	public String getContentcolor() {
		return contentcolor;
	}
	public void setContentcolor(String contentcolor) {
		this.contentcolor = contentcolor;
	}
	public String getContentbg() {
		return contentbg;
	}
	public void setContentbg(String contentbg) {
		this.contentbg = contentbg;
	}
	public String getSubtitlecontent() {
		return subtitlecontent;
	}
	public void setSubtitlecontent(String subtitlecontent) {
		this.subtitlecontent = subtitlecontent;
	}
	public boolean isSubtitle() {
		return subtitle;
	}
	public void setSubtitle(boolean subtitle) {
		this.subtitle = subtitle;
	}
	public String getBgcolor() {
		return bgcolor;
	}
	public void setBgcolor(String bgcolor) {
		this.bgcolor = bgcolor;
	}
	public String getTitlebold() {
		return titlebold;
	}
	public void setTitlebold(String titlebold) {
		this.titlebold = titlebold;
	}
	public boolean isTitleborder() {
		return titleborder;
	}
	public void setTitleborder(boolean titleborder) {
		this.titleborder = titleborder;
	}
	public String getChartcolor() {
		return chartcolor;
	}
	public void setChartcolor(String chartcolor) {
		this.chartcolor = chartcolor;
	}
	public int getChartnum() {
		return !StringUtils.isBlank(chartnum) && chartnum.matches("[\\d]{1,}") ? Integer.parseInt(chartnum) : 0;
	}
	public void setChartnum(String chartnum) {
		this.chartnum = chartnum;
	}
	public String getChartsize() {
		return chartsize;
	}
	public void setChartsize(String chartsize) {
		this.chartsize = chartsize;
	}
	public boolean isChartext() {
		return chartext;
	}
	public void setChartext(boolean chartext) {
		this.chartext = chartext;
	}
	public boolean isNoborder() {
		return noborder;
	}
	public void setNoborder(boolean noborder) {
		this.noborder = noborder;
	}
	public String getContentlineheight() {
		return contentlineheight;
	}
	public void setContentlineheight(String contentlineheight) {
		this.contentlineheight = contentlineheight;
	}
	public String getChartnamecolor() {
		return chartnamecolor;
	}
	public void setChartnamecolor(String chartnamecolor) {
		this.chartnamecolor = chartnamecolor;
	}
	public String getChartvaluecolor() {
		return chartvaluecolor;
	}
	public void setChartvaluecolor(String chartvaluecolor) {
		this.chartvaluecolor = chartvaluecolor;
	}
	public String getContentvaluecolor() {
		return contentvaluecolor;
	}
	public void setContentvaluecolor(String contentvaluecolor) {
		this.contentvaluecolor = contentvaluecolor;
	}
	public String getMinuscolor() {
		return minuscolor;
	}
	public void setMinuscolor(String minuscolor) {
		this.minuscolor = minuscolor;
	}
	public String getCtnamesize() {
		return ctnamesize;
	}
	public void setCtnamesize(String ctnamesize) {
		this.ctnamesize = ctnamesize;
	}
	public String getCtvaluesize() {
		return ctvaluesize;
	}
	public void setCtvaluesize(String ctvaluesize) {
		this.ctvaluesize = ctvaluesize;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public boolean isLinkdata() {
		return linkdata;
	}
	public void setLinkdata(boolean linkdata) {
		this.linkdata = linkdata;
	}
	public String getLinkdatatitle() {
		return linkdatatitle;
	}
	public void setLinkdatatitle(String linkdatatitle) {
		this.linkdatatitle = linkdatatitle;
	}
	public int getLinkcolspan() {
		return linkcolspan;
	}
	public void setLinkcolspan(int linkcolspan) {
		this.linkcolspan = linkcolspan;
	}
	public boolean isLinksplit() {
		return linksplit;
	}
	public void setLinksplit(boolean linksplit) {
		this.linksplit = linksplit;
	}
	public String getLinkdatajson() {
		return linkdatajson;
	}
	public void setLinkdatajson(String linkdatajson) {
		this.linkdatajson = linkdatajson;
	}
	public boolean isEnablelink() {
		return enablelink;
	}
	public void setEnablelink(boolean enablelink) {
		this.enablelink = enablelink;
	}
}
