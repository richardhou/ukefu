package com.ukefu.webim.web.handler.admin.callcenter;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.util.mobile.MobileAddress;
import com.ukefu.util.mobile.MobileNumberUtils;
import com.ukefu.util.task.DSData;
import com.ukefu.util.task.DSDataEvent;
import com.ukefu.util.task.ExcelImportProecess;
import com.ukefu.util.task.process.BlackListProcess;
import com.ukefu.webim.service.repository.BlackListRepository;
import com.ukefu.webim.service.repository.MetadataRepository;
import com.ukefu.webim.service.repository.ReporterRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.BlackEntity;
import com.ukefu.webim.web.model.MetadataTable;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.UKeFuDic;

@Controller
@RequestMapping("/admin/callcenter")
public class CallCenterBlackController extends Handler{
	
	@Autowired
	private BlackListRepository blackRes ;
	
	@Autowired
	private ReporterRepository reporterRes ;
	
	@Autowired
	private MetadataRepository metadataRes ;
	
	@Value("${web.upload-path}")
    private String path;
	
	@RequestMapping(value = "/black")
    @Menu(type = "callcenter" , subtype = "callcenterblack" , access = false )
    public ModelAndView black(ModelMap map , HttpServletRequest request , @Valid String hostid) {
		map.addAttribute("blackList" , blackRes.findByOrgiAndListtypeIsNull(super.getOrgi(request),
				new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.DESC, "createtime")));
		return request(super.createRequestPageTempletResponse("/admin/callcenter/black/index"));
    }
	
	@RequestMapping(value = "/black/add")
    @Menu(type = "callcenter" , subtype = "black" , access = false )
    public ModelAndView blackadd(ModelMap map , HttpServletRequest request , @Valid String hostid) {
    	return request(super.createRequestPageTempletResponse("/admin/callcenter/black/add"));
    }
	
	@RequestMapping(value = "/black/save")
    @Menu(type = "callcenter" , subtype = "black" , access = false )
    public ModelAndView blacksave(ModelMap map , HttpServletRequest request , @Valid String phones) {
		if(!StringUtils.isBlank(phones)){
			String[] ps = phones.split("[ ,，\t\n]") ;
			for(String ph : ps){
				if(ph.length() >= 3){
					int count = blackRes.countByPhoneAndOrgi(ph.trim(), super.getOrgi(request)) ;
					if(count == 0){
						BlackEntity be = new BlackEntity();
						be.setPhone(ph.trim());
						be.setId(UKTools.md5(ph.trim()));
						be.setChannel(UKDataContext.ChannelTypeEnum.PHONE.toString());
						be.setOrgi(super.getOrgi(request));
						be.setCreater(super.getUser(request).getId());
						blackRes.save(be) ;
					}
				}
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/admin/callcenter/black.html"));
    }
	
	@RequestMapping(value = "/black/edit")
    @Menu(type = "callcenter" , subtype = "black" , access = false )
    public ModelAndView blackedit(ModelMap map , HttpServletRequest request , @Valid String id) {
		map.addAttribute("black" , blackRes.findByIdAndOrgi(id, super.getOrgi(request)));
    	return request(super.createRequestPageTempletResponse("/admin/callcenter/black/edit"));
    }
	
	@RequestMapping(value = "/black/update")
    @Menu(type = "callcenter" , subtype = "black" , access = false )
    public ModelAndView pbxhostupdate(ModelMap map , HttpServletRequest request , @Valid BlackEntity black) {
		if(!StringUtils.isBlank(black.getId())){
			BlackEntity oldBlack = blackRes.findByIdAndOrgi(black.getId(), super.getOrgi(request)) ;
			if(oldBlack!=null){
				oldBlack.setPhone(black.getPhone());
				oldBlack.setChannel(UKDataContext.ChannelTypeEnum.PHONE.toString());
				oldBlack.setOrgi(super.getOrgi(request));
				blackRes.save(oldBlack);
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/admin/callcenter/black.html"));
    }
	
	@RequestMapping(value = "/black/delete")
    @Menu(type = "callcenter" , subtype = "black" , access = false )
    public ModelAndView blackdelete(ModelMap map , HttpServletRequest request , @Valid String id) {
		if(!StringUtils.isBlank(id)){
			blackRes.delete(id);
		}
		return request(super.createRequestPageTempletResponse("redirect:/admin/callcenter/black.html"));
    }
	
	@RequestMapping("/black/imp")
    @Menu(type = "callcenter" , subtype = "black")
    public ModelAndView imp(ModelMap map , HttpServletRequest request) {
		return request(super.createRequestPageTempletResponse("/admin/callcenter/black/imp"));
    }
    
    @RequestMapping("/black/impsave")
    @Menu(type = "callcenter" , subtype = "black")
    public ModelAndView impsave(ModelMap map , HttpServletRequest request , @RequestParam(value = "cusfile", required = false) MultipartFile cusfile) throws IOException {
    	DSDataEvent event = new DSDataEvent();
    	String fileName = "blacklist/"+UKTools.getUUID()+cusfile.getOriginalFilename().substring(cusfile.getOriginalFilename().lastIndexOf(".")) ;
    	File excelFile = new File(path , fileName) ;
    	if(!excelFile.getParentFile().exists()){
    		excelFile.getParentFile().mkdirs() ;
    	}
    	MetadataTable table = metadataRes.findByTablenameIgnoreCase("uk_blacklist") ;
    	if(table!=null){
	    	FileUtils.writeByteArrayToFile(new File(path , fileName), cusfile.getBytes());
	    	event.setDSData(new DSData(table,excelFile , cusfile.getContentType(), super.getUser(request)));
	    	event.getDSData().setClazz(BlackEntity.class);
	    	event.getDSData().setProcess(new BlackListProcess(blackRes));
	    	event.setOrgi(super.getOrgi(request));
	    	
	    	event.getValues().put("orgi", super.getOrgi(request)) ;
	    	event.getValues().put("createtime", new Date());
	    	event.getValues().put("channel", UKDataContext.ChannelTypeEnum.PHONE.toString());
	    	event.getValues().put("creater", super.getUser(request).getId()) ;
	    	reporterRes.save(event.getDSData().getReport()) ;
	    	new ExcelImportProecess(event).process() ;		//启动导入任务
    	}
    	
    	return request(super.createRequestPageTempletResponse("redirect:/admin/callcenter/black.html"));
    }
}
