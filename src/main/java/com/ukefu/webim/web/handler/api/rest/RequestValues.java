package com.ukefu.webim.web.handler.api.rest;

import java.io.Serializable;
import java.util.List;

public class RequestValues<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private QueryParams query ;
	private String batid ;
	private String orgi ;
	private T data ;
	private List<T> list ;
	public QueryParams getQuery() {
		return query;
	}
	public void setQuery(QueryParams query) {
		this.query = query;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
	public String getBatid() {
		return batid;
	}
	public void setBatid(String batid) {
		this.batid = batid;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
}
