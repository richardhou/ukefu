package com.ukefu.webim.web.handler.admin.system;

import com.hazelcast.core.HazelcastInstance;
import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.util.rpc.RPCDataBean;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.*;
import com.ukefu.webim.util.CallCenterUtils;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin/servicecon")
public class SystemServiceConfigController extends Handler{
	
	@Value("${uk.im.server.port}")  
    private Integer port;

	@Value("${web.upload-path}")
    private String path;
	
	@Value("${uk.file.server.port}")  
    private Integer voiceport;
	
	@Autowired
	public HazelcastInstance hazelcastInstance;	

	
	@Autowired
	private SMSTemplateRepository smsTemplateRes ;
	
	@Autowired
	private UserRepository userRes ;
	
	@Autowired
	private CallOutConfigRepository callOutConfigRepository;
	
	@Autowired
	private SystemMessageRepository systemMessageRepository;
	
	@Autowired
	private JobDetailRepository jobDetailRepository;
	
	@Autowired
	private FormFilterRepository filterRes;
	
	
	@Autowired
	private SaleStatusRepository saleStatusRes; //状态
	
	@Autowired
	private TemplateRepository templateRes;
	
	@Autowired
	private MediaRepository mediaRes ;

	
	@Autowired
	private PbxHostRepository pbxHostRes ;
	
	@Autowired
	private PbxHostOrgiRelaRepository pbxHostOrgiRelaRepository ;
	
	@Autowired
	private SNSAccountRepository snsAccountRes ;

	
    @RequestMapping("/index")
    @Menu(type = "admin" , subtype = "servicecon" , name = "superuser")
    public ModelAndView index(ModelMap map , HttpServletRequest request , @Valid String execute) throws SQLException {
    	List<CallOutConfig> configList = callOutConfigRepository.findByOrgi(super.getOrgi(request)) ;
    	if(configList.size() > 0) {
			map.put("callOutConfig", configList.get(0)) ;
		}

        return request(super.createAdminTempletResponse("/admin/servicecon/index"));
    }
    
    
    @RequestMapping("/save")
    @Menu(type = "admin" , subtype = "servicecon" , name = "superuser")
    public ModelAndView save(ModelMap map , HttpServletRequest request , @Valid CallOutConfig config ,BindingResult result 
    		,GuideData guideData) throws Exception {
    	CallOutConfig callOutConfigOld = callOutConfigRepository.findByIdAndOrgi(config.getId(), super.getOrgi(request));
    	
    	if(callOutConfigOld == null){
    		callOutConfigOld = new CallOutConfig();
    		callOutConfigOld.setOrgi(super.getOrgi(request));


    	}
		callOutConfigOld.setEnablebusinesscustom(config.isEnablebusinesscustom());

		saveGuideData(guideData, callOutConfigOld, config);

		callOutConfigOld.setEnablecuswodstatus(config.isEnablecuswodstatus());
		callOutConfigOld.setCuswodstatusdic(config.getCuswodstatusdic());
		
    	callOutConfigRepository.save(callOutConfigOld);

		if(!StringUtils.isBlank(callOutConfigOld.getDataid())) {
			CacheHelper.getSystemCacheBean().put(UKDataContext.SYSTEM_CACHE_CALLOUT_CONFIG+"_"+callOutConfigOld.getDataid() , callOutConfigOld, super.getOrgi(request));
		}else {
			CacheHelper.getSystemCacheBean().put(UKDataContext.SYSTEM_CACHE_CALLOUT_CONFIG+"_"+callOutConfigOld.getOrgi() , callOutConfigOld, super.getOrgi(request));
		}

    	return request(super.createRequestPageTempletResponse("redirect:/admin/servicecon/index.html"));
    }


	public void saveGuideData(GuideData guideDataTemp ,CallOutConfig callOutConfigOld ,CallOutConfig config ){
		GuideData guideData = null;
		List<GuideData> guideDataList = null;

		callOutConfigOld.setEnableagentcustomtabs(config.isEnableagentcustomtabs());
		if(guideDataTemp.getDisplaytype() != null && guideDataTemp.getDisplaytype().length > 0) {
			guideDataList = new ArrayList<>();
			for(int i = 0 ; i < guideDataTemp.getDisplaytype().length ; i++) {
				guideData = new GuideData();

				if(guideDataTemp.getDisplaytype().length > 0 && StringUtils.isNotBlank(guideDataTemp.getDisplaytype()[i])) {
					guideData.setType(guideDataTemp.getDisplaytype()[i].toString());
				}

				if(guideDataTemp.getTabname().length > 0 && StringUtils.isNotBlank(guideDataTemp.getTabname()[i])) {
					guideData.setTitle(guideDataTemp.getTabname()[i].toString());
				}

				if(guideDataTemp.getTaburl().length > 0 && StringUtils.isNotBlank(guideDataTemp.getTaburl()[i])) {
					guideData.setUrl(guideDataTemp.getTaburl()[i].toString());
				}
				guideDataList.add(guideData);
			}
			guideData = new GuideData();
			guideData.setList(guideDataList);
			callOutConfigOld.setDataagentcustomtabs(UKTools.toJson(guideDataList));
		}else {
			callOutConfigOld.setDataagentcustomtabs(null);
		}


	}
    
    
    
}