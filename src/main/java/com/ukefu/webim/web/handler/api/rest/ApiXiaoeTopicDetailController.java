package com.ukefu.webim.web.handler.api.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ukefu.util.Menu;
import com.ukefu.webim.service.es.TopicRepository;
import com.ukefu.webim.util.RestResult;
import com.ukefu.webim.util.RestResultType;
import com.ukefu.webim.web.handler.Handler;

@RestController
@RequestMapping("/api/xiaoe/topic/detail")
@Api(value = "智能机器人", description = "知识库条目内容")
public class ApiXiaoeTopicDetailController extends Handler{

	@Autowired
	private TopicRepository topicRepository;
	
	/**
	 * 知识库管理功能
	 * @param request
	 * @param username	搜索用户名，精确搜索
	 * @return
	 */
	@RequestMapping( method = RequestMethod.GET)
	@Menu(type = "apps" , subtype = "topic" , access = true)
	@ApiOperation("返回单条知识库条目")
    public ResponseEntity<RestResult> list(HttpServletRequest request , @Valid String id) {
        return new ResponseEntity<>(new RestResult(RestResultType.OK, topicRepository.findByIdAndOrgi(id,super.getOrgi(request))), HttpStatus.OK);
    }
}