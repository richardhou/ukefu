package com.ukefu.webim.web.handler.api.rest;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.ukefu.util.*;
import com.ukefu.webim.service.repository.*;
import com.ukefu.webim.web.handler.api.request.SearchData;
import com.ukefu.webim.web.model.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.extra.DataExchangeInterface;
import com.ukefu.webim.service.acd.ServiceQuene;
import com.ukefu.webim.util.MessageUtils;
import com.ukefu.webim.util.OnlineUserUtils;
import com.ukefu.webim.util.RestResult;
import com.ukefu.webim.util.RestResultType;
import com.ukefu.webim.util.server.message.ChatMessage;
import com.ukefu.webim.web.handler.Handler;

import freemarker.template.TemplateException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import sun.management.Agent;

@RestController
@RequestMapping("/api/webim")
@Api(value = "在线客服" , description = "在线客服接口功能")
public class ApiIMController extends Handler{

	@Autowired
	private ConsultInviteRepository consultInviteRepository;
	
	@Autowired
	private ChatMessageRepository chatMessageRes ;
	
	@Autowired
	private AgentServiceSatisRepository agentServiceSatisRes ;
	

	@Autowired
	private LeaveMsgRepository leaveMsgRes ;

	@Autowired
	private AgentUserRepository agentUserRepository ;

	@Autowired
	private TenantRepository  tenantRepository;

	@Autowired
	private AttachmentRepository attachementRes;

	@Autowired
	private SNSAccountRepository snsAccountRes;

	@Value("${web.upload-path}")
    private String path;
	
	/**
	 * 返回在线网站配置
	 * @param request
	 * @param id	snsaccountid
	 * @return
	 */
	@RequestMapping(value = "/{id}")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取在线客服")
    public ResponseEntity<RestResult> list(HttpServletRequest request , @PathVariable String id) {
		Map<String,Object> configMap = new HashMap<String,Object>();
		configMap.put("invite" , OnlineUserUtils.cousult(id ,null, consultInviteRepository)) ;
		configMap.put("commentList" , UKeFuDic.getInstance().getDic(UKDataContext.UKEFU_SYSTEM_COMMENT_DIC)) ;
		configMap.put("commentItemList" , UKeFuDic.getInstance().getDic(UKDataContext.UKEFU_SYSTEM_COMMENT_ITEM_DIC)) ;
        return new ResponseEntity<>(new RestResult(RestResultType.OK, configMap), HttpStatus.OK);
    }
	
	/**
	 * 返回当前访客的会话ID
	 * @param request
	 * @param username	搜索用户名，精确搜索
	 * @return
	 */
	@RequestMapping(value = "/session")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取在线客服会话ID")
    public ResponseEntity<RestResult> session(HttpServletRequest request) {
        return new ResponseEntity<>(new RestResult(RestResultType.OK, UKTools.getContextID(request.getSession().getId()) ), HttpStatus.OK);
    }
	

	/**
	 * 返回推荐知识
	 * @param request
	 * @param username	搜索用户名，精确搜索
	 * @return
	 * @throws TemplateException 
	 * @throws IOException 
	 */
	@RequestMapping(value = "/query")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取推荐知识")
    public ResponseEntity<RestResult> query(HttpServletRequest request,@Valid String q,@Valid String appid) throws IOException, TemplateException {
        return new ResponseEntity<>(new RestResult(RestResultType.OK, OnlineUserUtils.search(q, super.getOrgi(request), super.getUser(request) , appid)), HttpStatus.OK);
    }
	
	/**
	 * 返回热点知识
	 * @param request
	 * @param username	搜索用户名，精确搜索
	 * @return
	 * @throws TemplateException 
	 * @throws IOException 
	 */
	@RequestMapping(value = "/hot")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取推荐知识")
    public ResponseEntity<RestResult> hot(HttpServletRequest request,@Valid String q , @Valid String aiid) throws IOException, TemplateException {
        return new ResponseEntity<>(new RestResult(RestResultType.OK, OnlineUserUtils.suggest(q, super.getOrgi(request), super.getUser(request).getId(),null, aiid , null)), HttpStatus.OK);
    }
	
	/**
	 * 返回AI配置信息
	 * @param request
	 * @param aiid    , AI标识	
	 * @return
	 */
	@RequestMapping(value = "/ai")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取在线客服会话ID")
    public ResponseEntity<RestResult> session(HttpServletRequest request , @Valid String aiid, @Valid String orgi) {
		AiConfig aiConfig = null ;
		if(UKDataContext.model.get("xiaoe")!=null && !StringUtils.isBlank(aiid)){	//启用 AI ， 并且 AI优先 接待
			DataExchangeInterface dataInterface = (DataExchangeInterface) UKDataContext.getContext().getBean("aiconfig") ;
			aiConfig = (AiConfig) dataInterface.getDataByIdAndOrgi(aiid, orgi) ;
		}
        return new ResponseEntity<>(new RestResult(RestResultType.OK, aiConfig), HttpStatus.OK);
    }
	
	/**
	 * 返回访客历史消息
	 * @param request
	 * @param userid	访客ID
	 * @p 分页信息
	 * @return
	 */
	@RequestMapping(value = "/chat/his")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取在线客服会话历史消息 ，支持分页，分页参数为 p=1&ps=20，默认分页尺寸为 20条每页 按最新时间倒序")
    public ResponseEntity<RestResult> history(HttpServletRequest request , @Valid String userid, @Valid String orgi, @Valid String uid) {
        return new ResponseEntity<>(new RestResult(RestResultType.OK, chatMessageRes.findByUsessionAndOrgi(uid , orgi, new PageRequest(super.getP(request), super.getPs(request), Direction.DESC , "updatetime"))), HttpStatus.OK);
    }


	/**
	 * 返回访客历史消息
	 * @param request
	 * @param userid	访客ID
	 * @p 分页信息
	 * @return
	 */
	@RequestMapping(value = "/agent")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取在线客服会话历史消息")
    public ResponseEntity<RestResult> agent(HttpServletRequest request , @Valid String orgi) {
        return new ResponseEntity<>(new RestResult(RestResultType.OK, ServiceQuene.getAgentReport(orgi)), HttpStatus.OK);
    }
	
	/**
	 * 返回访客历史消息
	 * @param request
	 * @param userid	访客ID
	 * @p 分页信息
	 * @return
	 */
	@RequestMapping(value = "/url")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取图片和语音信息资源的访问URL地址信息")
    public ResponseEntity<RestResult> url(HttpServletRequest request , @Valid String orgi) {
		SystemConfig systemConfig = UKTools.getSystemConfig();
        return new ResponseEntity<>(new RestResult(RestResultType.OK, systemConfig!=null ? systemConfig.getIconstr():""), HttpStatus.OK);
    }
	
	/**
	 * 返回访客历史消息
	 * @param request
	 * @param userid	访客ID
	 * @p 分页信息
	 * @return
	 */
	@RequestMapping(value = "/satis")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取满意度调查")
    public ResponseEntity<RestResult> satis(HttpServletRequest request , @Valid String orgi) {
		Map<String,Object> statfMap = new HashMap<String,Object>();
		statfMap.put("commentList" , UKeFuDic.getInstance().getDic(UKDataContext.UKEFU_SYSTEM_COMMENT_DIC)) ;
		statfMap.put("commentItemList" , UKeFuDic.getInstance().getDic(UKDataContext.UKEFU_SYSTEM_COMMENT_ITEM_DIC)) ;
        return new ResponseEntity<>(new RestResult(RestResultType.OK, statfMap), HttpStatus.OK);
    }
	
	/**
	 * 返回访客历史消息
	 * @param request
	 * @param userid	访客ID
	 * @p 分页信息
	 * @return
	 */
	@RequestMapping(value = "/satis/save", method = RequestMethod.POST)
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取满意度调查")
    public ResponseEntity<RestResult> satissave(HttpServletRequest request , @Valid String orgi , @Valid AgentServiceSatis satis) {
		if(satis!=null && !StringUtils.isBlank(satis.getId())){
    		int count = agentServiceSatisRes.countByIdAndOrgi(satis.getId() ,orgi) ;
    		if(count == 1){
    			if(!StringUtils.isBlank(satis.getSatiscomment()) && satis.getSatiscomment().length() > 255){
    				satis.setSatiscomment(satis.getSatiscomment().substring(0 , 255));
    			}
    			satis.setSatisfaction(true);
    			satis.setSatistime(new Date());
    			agentServiceSatisRes.save(satis) ;
    		}
    	}
        return new ResponseEntity<>(new RestResult(RestResultType.OK), HttpStatus.OK);
    }
	
	/**
	 * 保存留言
	 * @param request
	 * @param userid	访客ID
	 * @p 分页信息
	 * @return
	 */
	@RequestMapping(value = "/leavemsg/save", method = RequestMethod.POST)
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("保存留言功能")
    public ResponseEntity<RestResult> leavemsg(HttpServletRequest request , @Valid String orgi , @Valid LeaveMsg msg) {
		if(msg!=null && !StringUtils.isBlank(msg.getUserid())){
			leaveMsgRes.save(msg) ;
    	}
        return new ResponseEntity<>(new RestResult(RestResultType.OK), HttpStatus.OK);
    }
	
	/**
	 * 设置消息有用
	 * @param request
	 * @param userid	访客ID
	 * @p 分页信息
	 * @return
	 */
	@RequestMapping(value = "/message/useful")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取满意度调查")
    public ResponseEntity<RestResult> useful(HttpServletRequest request , @Valid String orgi , @Valid String id) {
		if(!StringUtils.isBlank(id)){
    		ChatMessage chatMessage = chatMessageRes.findById(id) ;
    		chatMessage.setUseful(true);
    		chatMessage.setUsetime(new Date());
    		chatMessageRes.save(chatMessage) ;
    	}
        return new ResponseEntity<>(new RestResult(RestResultType.OK), HttpStatus.OK);
    }
	
	/**
	 * 设置消息有用
	 * @param request
	 * @param userid	访客ID
	 * @p 分页信息
	 * @return
	 */
	@RequestMapping(value = "/message/unuseful")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取满意度调查")
    public ResponseEntity<RestResult> unuseful(HttpServletRequest request , @Valid String orgi , @Valid String id) {
		if(!StringUtils.isBlank(id)){
    		ChatMessage chatMessage = chatMessageRes.findById(id) ;
    		if(chatMessage!=null) {
	    		chatMessage.setUseful(false);
	    		chatMessage.setUsetime(new Date());
	    		chatMessageRes.save(chatMessage) ;
    		}
    	}
        return new ResponseEntity<>(new RestResult(RestResultType.OK), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/suggest/mobile/{appid}")
    @Menu(type = "im" , subtype = "index" , access = true)
    public ResponseEntity<RestResult> mobilesuggest(ModelMap map , HttpServletRequest request , HttpServletResponse response,  @PathVariable String appid ,@Valid String q ,@Valid String traceid,@Valid String aiid ,@Valid String p ,@Valid String exchange, @Valid String title ,@Valid String url, @Valid String skill, @Valid String id , @Valid String userid , @Valid String agent , @Valid String name , @Valid String email ,@Valid String phone,@Valid String ai,@Valid String orgi ,@Valid String product,@Valid String description,@Valid String imgurl,@Valid String pid,@Valid String purl) throws Exception {
    	return new ResponseEntity<>(new RestResult(RestResultType.OK ,!StringUtils.isBlank(appid) && !StringUtils.isBlank(q) ? OnlineUserUtils.suggest(q, orgi, userid,OnlineUserUtils.cousult(appid ,null, consultInviteRepository) , aiid , skill  ): null), HttpStatus.OK);
    }
	


	/**
	 * 返回当前用户访问店铺消息列表，支持分页，分页参数为 p=1&ps=20，默认分页尺寸为 20条每页
	 * @param map
	 * @param request
	 * @param userid 用户id
	 * @param shopname 店铺名模糊搜索
	 * @return
	 */
	@RequestMapping(value = "/msg/shoplist")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("返回当前用户访问店铺消息列表，支持分页，分页参数为 p=1&ps=20，默认分页尺寸为 20条每页")
	public ResponseEntity<RestResult> index(ModelMap map , HttpServletRequest request , @Valid final String userid,@Valid String shopname,@Valid String shoporgi){
		Page<AgentUser> page = null;
		if(StringUtils.isNotBlank(userid)){
			if(StringUtils.isBlank(shopname)){
				shopname = "";
			}else{
				shopname = "%" + shopname + "%";
			}
			page = agentUserRepository.findByManyTerm(userid,shopname,shoporgi, new PageRequest(super.getP(request), super.getPs(request) , Sort.Direction.DESC, "lastmessage")) ;
			if(page!=null && page.getContent()!=null && page.getContent().size() > 0) {
				for(AgentUser m : page.getContent()) {
					String orgi = m.getOrgi();
					if(StringUtils.isNotBlank(orgi)){
						m.setTenant(tenantRepository.findByTenantcode(orgi));
					}
				}
			}
		}
		return new ResponseEntity<>(new RestResult(RestResultType.OK, page), HttpStatus.OK);
	}

	/**
	 * 返回当前用户未读消息总数
	 * @param map
	 * @param request
	 * @param userid 用户id
	 * @param uid 会话id
	 * @return
	 */
	@RequestMapping(value = "/msg/unreadcount")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("返回当前用户未读消息总数")
	public ResponseEntity<RestResult> unreadcount(ModelMap map , HttpServletRequest request , @Valid final String userid, @Valid final String uid){
		Map<String,Object> resultmap = new HashMap<>();
		if(StringUtils.isNotBlank(userid)){
			if(StringUtils.isNotBlank(uid)){
				long count = agentUserRepository.sumUsernumByCreaterAndUserid(userid,uid) ;
				resultmap.put("count",count);
			}else{
				long count = agentUserRepository.sumUsernumByCreater(userid) ;
				resultmap.put("count",count);
			}
			resultmap.put("userid",userid);
			resultmap.put("uid",uid);
		}else{
			resultmap.put("count",0);
			resultmap.put("userid",userid);
			resultmap.put("uid",uid);
		}
		return new ResponseEntity<>(new RestResult(RestResultType.OK, resultmap), HttpStatus.OK);
	}

	/**
	 * 清空用户未读消息数量
	 * @param map
	 * @param request
	 * @param userid 用户id
	 * @param uid 会话id
	 * @return
	 */
	@RequestMapping(value = "/msg/resetunreadcount")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("重置当前用户未读消息数为0 uid为空时 清空该用户所有未读消息")
	public ResponseEntity<RestResult> resetunreadcount(ModelMap map , HttpServletRequest request , @Valid final String userid , @Valid final String uid){
		Map<String,Object> resultmap = new HashMap<>();
		if(StringUtils.isNotBlank(userid)){
			if(StringUtils.isNotBlank(uid)){
				//creater 对应userid
				List<AgentUser> list = agentUserRepository.findByCreaterAndUserid(userid,uid) ;
				if(list.size() > 0){
					for(AgentUser u : list){
						u.setUsernum(0);
					}
					agentUserRepository.save(list);
				}
			}else{
				//creater 对应userid
				List<AgentUser> list = agentUserRepository.findByCreater(userid) ;
				if(list.size() > 0){
					for(AgentUser u : list){
						u.setUsernum(0);
					}
					agentUserRepository.save(list);
				}
			}
		}
		return new ResponseEntity<>(new RestResult(RestResultType.OK, resultmap), HttpStatus.OK);
	}


	/**
	 *
	 * @param map
	 * @param request
	 * @param channel 渠道
	 * @param userid 用户ID （必填项）
	 * @param username 用户名
	 * @param appid 渠道ID（网站标识）
	 * @param orgi 租户ID
	 * @param producttitle 商品标题
	 * @param productlogo 商品logo
	 * @param producturl 商品url
	 * @param productprice 商品价格
	 * @param productid 商品id
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/product/send")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("发送商品链接类型信息")
	public ResponseEntity<RestResult> productSend(ModelMap map,HttpServletRequest request , @Valid String channel, @Valid String userid, @Valid String username , @Valid String appid , @Valid String orgi,@Valid String producttitle,@Valid String productlogo,@Valid String producturl,@Valid String productprice,@Valid String productid) throws IOException {
		Map<String,Object> resultMap = new HashMap<String,Object>();
		if( StringUtils.isNotBlank(producttitle) && !StringUtils.isBlank(userid)){

			resultMap.put("producttitle" ,producttitle) ;
			resultMap.put("productlogo" , StringUtils.isNotBlank(productlogo)?productlogo = URLDecoder.decode(productlogo,"UTF-8"):productlogo) ;
			resultMap.put("producturl" ,StringUtils.isNotBlank(producturl)?producturl = URLDecoder.decode(producturl,"UTF-8"):producturl) ;
			resultMap.put("productprice" ,productprice) ;
			resultMap.put("productid" ,productid) ;

			if(!StringUtils.isBlank(channel)){
				MessageUtils.sendProduct(producttitle , channel, userid , username , appid , orgi ,producttitle, productlogo, productprice, producturl, productid);
			}else{
				MessageUtils.sendProduct(producttitle , userid  ,producttitle, productlogo, productprice, producturl, productid ,appid , orgi);
			}
			return new ResponseEntity<>(new RestResult(RestResultType.OK, resultMap), HttpStatus.OK);
		}else{
			return new ResponseEntity<>(new RestResult(RestResultType.PRODUCT_TITLE_NOTEXIST, resultMap), HttpStatus.OK);
		}
	}


	@RequestMapping("/image/upload")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("图片或附件上传")
	public ModelAndView upload(ModelMap map,HttpServletRequest request , @RequestParam(value = "imgFile", required = false) MultipartFile imgFile , @Valid String channel, @Valid String userid, @Valid String username , @Valid String appid , @Valid String orgi, @Valid String paste) throws IOException {
		ModelAndView view = request(super.createRequestPageTempletResponse("/apps/im/upload")) ;
		UploadStatus upload = null ;
		String fileName = null ;
		if(imgFile!=null && imgFile.getOriginalFilename().lastIndexOf(".") > 0 && !StringUtils.isBlank(userid)){
			File uploadDir = new File(path , "upload");
			if(!uploadDir.exists()){
				uploadDir.mkdirs() ;
			}
			String fileid = UKTools.md5(imgFile.getBytes()) ;
			if(imgFile.getContentType()!=null && imgFile.getContentType().indexOf("image") >= 0){
				fileName = "upload/"+fileid+"_original" ;
				File imageFile = new File(path , fileName) ;
				FileCopyUtils.copy(imgFile.getBytes(), imageFile);
				String thumbnailsFileName = "upload/"+fileid ;
				UKTools.processImage(new File(path , thumbnailsFileName), imageFile) ;


				upload = new UploadStatus("0" , "/res/image.html?id="+thumbnailsFileName);

				String image =  "/res/image.html?id="+thumbnailsFileName ;
				if(request.getServerPort() == 80){
					image = "/res/image.html?id="+thumbnailsFileName;
				}else{
					image = "/res/image.html?id="+thumbnailsFileName;
				}
				if(paste == null){
					if(!StringUtils.isBlank(channel)){
						MessageUtils.uploadImage(image , fileid ,(int)imgFile.getSize() , imgFile.getName() , channel, userid , username , appid , orgi);
					}else{
						MessageUtils.uploadImage(image , fileid ,(int)imgFile.getSize() , imgFile.getName() , userid , appid , orgi);
					}
				}
			}else{

				String id = processAttachmentFile(imgFile, request);

				upload = new UploadStatus("0" , "/res/file.html?id="+id);
				String file =  "/res/file.html?id="+id ;
				if(request.getServerPort() == 80){
					file = "/res/file.html?id="+id;
				}else{
					file = "/res/file.html?id="+id;
				}
				File tempFile = new File(imgFile.getOriginalFilename());
				if(!StringUtils.isBlank(channel)){
					MessageUtils.uploadFile(file ,(int)imgFile.getSize() , tempFile.getName() , channel, userid , username , appid , orgi , id);
				}else{
					MessageUtils.uploadFile(file ,(int)imgFile.getSize() , tempFile.getName() , userid , id, appid , orgi);
				}
			}
		}else{
			upload = new UploadStatus("请选择文件");
		}
		map.addAttribute("upload", upload) ;
		return view ;
	}

	@RequestMapping("/voice/upload")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("语音文件上传")
	public ModelAndView voiceupload(ModelMap map,HttpServletRequest request , @RequestParam(value = "voice", required = false) MultipartFile voice , @Valid String channel, @Valid String userid, @Valid String username , @Valid String appid , @Valid String orgi, @Valid String duration) throws IOException {
		ModelAndView view = request(super.createRequestPageTempletResponse("/apps/im/upload")) ;
		UploadStatus upload = null ;
		String fileName = null ;
		if(voice!=null && voice.getSize() >0 && !StringUtils.isBlank(userid)){
			File uploadDir = new File(path , "upload");
			if(!uploadDir.exists()){
				uploadDir.mkdirs() ;
			}
			String fileid = UKTools.md5(voice.getBytes()) ;
			if(voice.getContentType()!=null && voice.getContentType().indexOf("audio") >= 0 && !StringUtils.isBlank(duration) && duration.matches("[\\d]{1,}")){
				fileName = "webim/voice/"+fileid+".mp3" ;
				File file = new File(path ,fileName) ;
				if(file.getParentFile().exists()){
					file.getParentFile().mkdirs() ;
				}
				FileUtils.writeByteArrayToFile(file , voice.getBytes());

				String url = "/res/voice.html?id="+fileName ;
				upload = new UploadStatus("0" ,url);
				if(!StringUtils.isBlank(channel)){
					MessageUtils.uploadVoice(url ,(int)file.length() , file.getName() , channel, userid , username , appid , orgi , fileid , Integer.parseInt(duration)/1000);
				}else{
					MessageUtils.uploadVoice(url ,(int)file.length() , file.getName() , userid , fileid , Integer.parseInt(duration)/1000, appid , orgi);
				}
			}
		}else{
			upload = new UploadStatus("请选择文件");
		}
		map.addAttribute("upload", upload) ;
		return view ;
	}

	private String processAttachmentFile(MultipartFile file , HttpServletRequest request) throws IOException{
		String id = null ;
		if(file.getSize() > 0){			//文件尺寸 限制 ？在 启动 配置中 设置 的最大值，其他地方不做限制
			String fileid = UKTools.md5(file.getBytes()) ;	//使用 文件的 MD5作为 ID，避免重复上传大文件
			if(!StringUtils.isBlank(fileid)){
				AttachmentFile attachmentFile = new AttachmentFile() ;
				attachmentFile.setCreater(super.getUser(request).getId());
				attachmentFile.setOrgi(super.getOrgi(request));
				attachmentFile.setOrgan(super.getUser(request).getOrgan());
				attachmentFile.setModel(UKDataContext.ModelType.WEBIM.toString());
				attachmentFile.setFilelength((int) file.getSize());
				if(file.getContentType()!=null && file.getContentType().length() > 255){
					attachmentFile.setFiletype(file.getContentType().substring(0 , 255));
				}else{
					attachmentFile.setFiletype(file.getContentType());
				}
				File uploadFile = new File(file.getOriginalFilename());
				if(uploadFile.getName()!=null && uploadFile.getName().length() > 255){
					attachmentFile.setTitle(uploadFile.getName().substring(0 , 255));
				}else{
					attachmentFile.setTitle(uploadFile.getName());
				}
				if(!StringUtils.isBlank(attachmentFile.getFiletype()) && attachmentFile.getFiletype().indexOf("image") >= 0){
					attachmentFile.setImage(true);
				}
				attachmentFile.setFileid(fileid);
				attachementRes.save(attachmentFile) ;
				FileUtils.writeByteArrayToFile(new File(path , "app/webim/"+fileid), file.getBytes());
				id = attachmentFile.getId();
			}
		}
		return id  ;
	}


	/**
	 * 获取租户下网站列表配置信息
	 * @param request
	 * @param orgi	租户ID
	 * @return
	 */
	@RequestMapping(value = "/getweblist")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取租户下网站列表配置信息")
	public ResponseEntity<RestResult> getweblist(HttpServletRequest request , @Valid String orgi) {
		List<Map<String,Object>> resultList = new ArrayList<>();
		if(StringUtils.isNotBlank(orgi)){
			Tenant tenant = tenantRepository.findByTenantcode(orgi);
			if(tenant != null && !tenant.isDatastatus()){
				List<SNSAccount> snsAccountList = snsAccountRes.findBySnstypeAndOrgi( UKDataContext.ChannelTypeEnum.WEBIM.toString() , orgi);
				if(snsAccountList.size() > 0 ){

					String schema = request.getScheme();
					int port = request.getServerPort();
					String hostname = request.getServerName();

					for(SNSAccount snsAccount : snsAccountList){
						Map<String,Object> map = new HashMap<>();
						map.put("snsAccount",snsAccount);
						CousultInvite coultInvite = consultInviteRepository.findBySnsaccountidAndOrgi(snsAccount.getSnsid(),snsAccount.getOrgi()) ;
						if(coultInvite!=null){
							map.put("cousultInvite",coultInvite);
							//map.addAttribute("skillList", getOrgans(request)) ;
							//map.addAttribute("agentList",getUsers(request)) ;
							map.put("appid",snsAccount.getSnsid());
							map.put("orgi",snsAccount.getOrgi());
							String scripturl = "";
							String texturl = schema + "://" + hostname + ":" + port + "/im/text/";

							//><#if inviteData??>&lt;script defer="true" src="${schema!''}://${hostname!''}:${import?string('###')}/im/${inviteData.snsaccountid!''}.html"&gt;&lt;/script&gt;<#else></#if>
							//<#if inviteData??>${schema!''}://${hostname!''}:${import?string('###')}/im/text/${inviteData.snsaccountid!''}.html<#else></#if>
						}
						resultList.add(map);
					}
					return new ResponseEntity<>(new RestResult(RestResultType.OK, resultList), HttpStatus.OK);
				}
			}
		}
		return new ResponseEntity<>(new RestResult(RestResultType.OK, ""), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getsyslist")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取平台下网站列表配置信息")
	public ResponseEntity<RestResult> getsyslist(HttpServletRequest request , @Valid String orgi) {
		List<Map<String,Object>> resultList = new ArrayList<>();
		List<SNSAccount> snsAccountList = snsAccountRes.findBySnstypeAndOrgi( UKDataContext.ChannelTypeEnum.WEBIM.toString() , UKDataContext.SYSTEM_ORGI.toString());
		if(snsAccountList.size() > 0 ){

			String schema = request.getScheme();
			int port = request.getServerPort();
			String hostname = request.getServerName();

			for(SNSAccount snsAccount : snsAccountList){
				Map<String,Object> map = new HashMap<>();
				map.put("snsAccount",snsAccount);
				CousultInvite coultInvite = consultInviteRepository.findBySnsaccountidAndOrgi(snsAccount.getSnsid(),snsAccount.getOrgi()) ;
				if(coultInvite!=null){
					map.put("cousultInvite",coultInvite);
					//map.addAttribute("skillList", getOrgans(request)) ;
					//map.addAttribute("agentList",getUsers(request)) ;
					map.put("appid",snsAccount.getSnsid());
					map.put("orgi",snsAccount.getOrgi());
					String scripturl = "";
					String texturl = schema + "://" + hostname + ":" + port + "/im/text/";

					//><#if inviteData??>&lt;script defer="true" src="${schema!''}://${hostname!''}:${import?string('###')}/im/${inviteData.snsaccountid!''}.html"&gt;&lt;/script&gt;<#else></#if>
					//<#if inviteData??>${schema!''}://${hostname!''}:${import?string('###')}/im/text/${inviteData.snsaccountid!''}.html<#else></#if>
				}
				resultList.add(map);
			}
			return new ResponseEntity<>(new RestResult(RestResultType.OK, resultList), HttpStatus.OK);
		}
		return new ResponseEntity<>(new RestResult(RestResultType.OK, ""), HttpStatus.OK);
	}

	/**
	 * 获取租户下某网站的信息
	 * @param request
	 * @param orgi	租户ID
	 * @return
	 */
	@RequestMapping(value = "/getweb")
	@Menu(type = "apps" , subtype = "webim" , access = true)
	@ApiOperation("获取租户下网站列表配置信息")
	public ResponseEntity<RestResult> getweb(HttpServletRequest request , @Valid String orgi,@Valid String appid) {
		Map<String,Object> resultMap = new HashMap<>();
		if(StringUtils.isNotBlank(orgi)){
			Tenant tenant = tenantRepository.findByTenantcode(orgi);
			if(tenant != null && !tenant.isDatastatus()){

				String sessionid = UKTools.getContextID(request.getSession().getId()) ;
				if(!StringUtils.isBlank(appid)){
					SNSAccount snsAccount = snsAccountRes.findBySnsid(appid);
					resultMap.put("snsAccount",snsAccount);
					CousultInvite invite = OnlineUserUtils.cousult(appid,orgi, consultInviteRepository);
					/*SystemConfig systemConfig = UKTools.getSystemConfig();
					if(systemConfig!=null && systemConfig.isEnablessl()) {
						view.addObject("schema","https") ;
						if(request.getServerPort() == 80) {
							view.addObject("port", 443) ;
						}else {
							view.addObject("port", request.getServerPort()) ;
						}
					}else {
						view.addObject("schema",request.getScheme()) ;
						view.addObject("port", request.getServerPort()) ;
					}*/
					String schema = request.getScheme();
					int port = request.getServerPort();
					String hostname = request.getServerName();

					resultMap.put("schema",schema);
					resultMap.put("port",port);
					resultMap.put("hostname",hostname);
					resultMap.put("appid",appid);

					resultMap.put("client",UKTools.getUUID());
					resultMap.put("sessionid",sessionid);

					String userip = UKTools.md5(request.getRemoteAddr()) ;
					Cookie ipsign = null;

					Cookie[] cookies = request.getCookies();//这样便可以获取一个cookie数组
					if(cookies!=null){
						for(Cookie cookie : cookies){
							if(cookie!=null && cookie.getName().equals("userip")){
								ipsign = cookie ;userip = cookie.getValue() ; break ;
							}
						}
					}
					if(ipsign == null) {
						ipsign = new Cookie("userip",userip);
						ipsign.setMaxAge(Integer.MAX_VALUE);
						//response.addCookie(ipsign);
					}

					//TODO
					/*view.addObject("ip", userip) ;

					view.addObject("mobile", CheckMobile.check(request.getHeader("User-Agent"))) ;

					view.addObject("name", name) ;
					view.addObject("email", email) ;
					view.addObject("phone", phone) ;
					if(!StringUtils.isBlank(userid)) {
						view.addObject("userid", userid) ;
					}
					view.addObject("id", id) ;

					view.addObject("product", product) ;
					view.addObject("description", description) ;
					view.addObject("imgurl", imgurl) ;
					view.addObject("pid", pid) ;
					view.addObject("purl", purl) ;

					view.addObject("data", data) ;*/

					if(invite!=null){
						orgi = invite.getOrgi() ;
						resultMap.put("inviteData",invite);
						//view.addObject("inviteData", invite);
						resultMap.put("appid",appid);
						resultMap.put("orgi",invite.getOrgi());



						if(!StringUtils.isBlank(invite.getAiid())) {
							resultMap.put("aiid",invite.getAiid());
						}
						//记录用户行为日志
						UserHistory userHistory = new UserHistory() ;
						String url = request.getHeader("referer");
						if(!StringUtils.isBlank(url)){
							if(url.length() >255){
								userHistory.setUrl(url.substring( 0 , 255));
							}else{
								userHistory.setUrl(url);
							}
							userHistory.setReferer(userHistory.getUrl());
						}
						userHistory.setParam(UKTools.getParameter(request));
						if(userHistory!=null){
							userHistory.setMaintype("im");
							userHistory.setSubtype("point");
							userHistory.setName("online");
							userHistory.setAdmin(false);
							userHistory.setAccessnum(true);
						}
						//TODO
						/*User imUser = super.getIMUser(request , userid, name , appid , invite.getOrgi()) ;
						if(imUser!=null){
							userHistory.setCreater(imUser.getId());
							userHistory.setUsername(imUser.getUsername());
							userHistory.setOrgi(orgi);
						}
						if(!StringUtils.isBlank(title)){
							if(title.length() > 255){
								userHistory.setTitle(title.substring(0 , 255));
							}else{
								userHistory.setTitle(title);
							}
						}*/
						userHistory.setOrgi(invite.getOrgi());
						userHistory.setAppid(appid);
						userHistory.setSessionid(sessionid);

						String ip = UKTools.getIpAddr(request);
						userHistory.setHostname(ip);
						userHistory.setIp(ip);
						IP ipdata = IPTools.getInstance().findGeography(ip);
						userHistory.setCountry(ipdata.getCountry());
						userHistory.setProvince(ipdata.getProvince());
						userHistory.setCity(ipdata.getCity());
						userHistory.setIsp(ipdata.getIsp());

						BrowserClient client = UKTools.parseClient(request) ;
						userHistory.setOstype(client.getOs());
						userHistory.setBrowser(client.getBrowser());
						userHistory.setMobile(CheckMobile.check(request.getHeader("User-Agent")) ? "1" : "0");

						if(invite.isSkill()){
							/***
							 * 查询 技能组 ， 缓存？
							 */
							//view.addObject("skillList", OnlineUserUtils.organ(orgi, ipdata , invite,true))  ;
							resultMap.put("skillList",OnlineUserUtils.organ(orgi, ipdata , invite,true));

							/**
							 * 查询坐席 ， 缓存？
							 */
							//view.addObject("agentList", OnlineUserUtils.agents(orgi,true))  ;
							resultMap.put("agentList",OnlineUserUtils.agents(orgi,true));
						}
						resultMap.put("traceid", userHistory.getId());
						//view.addObject("traceid", userHistory.getId()) ;
						if(invite.isRecordhis()){
							UKTools.published(userHistory);
						}
						resultMap.put("pointAd", UKTools.getPointAdv(UKDataContext.AdPosEnum.POINT.toString(),orgi));
						resultMap.put("inviteAd", UKTools.getPointAdv(UKDataContext.AdPosEnum.INVITE.toString(),orgi));
						//view.addObject("pointAd", UKTools.getPointAdv(UKDataContext.AdPosEnum.POINT.toString(),orgi)) ;
						//view.addObject("inviteAd", UKTools.getPointAdv(UKDataContext.AdPosEnum.INVITE.toString(),orgi)) ;
					}
				}

				return new ResponseEntity<>(new RestResult(RestResultType.OK, resultMap), HttpStatus.OK);
			}
		}
		return new ResponseEntity<>(new RestResult(RestResultType.OK, ""), HttpStatus.OK);
	}
}