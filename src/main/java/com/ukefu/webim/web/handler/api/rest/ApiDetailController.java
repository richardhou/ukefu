package com.ukefu.webim.web.handler.api.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ukefu.util.Menu;
import com.ukefu.webim.service.es.WorkOrdersRepository;
import com.ukefu.webim.util.RestResult;
import com.ukefu.webim.util.RestResultType;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.WorkOrders;

@RestController
@RequestMapping("/api/workorders/detail")
@Api(value = "工单服务", description = "查看工单详细信息，并能够处理工单信息")
public class ApiDetailController extends Handler{

	@Autowired
	private WorkOrdersRepository workOrdersRepository;

	/**
	 * 返回工单列表，支持分页，支持关键词查询，分页参数为 p=1&ps=50，默认分页尺寸为 20条每页
	 * @param request
	 * @param q	
	 * @return
	 */
	@RequestMapping( method = RequestMethod.GET)
	@Menu(type = "apps" , subtype = "user" , access = true)
	@ApiOperation("返回所有已关闭的工单列表")
    public ResponseEntity<RestResult> list(HttpServletRequest request ,@Valid String id) {
		RestResult result = new RestResult(RestResultType.OK) ; 
		if(!StringUtils.isBlank(id)){
			WorkOrders workOrders = workOrdersRepository.findByIdAndOrgi(id,super.getOrgi(request)) ;
			if(workOrders==null){
				result.setStatus(RestResultType.WORKORDERS_NOTEXIST);
			}else{
				result.setData(workOrders);
			}
		}
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}