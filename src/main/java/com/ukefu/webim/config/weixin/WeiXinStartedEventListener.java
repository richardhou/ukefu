package com.ukefu.webim.config.weixin;

import com.ukefu.webim.util.weixin.config.WechatOpenConfiguration;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.ukefu.core.UKDataContext;
import com.ukefu.webim.service.repository.InstructionRepository;
import com.ukefu.webim.service.repository.SNSAccountRepository;
import com.ukefu.webim.util.weixin.config.WechatMpConfiguration;

@Component
public class WeiXinStartedEventListener implements ApplicationListener<ContextRefreshedEvent> {
	
	
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
    	if(UKDataContext.getContext() == null){
    		UKDataContext.setApplicationContext(event.getApplicationContext());
    	}
    	WechatMpConfiguration.getInstance().initSNSAccount(event.getApplicationContext().getBean(SNSAccountRepository.class) , event.getApplicationContext().getBean(InstructionRepository.class));
		WechatOpenConfiguration.getInstance().initSNSAccount(event.getApplicationContext().getBean(SNSAccountRepository.class) , event.getApplicationContext().getBean(InstructionRepository.class));
    }
}