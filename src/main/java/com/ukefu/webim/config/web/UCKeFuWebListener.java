package com.ukefu.webim.config.web;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.ukefu.core.UKDataContext;

@WebListener
public class UCKeFuWebListener implements ServletContextListener{

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		if(UKDataContext.udpService!=null) {
			UKDataContext.udpService.shutdown();
		}
		UKDataContext.group.shutdownGracefully() ;
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		
	}
}
