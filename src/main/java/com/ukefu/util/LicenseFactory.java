package com.ukefu.util;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;



public abstract class LicenseFactory{   
    public static final String KEY_ALGORITHM = "RSA";   
    public static final String SIGNATURE_ALGORITHM = "MD5withRSA";   
  
    private static final String PUBLIC_KEY = "RSAPublicKey";   
    private static final String PRIVATE_KEY = "RSAPrivateKey";  

    public static final String key = "30819F300D06092A864886F70D010101050003818D003081890281810099D4A69AC23307A5A9ABC696532C18AF1EAD302F4E16AE9B31845F297D02FD146A0B9828F6E480A3851CB7DE5F047D1F424502E64382891C736595E8BB9F0A2B371965234FE0470F4EFF464C023391AFAAD7D9CA037BA8B0910ABA4BC7D23A484E37835D566B96A075C1009985F8B4E760D519B67F990411393DD95BBC5328250203010001" ;
    /**  
     * 用私钥对信息生成数字签名  
     *   
     * @param data  
     *            加密数据  
     * @param privateKey  
     *            私钥  
     *   
     * @return  
     * @throws Exception  
     */  
    public static String sign(byte[] data, String privateKey) throws Exception {   
        // 解密由base64编码的私钥   
        byte[] keyBytes = decryptBASE64(privateKey);   
  
        // 构造PKCS8EncodedKeySpec对象   
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);   
  
        // KEY_ALGORITHM 指定的加密算法   
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);   
  
        // 取私钥匙对象   
        PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);   
  
        // 用私钥对信息生成数字签名   
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);   
        signature.initSign(priKey);   
        signature.update(data);   
  
        return encryptBASE64(signature.sign());   
    }   
  
    /**  
     * 校验数字签名  
     *   
     * @param data  
     *            加密数据  
     * @param publicKey  
     *            公钥  
     * @param sign  
     *            数字签名  
     *   
     * @return 校验成功返回true 失败返回false  
     * @throws Exception  
     *   
     */  
    public static boolean verify(byte[] data, String publicKey, String sign)   
            throws Exception {   
  
        // 解密由base64编码的公钥   
        byte[] keyBytes = decryptBASE64(publicKey);   
  
        // 构造X509EncodedKeySpec对象   
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);   
  
        // KEY_ALGORITHM 指定的加密算法   
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);   
  
        // 取公钥匙对象   
        PublicKey pubKey = keyFactory.generatePublic(keySpec);   
  
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);   
        signature.initVerify(pubKey);   
        signature.update(data);   
  
        // 验证签名是否正常   
        return signature.verify(decryptBASE64(sign));   
    }  
  
    /**  
     * 初始化密钥  
     *   
     * @return  
     * @throws Exception  
     */  
    public static Map<String, Object> initKey() throws Exception {   
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);   
        keyPairGen.initialize(1024);   
  
        KeyPair keyPair = keyPairGen.generateKeyPair();   
  
        // 公钥   
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();   
  
        // 私钥   
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();   
  
        Map<String, Object> keyMap = new HashMap<String, Object>(2);   
  
        keyMap.put(PUBLIC_KEY, publicKey);   
        keyMap.put(PRIVATE_KEY, privateKey);   
        return keyMap;   
    }     
    
    /**  
     * 取得私钥  
     *   
     * @param keyMap  
     * @return  
     * @throws Exception  
     */  
    public static String getPrivateKey(Map<String, Object> keyMap)   
            throws Exception {   
        Key key = (Key) keyMap.get(PRIVATE_KEY);   
  
        return encryptBASE64(key.getEncoded());   
    }   
  
    /**  
     * 取得公钥  
     *   
     * @param keyMap  
     * @return  
     * @throws Exception  
     */  
    public static String getPublicKey(Map<String, Object> keyMap)   
            throws Exception {   
        Key key = (Key) keyMap.get(PUBLIC_KEY);   
        return encryptBASE64(key.getEncoded());   
    }   
    
    /**  
     * BASE64解密  
     *   
     * @param key  
     * @return  
     * @throws Exception  
     */  
    public static byte[] decryptBASE64(String key) throws Exception {   
        return hex2BYTE(key);   
    }   
  
    /**  
     * BASE64加密  
     *   
     * @param key  
     * @return  
     * @throws Exception  
     */  
    public static String encryptBASE64(byte[] key) throws Exception {   
        return byteHEX(key);   
    }  
    
    public static byte[] hex2BYTE(String hex) throws IllegalArgumentException {    
        if (hex.length() % 2 != 0) {    
            throw new IllegalArgumentException();    
        }    
        char[] arr = hex.toCharArray();    
        byte[] b = new byte[hex.length() / 2];    
        for (int i = 0, j = 0, l = hex.length(); i < l; i++, j++) {    
            String swap = "" + arr[i++] + arr[i];    
            int byteint = Integer.parseInt(swap, 16) & 0xFF;    
            b[j] = new Integer(byteint).byteValue();    
        }    
        return b;    
    }  
    
    /**
     * 编码byte
     * @param ib
     * @return
     */
    public static String byteHEX(byte[] ib) {
        char[] Digit = { '0','1','2','3','4','5','6','7','8','9',
        'A','B','C','D','E','F' };
        StringBuffer strb = new StringBuffer() ;
        for(byte data: ib){
        	char [] ob = new char[2];
	        ob[0] = Digit[(data >>> 4) & 0X0F];
	        ob[1] = Digit[data & 0X0F];
	        strb.append(ob);
        }
        return strb.toString();
    }
}  