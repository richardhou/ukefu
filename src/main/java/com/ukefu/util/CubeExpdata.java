package com.ukefu.util;

import java.util.List;

import com.ukefu.webim.web.model.Cube;
import com.ukefu.webim.web.model.CubeLevel;
import com.ukefu.webim.web.model.CubeMeasure;
import com.ukefu.webim.web.model.CubeMetadata;
import com.ukefu.webim.web.model.Dimension;
import com.ukefu.webim.web.model.PublishedCube;

public class CubeExpdata implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PublishedCube pubCube ;
	private Cube cube ;
	private List<CubeMetadata> metadata = null;
	private List<Dimension> dimension = null;
	private List<CubeLevel> levels = null;
	private List<CubeMeasure> measure = null;
	
	public CubeExpdata() {}
	
	public CubeExpdata(Cube cube , List<CubeMetadata> metadata ,
			List<Dimension> dimension , List<CubeLevel> levels , 
			List<CubeMeasure> measure) {
		this.cube = cube ;
		this.measure = measure;
		this.metadata = metadata ;
		this.dimension = dimension ;
		this.levels = levels ;
	}
	
	public Cube getCube() {
		return cube;
	}
	public void setCube(Cube cube) {
		this.cube = cube;
	}
	public List<CubeMetadata> getMetadata() {
		return metadata;
	}
	public void setMetadata(List<CubeMetadata> metadata) {
		this.metadata = metadata;
	}
	public List<Dimension> getDimension() {
		return dimension;
	}
	public void setDimension(List<Dimension> dimension) {
		this.dimension = dimension;
	}
	public List<CubeLevel> getLevels() {
		return levels;
	}
	public void setLevels(List<CubeLevel> levels) {
		this.levels = levels;
	}
	public List<CubeMeasure> getMeasure() {
		return measure;
	}
	public void setMeasure(List<CubeMeasure> measure) {
		this.measure = measure;
	}

	public PublishedCube getPubCube() {
		return pubCube;
	}

	public void setPubCube(PublishedCube pubCube) {
		this.pubCube = pubCube;
	}
}
