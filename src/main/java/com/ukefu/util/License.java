package com.ukefu.util;

import java.io.Serializable;
import java.util.Date;
public class License implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -218343675394406087L;
	private String id;
	private Date createdate ;	//创建时间
	private String title = "";//授权客户名称
	private String product = "1" ;//授权功能列表：weixin,xiaoe,webim,contacts...
	private String version = "1"; //版本 1:试用版   2:正式版
	private String begin = "";//开始时间
	private String end  = "";//结束时间
	private String type = "2";//类型：1:临时授权 ， 2：永久授权
	private int    num = 0 ;//授权数量
	private int nodenum = 1; //授权服务器数量
	private String cpuid ;
	private String funtion = "server";//授权模式 ： 分布式 ， 集群
	private String sign ;//签名
	private int cores=1;//核心数量
	private String api="0";//是否有API授权
	
	public License(){}
	public License(String title ,String product , String version , String begin,String end , String type , String num ,String nodenum, String cpuid , String function , String sign){
		this.title = title ;
		this.product = product!=null?product: this.product;
		this.version = version!=null?version:this.version ;
		this.begin = begin ;
		this.end = end ;
		this.type = type!=null?type:this.type ;
		this.num = num!=null&&num.matches("[\\d]*")?Integer.parseInt(num):this.num ;
		this.nodenum = nodenum!=null&&nodenum.matches("[\\d]*")?Integer.parseInt(nodenum):this.nodenum ;
		this.cpuid = cpuid ;
		this.funtion = function!=null?function:this.funtion ;
		this.sign = sign ;
	}
	
	public License(String title ,String product , String version , String begin,String end , String type , String num ,String nodenum, String cpuid , String function , String sign , String cores , String api){
		this(title, product, version, begin, end, type, num, nodenum, cpuid, function, sign);
		this.cores=cores!=null&&cores.matches("[\\d]*")?Integer.parseInt(cores):this.cores ;
		this.api=api;
	}
	/**
	 * 
	 */
	public String toString(){
		StringBuffer strb = new StringBuffer() ;
		strb.append(title).append(",").append(product).append(",").append(version).append(",").append(begin).append(",").append(end).append(",").append(type).append(",").append(num).append(",").append(nodenum).append(",").append(cpuid).append(",").append(funtion).append(",").append(cores).append(",").append(api) ;
		return strb.toString() ;
	}
	public String getTitle() {
		return title;
	}
	public String getProduct() {
		return product;
	}
	public String getVersion() {
		return version;
	}
	public String getBegin() {
		return begin;
	}
	public String getEnd() {
		return end;
	}
	public String getType() {
		return type;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getCores() {
		return cores;
	}
	public String getApi() {
		return api;
	}
	public int getNum() {
		return num;
	}
	public String getCpuid() {
		return cpuid;
	}
	public String getFuntion() {
		return funtion;
	}
	public String getSign() {
		return sign;
	}
	public int getNodenum() {
		return nodenum;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public void setBegin(String begin) {
		this.begin = begin;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public void setNodenum(int nodenum) {
		this.nodenum = nodenum;
	}
	public void setCpuid(String cpuid) {
		this.cpuid = cpuid;
	}
	public void setFuntion(String funtion) {
		this.funtion = funtion;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public void setCores(int cores) {
		this.cores = cores;
	}
	public void setApi(String api) {
		this.api = api;
	}
}
