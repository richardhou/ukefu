package com.ukefu.util;

import java.util.List;

import com.ukefu.webim.web.model.ColumnProperties;
import com.ukefu.webim.web.model.PublishedReport;
import com.ukefu.webim.web.model.Report;
import com.ukefu.webim.web.model.ReportFilter;
import com.ukefu.webim.web.model.ReportModel;

public class ReportExpdata implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PublishedReport pubReport;
	private Report report ;
	private List<ReportModel> reportModels = null;
	private List<ReportFilter> reportFilters = null;
	private List<ColumnProperties> columnProperties = null;
	
	public ReportExpdata() {}
	
	public ReportExpdata(Report report , List<ReportModel> reportModels ,
			List<ReportFilter> reportFilters) {
		this.report = report ;
		this.reportModels = reportModels;
		this.reportFilters = reportFilters ;
	}

	public PublishedReport getPubReport() {
		return pubReport;
	}

	public void setPubReport(PublishedReport pubReport) {
		this.pubReport = pubReport;
	}

	public Report getReport() {
		return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	public List<ReportModel> getReportModels() {
		return reportModels;
	}

	public void setReportModels(List<ReportModel> reportModels) {
		this.reportModels = reportModels;
	}

	public List<ReportFilter> getReportFilters() {
		return reportFilters;
	}

	public void setReportFilters(List<ReportFilter> reportFilters) {
		this.reportFilters = reportFilters;
	}

	public List<ColumnProperties> getColumnProperties() {
		return columnProperties;
	}

	public void setColumnProperties(List<ColumnProperties> columnProperties) {
		this.columnProperties = columnProperties;
	}
}
