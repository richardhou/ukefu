package com.ukefu.util.task.export;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.UCKeFuTime;
import com.ukefu.util.UKTools;
import com.ukefu.util.extra.DataExchangeInterface;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.web.model.MetadataTable;
import com.ukefu.webim.web.model.QualityResultExporter;
import com.ukefu.webim.web.model.QualityResultItem;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.TableProperties;
import com.ukefu.webim.web.model.UKeFuDic;
import com.ukefu.webim.web.model.User;

@SuppressWarnings("deprecation")
public class ExcelExporterQualityResultProcess {
	private XSSFWorkbook  wb; 
	private Sheet sheet; 
	private CellStyle firstStyle = null ;
	
	private int rowNum ;
	private int rowheadNum ;
	
	private List<Map<String ,Object>> values ;
	private List<QualityResultExporter> showitemList ;
	private List<QualityResultItem> tabooList ;
	private MetadataTable table ;
	private OutputStream output ;
	private Row titleRow ;
	
	private Map<String, List<String>> qcitemidsMap = new HashMap<String, List<String>>();
	private String tpid;
	
	/**
	 * 
	 * @param tpMap 业务数据
	 * @param showitemMap 质检项表头
	 * @param rowheadNumMap 表头行数
	 * @param table 元数据
	 * @param output
	 */
	public ExcelExporterQualityResultProcess(Map<String, List<Map<String,Object>>> tpMap ,Map<String, List<QualityResultExporter>> showitemMap,Map<String, List<QualityResultItem>> tabooMap,Map<String, Integer> rowheadNumMap, MetadataTable table , OutputStream output) {
		if (tpMap != null && tpMap.size() > 0) {
			wb = new XSSFWorkbook();
			sheet = wb.createSheet();
			firstStyle = createFirstCellStyle();
			for(String tp : tpMap.keySet()){ //一个模板id创建一组数据表
				if (tpMap.get(tp)!=null && showitemMap.get(tp) != null && rowheadNumMap.get(tp)!=null) {
					this.tpid = tp;
					this.values = tpMap.get(tp) ;
					this.showitemList = showitemMap.get(tp);
					this.tabooList = tabooMap.get(tp);
					this.rowheadNum=rowheadNumMap.get(tp);
					this.table = table ;
					this.output = output;
					createHead() ;
					createContent();
				}
			}
		}
		
	}
	public void process() throws IOException{
		if(table!=null){
			wb.write(this.output);
		}
	}
	
	/**
	 * 构建头部
	 */
	private void createHead(){
		if (rowheadNum>0) {
			Map<Integer, QualityResultItem> posVal2Map = new HashMap<>();
			Map<Integer, QualityResultItem> posVal3Map = new HashMap<>();
			for(int r=0;r<rowheadNum;r++){
				titleRow = sheet.createRow(rowNum+r);
				int inx = 0 ;
				if(table!=null && table.getTableproperty()!=null){
					for(TableProperties tp : table.getTableproperty()){
						if(tp.isUnresize() == false) {
							Cell cell2 = titleRow.createCell(inx++); 
							cell2.setCellStyle(firstStyle); 
							if (r==0) {
								cell2.setCellValue(new XSSFRichTextString(tp.getName()));
							}
							if (r==(rowheadNum-1) && rowheadNum>1) {//创建最后一行的时候，如果大于2行，则需要行合并
								sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum+r,inx-1,inx-1));
							}
							if("qualityuser".equals(tp.getFieldname().toLowerCase())){
								cell2.setCellValue(new XSSFRichTextString("质检员姓名"));
								Cell cell3 = titleRow.createCell(inx++); 
								cell3.setCellStyle(firstStyle); 
								if (r==0) {
									cell3.setCellValue(new XSSFRichTextString("质检员工号"));
								}
								if (r==(rowheadNum-1) && rowheadNum>1) {//创建最后一行的时候，如果大于2行，则需要行合并
									sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum+r,inx-1,inx-1));
								}
							}
							if("userid".equals(tp.getFieldname().toLowerCase())){
								cell2.setCellValue(new XSSFRichTextString("话务员姓名"));
								Cell cell3 = titleRow.createCell(inx++); 
								cell3.setCellStyle(firstStyle); 
								if (r==0) {
									cell3.setCellValue(new XSSFRichTextString("话务员工号"));
								}
								if (r==(rowheadNum-1) && rowheadNum>1) {//创建最后一行的时候，如果大于2行，则需要行合并
									sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum+r,inx-1,inx-1));
								}
							}
						}
					}
					for(TableProperties tp : table.getTableproperty()){
						if(tp.isUnresize() == true) {
							List<SysDic> dicList = UKeFuDic.getInstance().getDic("com.dic.xiaoe.input") ;
							for(SysDic sysDic : dicList) {
								Cell cell2 = titleRow.createCell(inx++); 
								cell2.setCellStyle(firstStyle); 
								if (r==0) {
									cell2.setCellValue(new XSSFRichTextString(sysDic.getName()));
								}
								if (r==(rowheadNum-1)) {
									sheet.addMergedRegion(new CellRangeAddress(0,r,inx,inx));
								}
							}
						}
					}
				}
				List<Integer> pos1list = new ArrayList<>();//一级质检项位置，用于单元格合并
				List<Integer> pos2list = new ArrayList<>();//二级质检项位置，用于单元格合并
				List<Integer> pos3list = new ArrayList<>();//三级质检项位置，用于单元格合并
				pos1list.add(inx);//表头行中质检项开始位置(列)
				pos2list.add(inx);//表头行中质检项开始位置(列)
				pos3list.add(inx);//表头行中质检项开始位置(列)
				if (showitemList != null && showitemList.size() > 0) {
					List<String> qcitemids = new ArrayList<>();//数据内容需要展示的质检项id，后面根据该id获取展示内容
					for(QualityResultExporter showe: showitemList){
						int pos = inx+showe.getColinx();
						int pos2 = inx ;
						int pos3 = inx ;
						pos1list.add(pos);
						Cell cell2 = titleRow.createCell(inx++); 
						cell2.setCellStyle(firstStyle); 
						if (r==0) {
							cell2.setCellValue(new XSSFRichTextString(showe.getItem().getName()));
						}
						while (inx!=(pos)) {//单个一级质检项可以定下占用的单元格范围
							Cell cell3 = titleRow.createCell(inx++); 
							cell3.setCellStyle(firstStyle); 
							cell3.setCellValue(new XSSFRichTextString(""));
						}
						boolean isdata1 = true;//一级质检项是否作为展示数据
						if (showe.getChildren() != null && showe.getChildren().size() > 0) {
							isdata1 = false;
							List<QualityResultExporter> showitem2List = showe.getChildren();
							for(QualityResultExporter showe2 : showitem2List){
								boolean isdata2 = true;//二级质检项是否作为展示数据
								posVal2Map.put(pos2, showe2.getItem());
								pos2list.add(pos2=pos2+showe2.getColinx());
								if (showe2.getChildren() != null && showe2.getChildren().size() > 0) {
									isdata2 = false;
									List<QualityResultExporter> showitem3List = showe2.getChildren();
									for(QualityResultExporter showe3 : showitem3List){
										posVal3Map.put(pos3, showe3.getItem());
										pos3list.add(pos3);
										if (r==0) {
											qcitemids.add(showe3.getItem().getItemid());
										}
										pos3 = pos3+showe3.getColinx();
									}
								}
								if (isdata2 && r==0) {
									qcitemids.add(showe2.getItem().getItemid());
								}
							}
						}
						if (isdata1 && r==0) {
							qcitemids.add(showe.getItem().getItemid());
						}
					}
					if (r==0) {
						qcitemidsMap.put(this.tpid, qcitemids);
					}
				}
				if (pos1list != null && pos1list.size() > 0 && r==0) {
					for(int i = 0;i<pos1list.size()-1;i++){
						if (pos1list.get(i)<pos1list.get(i+1)-1) {
							sheet.addMergedRegion(new CellRangeAddress(rowNum+r,rowNum+r,pos1list.get(i),pos1list.get(i+1)-1));
						}
					}
				}
				if (pos2list != null && pos2list.size() > 0 && r==1) {
					for(int i = 0;i<pos2list.size()-1;i++){
						if (posVal2Map != null && posVal2Map.size() > 0 && posVal2Map.get(pos2list.get(i))!=null) {
							titleRow.getCell(pos2list.get(i)).setCellValue(new XSSFRichTextString(posVal2Map.get(pos2list.get(i)).getName()));
						}
						if (pos2list.get(i)<pos2list.get(i+1)-1) {
							sheet.addMergedRegion(new CellRangeAddress(rowNum+r,rowNum+r,pos2list.get(i),pos2list.get(i+1)-1));
						}
					}
				}
				if (pos3list != null && pos3list.size() > 0 && r==2) {
					for(int i = 0;i<pos3list.size();i++){
						if (posVal3Map != null && posVal3Map.size() > 0 && posVal3Map.get(pos3list.get(i))!=null) {
							titleRow.getCell(pos3list.get(i)).setCellValue(new XSSFRichTextString(posVal3Map.get(pos3list.get(i)).getName()));
						}
					}
				}
				if (tabooList!=null && tabooList.size() >0) {
					for(int i=0;i< tabooList.size() ;i++){
						Cell cell2 = titleRow.createCell(inx++); 
						cell2.setCellStyle(firstStyle); 
						if (r==0 && i==0) {
							cell2.setCellValue(new XSSFRichTextString("禁忌项"));
						}else if(r==1){
							cell2.setCellValue(new XSSFRichTextString(tabooList.get(i).getName()));
							if (qcitemidsMap.get(this.tpid)!=null) {
								List<String> qcitemids = qcitemidsMap.get(this.tpid);
								if (qcitemids != null) {
									qcitemids.add(tabooList.get(i).getItemid());
								}
							}
						}
					}
					if (r==0) {
						sheet.addMergedRegion(new CellRangeAddress(rowNum+r,rowNum+r,inx-tabooList.size(),inx-1));
					}
				}
				
				Cell cellpass = titleRow.createCell(inx++); 
				cellpass.setCellStyle(firstStyle); 
				if (r==0) {
					cellpass.setCellValue(new XSSFRichTextString("是否合格"));
				}
				if (r==(rowheadNum-1) && rowheadNum>1) {
					sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum+r,inx-1,inx-1));
				}
				Cell cellscore = titleRow.createCell(inx++); 
				cellscore.setCellStyle(firstStyle); 
				if (r==0) {
					cellscore.setCellValue(new XSSFRichTextString("总分"));
				}
				if (r==(rowheadNum-1) && rowheadNum>1) {
					sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum+r,inx-1,inx-1));
				}
				Cell cellre = titleRow.createCell(inx++); 
				cellre.setCellStyle(firstStyle); 
				if (r==0) {
					cellre.setCellValue(new XSSFRichTextString("质检备注"));
				}
				if (r==(rowheadNum-1) && rowheadNum>1) {
					sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum+r,inx-1,inx-1));
				}
				Cell cellad = titleRow.createCell(inx++); 
				cellad.setCellStyle(firstStyle); 
				if (r==0) {
					cellad.setCellValue(new XSSFRichTextString("优点评语"));
				}
				if (r==(rowheadNum-1) && rowheadNum>1) {
					sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum+r,inx-1,inx-1));
				}
				Cell cellqc = titleRow.createCell(inx++); 
				cellqc.setCellStyle(firstStyle); 
				if (r==0) {
					cellqc.setCellValue(new XSSFRichTextString("QA评语"));
				}
				if (r==(rowheadNum-1) && rowheadNum>1) {
					sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum+r,inx-1,inx-1));
				}
				Cell cellim = titleRow.createCell(inx++); 
				cellim.setCellStyle(firstStyle); 
				if (r==0) {
					cellim.setCellValue(new XSSFRichTextString("改进评语"));
				}
				if (r==(rowheadNum-1) && rowheadNum>1) {
					sheet.addMergedRegion(new CellRangeAddress(rowNum,rowNum+r,inx-1,inx-1));
				}
			}
			rowNum +=(rowheadNum-1);
		}
		
		rowNum ++ ;
	}
	
	private CellStyle createContentStyle(){
		CellStyle cellStyle = wb.createCellStyle(); 
		
		cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER); // 指定单元格居中对齐 
		cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);// 指定单元格垂直居中对齐 
		cellStyle.setWrapText(false);// 指定单元格自动换行 

		// 设置单元格字体 
		Font font = wb.createFont(); 
		//font.setFontName("微软雅黑"); 
		font.setFontHeight((short) 200); 
		cellStyle.setFont(font); 
		cellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		return cellStyle ;
	}
	
	/**
	 * 首列样式
	 * @return
	 */
	private CellStyle createFirstCellStyle(){
		CellStyle cellStyle = baseCellStyle();
		Font font = wb.createFont();
		//font.setFontName("微软雅黑"); 
		font.setFontHeight((short) 180);
		cellStyle.setFont(font);
		
		cellStyle.setWrapText(false);
		
		cellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		
	
		return cellStyle;
	}
	
	
	private synchronized void createContent(){
		CellStyle cellStyle = createContentStyle() ;
		if(table!=null && table.getTableproperty()!=null){
			for(Map<String , Object> value:values){
				Row row2 = sheet.createRow(rowNum);
				List<ExportData> tempExportDatas = new ArrayList<ExportData>();
				int cols = 0 ;
				for(TableProperties tp : table.getTableproperty()){
					if(tp.isUnresize() == false) {
						Cell cell2 = row2.createCell(cols++); 
						cell2.setCellStyle(cellStyle); 
						if(value.get(tp.getFieldname().toLowerCase())!=null){
							if ("datetime".equals(tp.getDatatypename())) {
								sheet.setColumnWidth(cols-1, 16*256);
							}
							if(tp.isModits()) {
								@SuppressWarnings("unchecked")
								List<String> list = (List<String>)value.get(tp.getFieldname().toLowerCase());
								if(list.size()>0) {
									cell2.setCellValue(new XSSFRichTextString(list.remove(0)));
								}
								ExportData expData = new ExportData(tp , list) ;
								if(list.size()>0) {
									tempExportDatas.add(expData) ;
									if(list.size() > expData.getMaxcols()) {
										expData.setMaxcols(list.size());
									}
								}
							}else if(tp.isSeldata()){
								SysDic sysDic = UKeFuDic.getInstance().getDicItem(String.valueOf(value.get(tp.getFieldname().toLowerCase()))) ;
								if(sysDic!=null) {
									cell2.setCellValue(new XSSFRichTextString(sysDic.getName()));
								}else {
									List<SysDic> dicItemList = UKeFuDic.getInstance().getSysDic(tp.getSeldatacode());
									if(dicItemList!=null && dicItemList.size() > 0) {
										for(SysDic dicItem : dicItemList) {
											String s = "";
											Object obj = value.get(tp.getFieldname().toLowerCase());
											if(obj instanceof Boolean) {
												s = (Boolean)obj?"1":"0";
											}else {
												s= String.valueOf(value.get(tp.getFieldname().toLowerCase()));
											}
											if(dicItem.getCode().equals(s)) {
												cell2.setCellValue(new XSSFRichTextString(dicItem.getName())); break ;
											}
										}
									}
								}
							}else if(tp.isReffk() && !StringUtils.isBlank(tp.getReftbid())){
								String key = (String) value.get(tp.getFieldname().toLowerCase()) ;
								String orgi = (String) value.get("orgi") ;
								if(!StringUtils.isBlank(key) && !StringUtils.isBlank(orgi)) {
				            		DataExchangeInterface exchange = (DataExchangeInterface) UKDataContext.getContext().getBean(tp.getReftbid()) ;
				            		Object refvalue = exchange.getDataByIdAndOrgi(key, orgi) ;
				            		if(refvalue!=null) {
				            			cell2.setCellValue(new XSSFRichTextString(refvalue.toString()));
				            		}
								}
							}else{
								boolean writed = false ;
								if(!StringUtils.isBlank(String.valueOf(value.get("distype")))){
									if(value.get("disphonenum")!=null && value.get("disphonenum").equals(value.get(tp.getFieldname()))){
										cell2.setCellValue(new XSSFRichTextString(UKTools.processSecField(String.valueOf(value.get(tp.getFieldname().toLowerCase())),String.valueOf(value.get("distype")))));
										writed = true ;
									}
								}
								if(writed == false){
									if(!StringUtils.isBlank(tp.getPlugin())) {
										if(tp.getPlugin().equals("sectime") && String.valueOf(value.get(tp.getFieldname().toLowerCase())).matches("[\\d]{1,}")) {
											int sectime = (int)Long.parseLong(String.valueOf(value.get(tp.getFieldname().toLowerCase()))) ;
											cell2.setCellValue(new XSSFRichTextString(new UCKeFuTime(0,0,sectime).toString())) ;
										}else if(tp.getPlugin().equals("mintime") && String.valueOf(value.get(tp.getFieldname().toLowerCase())).matches("[\\d]{1,}")) {
											int mintime = (int)Long.parseLong(String.valueOf(value.get(tp.getFieldname().toLowerCase())))/1000 ;
											cell2.setCellValue(new XSSFRichTextString(new UCKeFuTime(0,0,(int)mintime).toString())) ;
										}
									}else {
										cell2.setCellValue(new XSSFRichTextString(String.valueOf(value.get(tp.getFieldname().toLowerCase()))));
									}
								}
								//特别标注创建人姓名
								if("creater".equals(tp.getFieldname().toLowerCase())){
									UserRepository userRepository = UKDataContext.getContext().getBean(UserRepository.class);
									User user = userRepository.findByIdAndOrgi(value.get(tp.getFieldname().toLowerCase()).toString(), value.get("orgi").toString());
									if(user != null){
										cell2.setCellValue(new XSSFRichTextString(user.getUname()));
									}
								}
							}
						}
						
						if("qualityuser".equals(tp.getFieldname().toLowerCase())){
							User user = null;
							if (value.get(tp.getFieldname().toLowerCase()) != null) {
								UserRepository userRepository = UKDataContext.getContext().getBean(UserRepository.class);
								user = userRepository.findByIdAndOrgi(value.get(tp.getFieldname().toLowerCase()).toString(), value.get("orgi").toString());
							}
							cell2.setCellValue(new XSSFRichTextString(user!=null?user.getUname():""));
							sheet.setColumnWidth(cols, 10*256);
							Cell cell3 = row2.createCell(cols++); 
							cell3.setCellStyle(cellStyle); 
							cell3.setCellValue(new XSSFRichTextString(user!=null?user.getUsername():""));
							sheet.setColumnWidth(cols, 10*256);
						}
						if("userid".equals(tp.getFieldname().toLowerCase())){
							User user = null;
							if (value.get(tp.getFieldname().toLowerCase()) != null) {
								UserRepository userRepository = UKDataContext.getContext().getBean(UserRepository.class);
								user = userRepository.findByIdAndOrgi(value.get(tp.getFieldname().toLowerCase()).toString(), value.get("orgi").toString());
							}
							cell2.setCellValue(new XSSFRichTextString(user!=null?user.getUname():""));
							sheet.setColumnWidth(cols, 10*256);
							Cell cell3 = row2.createCell(cols++); 
							cell3.setCellStyle(cellStyle); 
							cell3.setCellValue(new XSSFRichTextString(user!=null?user.getUsername():""));
							sheet.setColumnWidth(cols, 10*256);
						}
					}
				}
				
				for(TableProperties tp : table.getTableproperty()){
					if(tp.isUnresize() == true) {
						String json = (String) value.get(tp.getFieldname()) ;
						if(!StringUtils.isBlank(json)) {
							List<SysDic> dicList = UKeFuDic.getInstance().getDic("com.dic.xiaoe.input") ;
							@SuppressWarnings("unchecked")
							Map<String, String> values = UKTools.toObject(json,HashMap.class) ;
							for(SysDic sysDic : dicList) {
								Cell cell2 = row2.createCell(cols++); 
								cell2.setCellStyle(cellStyle); 
								List<SysDic> subDicList = UKeFuDic.getInstance().getDic("com.dic.xiaoe.input", sysDic.getId()) ;
								if(subDicList!=null && subDicList.size() > 0) {
									for(SysDic subDic : subDicList) {
										if(values.get(sysDic.getCode()).equals(subDic.getCode()) || values.get(sysDic.getCode()).equals(subDic.getId())) {
											cell2.setCellValue(new XSSFRichTextString(subDic.getName()));
										}
									}
								}else {
									cell2.setCellValue(new XSSFRichTextString(values.get(sysDic.getCode())));
								}
							}
						}
					}
				}
				if(tempExportDatas.size() > 0) {
					for(ExportData expData : tempExportDatas) {
						for(int i=0 ; i<expData.getMaxcols() && (cols + i)<256 ; i++) {
							if(titleRow.getCell(cols + i) == null) {
								Cell title = titleRow.createCell(cols + i); 
								title.setCellStyle(firstStyle); 
								title.setCellValue(new XSSFRichTextString(expData.getTp().getName()));
							}
						}
						
						for(String itemValue : expData.getValues()) {
							if(cols < 255) {
								Cell cell2 = row2.createCell(cols++);
								cell2.setCellValue(new XSSFRichTextString(itemValue));
							}
						}
					}
				}
				if (qcitemidsMap != null && qcitemidsMap.size() > 0 && qcitemidsMap.get(this.tpid)!=null && qcitemidsMap.get(this.tpid).size() > 0) {
					for(int i=0 ; i<qcitemidsMap.get(this.tpid).size(); i++){
						Cell cell2 = row2.createCell(cols++); 
						cell2.setCellStyle(cellStyle); 
						cell2.setCellValue(new XSSFRichTextString(value.get(qcitemidsMap.get(this.tpid).get(i))!=null?value.get(qcitemidsMap.get(this.tpid).get(i)).toString():""));
					}
				}
				
				
				Cell cellpass = row2.createCell(cols++); 
				cellpass.setCellStyle(cellStyle); 
				cellpass.setCellValue(new XSSFRichTextString(value.get("qcResult_pass")!=null?value.get("qcResult_pass").toString():""));
				
				Cell cellscore = row2.createCell(cols++); 
				cellscore.setCellStyle(cellStyle); 
				cellscore.setCellValue(new XSSFRichTextString(value.get("qcResult_score")!=null?value.get("qcResult_score").toString():""));
				
				Cell cellre = row2.createCell(cols++); 
				cellre.setCellStyle(cellStyle); 
				cellre.setCellValue(new XSSFRichTextString(value.get("qcResult_remarks")!=null?value.get("qcResult_remarks").toString():""));
				
				Cell cellad = row2.createCell(cols++); 
				cellad.setCellStyle(cellStyle); 
				cellad.setCellValue(new XSSFRichTextString(value.get("qcResult_adcom")!=null?value.get("qcResult_adcom").toString():""));
				
				Cell cellqc = row2.createCell(cols++); 
				cellqc.setCellStyle(cellStyle); 
				cellqc.setCellValue(new XSSFRichTextString(value.get("qcResult_qacom")!=null?value.get("qcResult_qacom").toString():""));
				
				Cell cellim = row2.createCell(cols++); 
				cellim.setCellStyle(cellStyle); 
				cellim.setCellValue(new XSSFRichTextString(value.get("qcResult_imcom")!=null?value.get("qcResult_imcom").toString():""));
				rowNum ++ ;
			}
		}
		sheet.createRow(rowNum++);
	}
	
	
	private CellStyle baseCellStyle(){
		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER); 

		cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER); 
				
		cellStyle.setWrapText(true);
		cellStyle.setBorderTop(XSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderRight(XSSFCellStyle.BORDER_THIN);
		cellStyle.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		
		Font font = wb.createFont(); 
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD); 
		//font.setFontName("宋体"); 
		font.setFontHeight((short) 200); 
		cellStyle.setFont(font); 
		
		return cellStyle;
	}
}
