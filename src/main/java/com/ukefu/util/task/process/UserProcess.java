package com.ukefu.util.task.process;

import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.web.model.User;

public class UserProcess implements JPAProcess{
	
	private UserRepository userRepository ;
	
	public UserProcess(UserRepository userRepository){
		this.userRepository = userRepository ;
	}

	@Override
	public void process(Object data) {
		userRepository.save((User)data) ;
	}

	@Override
	public void end() {
		
	}

}
