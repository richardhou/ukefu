package com.ukefu.util.task.process;

import com.ukefu.webim.service.repository.BlackListRepository;
import com.ukefu.webim.web.model.BlackEntity;

public class BlackListProcess implements JPAProcess{
	
	private BlackListRepository blackListRepository ;
	
	public BlackListProcess(BlackListRepository blackListRepository){
		this.blackListRepository = blackListRepository ;
	}

	@Override
	public void process(Object data) {
		blackListRepository.save((BlackEntity)data) ;
	}

	
	@Override
	public void end() {
		
	}

}
