package com.ukefu.util.sms;

import com.ukefu.util.es.UKDataBean;
import com.ukefu.util.event.UserEvent;

public class CommandSms implements UserEvent{
	private String mobile;
	private String smsid;
	private UKDataBean uKDataBean;
	private String type;
	private String orgi ;
	private String userid ;
	public CommandSms(String mobile, String smsid, UKDataBean uKDataBean ,String type , String orgi) {
		super();
		this.mobile = mobile;
		this.smsid = smsid;
		this.uKDataBean = uKDataBean;
		this.type = type;
		this.orgi = orgi ;
	}
	public CommandSms(String mobile, String smsid, UKDataBean uKDataBean ,String type , String orgi,String userid) {
		super();
		this.mobile = mobile;
		this.smsid = smsid;
		this.uKDataBean = uKDataBean;
		this.type = type;
		this.orgi = orgi ;
		this.userid = userid;
	}
	public CommandSms() {
		super();
	}
	private static final long serialVersionUID = 1L;
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getSmsid() {
		return smsid;
	}
	public void setSmsid(String smsid) {
		this.smsid = smsid;
	}
	public UKDataBean getuKDataBean() {
		return uKDataBean;
	}
	public void setuKDataBean(UKDataBean uKDataBean) {
		this.uKDataBean = uKDataBean;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
}
