package com.ukefu.util.tts;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.FileCopyUtils;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.UKTools;
import com.ukefu.util.tts.xunfei.XunFeiWebTTSUtils;
import com.ukefu.webim.web.model.SystemConfig;

public class TTSUtil {
	
	@SuppressWarnings("static-access")
	public static boolean doTTS(String text,String fileName,String path,String type ,String id) throws Exception{
		boolean success=false;
		SystemConfig config = UKTools.getSystemConfig();
		if(!StringUtils.isBlank(config.getTtsservice())){
			if(config.getTtsservice().equals(UKDataContext.VoiceServiceType.XUNFEI.toString())){
//				XunFeiWebTTS xunFeiWebTTS = new XunFeiWebTTS();
//		    	xunFeiWebTTS.setTEXT(text);
//		    	xunFeiWebTTS.setAPPID(config.getXunfeittsappid());
//		    	xunFeiWebTTS.setAPI_KEY(config.getXunfeittsapikey());
//		    	
//		        Map<String, Object> resultMap = xunFeiWebTTS.speechSynthesis();
//		        ttsVoiceFileSave(resultMap,fileName,path,type);
				
				if(!StringUtils.isBlank(config.getXunfeittsappid()) && !StringUtils.isBlank(config.getXunfeittsapisecret())
						&& !StringUtils.isBlank(config.getXunfeittsapikey()) && !StringUtils.isBlank(config.getXunfeittsvoicename())) {
					
					XunFeiWebTTSUtils xunFeiWebTTSUtils = new XunFeiWebTTSUtils();
					xunFeiWebTTSUtils.setAppid(config.getXunfeittsappid());
					xunFeiWebTTSUtils.setApiSecret(config.getXunfeittsapisecret());
					xunFeiWebTTSUtils.setApiKey(config.getXunfeittsapikey());
					xunFeiWebTTSUtils.setVcn(config.getXunfeittsvoicename());
					xunFeiWebTTSUtils.setText(text);
					xunFeiWebTTSUtils.setPath(path);
					xunFeiWebTTSUtils.setType(type);
					xunFeiWebTTSUtils.setId(id);
					xunFeiWebTTSUtils.getTTS();
				}
		        success = true;
			}
			if(config.getTtsservice().equals(UKDataContext.VoiceServiceType.BAIDU.toString())){
				//TODO
			}
			if(config.getTtsservice().equals(UKDataContext.VoiceServiceType.ALIYUN.toString())){
				//TODO
			}
			if(config.getTtsservice().equals(UKDataContext.VoiceServiceType.TENCENT.toString())){
				//TODO
			}
		}
		return success;
	}
	
	private static void ttsVoiceFileSave(Map<String, Object> resultMap , String fileName,String path,String type) throws  IOException{
        if(resultMap!=null && fileName.length() > 0 && !UKTools.INSECURE_URI.matcher(fileName).matches()){
            File logoDir = new File(path , type);
            if(!logoDir.exists()){
                logoDir.mkdirs() ;
            }
            FileCopyUtils.copy((byte[])resultMap.get("body"), new File(path , fileName));
        }
    }

}
