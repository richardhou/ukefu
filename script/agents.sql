/*
Navicat MySQL Data Transfer

Source Server         : LocalHost
Source Server Version : 50727
Source Host           : localhost:3306
Source Database       : uckefu

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2020-07-29 21:35:25
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `agents`
-- ----------------------------
DROP TABLE IF EXISTS `agents`;
CREATE TABLE `agents` (
  `name` varchar(255) DEFAULT NULL,
  `system` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `max_no_answer` int(11) NOT NULL DEFAULT '0',
  `wrap_up_time` int(11) NOT NULL DEFAULT '0',
  `reject_delay_time` int(11) NOT NULL DEFAULT '0',
  `busy_delay_time` int(11) NOT NULL DEFAULT '0',
  `no_answer_delay_time` int(11) NOT NULL DEFAULT '0',
  `last_bridge_start` int(11) NOT NULL DEFAULT '0',
  `last_bridge_end` int(11) NOT NULL DEFAULT '0',
  `last_offered_call` int(11) NOT NULL DEFAULT '0',
  `last_status_change` int(11) NOT NULL DEFAULT '0',
  `no_answer_count` int(11) NOT NULL DEFAULT '0',
  `calls_answered` int(11) NOT NULL DEFAULT '0',
  `talk_time` int(11) NOT NULL DEFAULT '0',
  `ready_time` int(11) NOT NULL DEFAULT '0',
  `external_calls_count` int(11) NOT NULL DEFAULT '0'
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of agents
-- ----------------------------
INSERT INTO `agents` VALUES ('1008', 'single_box', null, 'callback', '[call_timeout=120]user/1008', 'On Break', 'Waiting', '3', '10', '10', '60', '0', '0', '0', '0', '1593689411', '0', '0', '0', '0', '0');
INSERT INTO `agents` VALUES ('1009', 'single_box', null, 'callback', '[call_timeout=120]user/1009', 'On Break', 'Waiting', '3', '10', '10', '60', '0', '0', '0', '0', '1593689411', '0', '0', '0', '0', '0');
INSERT INTO `agents` VALUES ('1010', 'single_box', null, 'callback', '[call_timeout=120]user/1010', 'On Break', 'Waiting', '3', '10', '10', '60', '0', '0', '0', '0', '1593689411', '0', '0', '0', '0', '0');
INSERT INTO `agents` VALUES ('1006', 'single_box', null, 'callback', '[call_timeout=120]user/1006', 'On Break', 'Waiting', '3', '10', '10', '60', '0', '0', '0', '0', '1593689411', '0', '0', '0', '0', '0');
INSERT INTO `agents` VALUES ('1007', 'single_box', null, 'callback', '[call_timeout=120]user/1007', 'On Break', 'Waiting', '3', '10', '10', '60', '0', '0', '0', '0', '1593689411', '0', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `aliases`
-- ----------------------------
DROP TABLE IF EXISTS `aliases`;
CREATE TABLE `aliases` (
  `sticky` int(11) DEFAULT NULL,
  `alias` varchar(128) DEFAULT NULL,
  `command` varchar(255) DEFAULT NULL,
  `hostname` varchar(256) DEFAULT NULL,
  KEY `alias1` (`alias`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aliases
-- ----------------------------

-- ----------------------------
-- Table structure for `calls`
-- ----------------------------
DROP TABLE IF EXISTS `calls`;
CREATE TABLE `calls` (
  `call_uuid` varchar(255) DEFAULT NULL,
  `call_created` varchar(128) DEFAULT NULL,
  `call_created_epoch` int(11) DEFAULT NULL,
  `caller_uuid` varchar(256) DEFAULT NULL,
  `callee_uuid` varchar(256) DEFAULT NULL,
  `hostname` varchar(256) DEFAULT NULL,
  KEY `callsidx1` (`hostname`),
  KEY `eruuindex` (`caller_uuid`,`hostname`),
  KEY `eeuuindex` (`callee_uuid`),
  KEY `eeuuindex2` (`call_uuid`),
  KEY `calls1` (`hostname`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of calls
-- ----------------------------

-- ----------------------------
-- Table structure for `channels`
-- ----------------------------
DROP TABLE IF EXISTS `channels`;
CREATE TABLE `channels` (
  `uuid` varchar(256) DEFAULT NULL,
  `direction` varchar(32) DEFAULT NULL,
  `created` varchar(128) DEFAULT NULL,
  `created_epoch` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `state` varchar(64) DEFAULT NULL,
  `cid_name` varchar(255) DEFAULT NULL,
  `cid_num` varchar(256) DEFAULT NULL,
  `ip_addr` varchar(256) DEFAULT NULL,
  `dest` varchar(255) DEFAULT NULL,
  `application` varchar(128) DEFAULT NULL,
  `application_data` varchar(255) DEFAULT NULL,
  `dialplan` varchar(128) DEFAULT NULL,
  `context` varchar(128) DEFAULT NULL,
  `read_codec` varchar(128) DEFAULT NULL,
  `read_rate` varchar(32) DEFAULT NULL,
  `read_bit_rate` varchar(32) DEFAULT NULL,
  `write_codec` varchar(128) DEFAULT NULL,
  `write_rate` varchar(32) DEFAULT NULL,
  `write_bit_rate` varchar(32) DEFAULT NULL,
  `secure` varchar(64) DEFAULT NULL,
  `hostname` varchar(256) DEFAULT NULL,
  `presence_id` varchar(255) DEFAULT NULL,
  `presence_data` varchar(255) DEFAULT NULL,
  `accountcode` varchar(256) DEFAULT NULL,
  `callstate` varchar(64) DEFAULT NULL,
  `callee_name` varchar(255) DEFAULT NULL,
  `callee_num` varchar(256) DEFAULT NULL,
  `callee_direction` varchar(5) DEFAULT NULL,
  `call_uuid` varchar(256) DEFAULT NULL,
  `sent_callee_name` varchar(255) DEFAULT NULL,
  `sent_callee_num` varchar(256) DEFAULT NULL,
  `initial_cid_name` varchar(255) DEFAULT NULL,
  `initial_cid_num` varchar(256) DEFAULT NULL,
  `initial_ip_addr` varchar(256) DEFAULT NULL,
  `initial_dest` varchar(255) DEFAULT NULL,
  `initial_dialplan` varchar(128) DEFAULT NULL,
  `initial_context` varchar(128) DEFAULT NULL,
  KEY `chidx1` (`hostname`),
  KEY `uuindex` (`uuid`,`hostname`),
  KEY `uuindex2` (`call_uuid`),
  KEY `uuid1` (`uuid`),
  KEY `channels1` (`hostname`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of channels
-- ----------------------------

-- ----------------------------
-- Table structure for `complete`
-- ----------------------------
DROP TABLE IF EXISTS `complete`;
CREATE TABLE `complete` (
  `sticky` int(11) DEFAULT NULL,
  `a1` varchar(128) DEFAULT NULL,
  `a2` varchar(128) DEFAULT NULL,
  `a3` varchar(128) DEFAULT NULL,
  `a4` varchar(128) DEFAULT NULL,
  `a5` varchar(128) DEFAULT NULL,
  `a6` varchar(128) DEFAULT NULL,
  `a7` varchar(128) DEFAULT NULL,
  `a8` varchar(128) DEFAULT NULL,
  `a9` varchar(128) DEFAULT NULL,
  `a10` varchar(128) DEFAULT NULL,
  `hostname` varchar(256) DEFAULT NULL,
  KEY `complete1` (`a1`,`hostname`),
  KEY `complete2` (`a2`,`hostname`),
  KEY `complete3` (`a3`,`hostname`),
  KEY `complete4` (`a4`,`hostname`),
  KEY `complete5` (`a5`,`hostname`),
  KEY `complete6` (`a6`,`hostname`),
  KEY `complete7` (`a7`,`hostname`),
  KEY `complete8` (`a8`,`hostname`),
  KEY `complete9` (`a9`,`hostname`),
  KEY `complete10` (`a10`,`hostname`),
  KEY `complete11` (`hostname`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `db_data`
-- ----------------------------
DROP TABLE IF EXISTS `db_data`;
CREATE TABLE `db_data` (
  `hostname` varchar(255) DEFAULT NULL,
  `realm` varchar(255) DEFAULT NULL,
  `data_key` varchar(255) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  UNIQUE KEY `dd_data_key_realm` (`data_key`,`realm`),
  KEY `dd_realm` (`realm`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_data
-- ----------------------------

-- ----------------------------
-- Table structure for `directory_search`
-- ----------------------------
DROP TABLE IF EXISTS `directory_search`;
CREATE TABLE `directory_search` (
  `hostname` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `full_name_digit` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `first_name_digit` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `last_name_digit` varchar(255) DEFAULT NULL,
  `name_visible` int(11) DEFAULT NULL,
  `exten_visible` int(11) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of directory_search
-- ----------------------------

-- ----------------------------
-- Table structure for `fifo_bridge`
-- ----------------------------
DROP TABLE IF EXISTS `fifo_bridge`;
CREATE TABLE `fifo_bridge` (
  `fifo_name` varchar(255) NOT NULL,
  `caller_uuid` varchar(255) NOT NULL,
  `caller_caller_id_name` varchar(255) DEFAULT NULL,
  `caller_caller_id_number` varchar(255) DEFAULT NULL,
  `consumer_uuid` varchar(255) NOT NULL,
  `consumer_outgoing_uuid` varchar(255) DEFAULT NULL,
  `bridge_start` int(11) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fifo_bridge
-- ----------------------------

-- ----------------------------
-- Table structure for `fifo_callers`
-- ----------------------------
DROP TABLE IF EXISTS `fifo_callers`;
CREATE TABLE `fifo_callers` (
  `fifo_name` varchar(255) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `caller_caller_id_name` varchar(255) DEFAULT NULL,
  `caller_caller_id_number` varchar(255) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fifo_callers
-- ----------------------------

-- ----------------------------
-- Table structure for `fifo_outbound`
-- ----------------------------
DROP TABLE IF EXISTS `fifo_outbound`;
CREATE TABLE `fifo_outbound` (
  `uuid` varchar(255) DEFAULT NULL,
  `fifo_name` varchar(255) DEFAULT NULL,
  `originate_string` varchar(255) DEFAULT NULL,
  `simo_count` int(11) DEFAULT NULL,
  `use_count` int(11) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL,
  `lag` int(11) DEFAULT NULL,
  `next_avail` int(11) NOT NULL DEFAULT '0',
  `expires` int(11) NOT NULL DEFAULT '0',
  `static` int(11) NOT NULL DEFAULT '0',
  `outbound_call_count` int(11) NOT NULL DEFAULT '0',
  `outbound_fail_count` int(11) NOT NULL DEFAULT '0',
  `hostname` varchar(255) DEFAULT NULL,
  `taking_calls` int(11) NOT NULL DEFAULT '1',
  `status` varchar(255) DEFAULT NULL,
  `outbound_call_total_count` int(11) NOT NULL DEFAULT '0',
  `outbound_fail_total_count` int(11) NOT NULL DEFAULT '0',
  `active_time` int(11) NOT NULL DEFAULT '0',
  `inactive_time` int(11) NOT NULL DEFAULT '0',
  `manual_calls_out_count` int(11) NOT NULL DEFAULT '0',
  `manual_calls_in_count` int(11) NOT NULL DEFAULT '0',
  `manual_calls_out_total_count` int(11) NOT NULL DEFAULT '0',
  `manual_calls_in_total_count` int(11) NOT NULL DEFAULT '0',
  `ring_count` int(11) NOT NULL DEFAULT '0',
  `start_time` int(11) NOT NULL DEFAULT '0',
  `stop_time` int(11) NOT NULL DEFAULT '0'
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fifo_outbound
-- ----------------------------

-- ----------------------------
-- Table structure for `group_data`
-- ----------------------------
DROP TABLE IF EXISTS `group_data`;
CREATE TABLE `group_data` (
  `hostname` varchar(255) DEFAULT NULL,
  `groupname` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  KEY `gd_groupname` (`groupname`),
  KEY `gd_url` (`url`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of group_data
-- ----------------------------

-- ----------------------------
-- Table structure for `interfaces`
-- ----------------------------
DROP TABLE IF EXISTS `interfaces`;
CREATE TABLE `interfaces` (
  `type` varchar(128) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `ikey` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `syntax` varchar(10000) DEFAULT NULL,
  `hostname` varchar(256) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `jabber_subscriptions`
-- ----------------------------
DROP TABLE IF EXISTS `jabber_subscriptions`;
CREATE TABLE `jabber_subscriptions` (
  `sub_from` varchar(255) DEFAULT NULL,
  `sub_to` varchar(255) DEFAULT NULL,
  `show_pres` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jabber_subscriptions
-- ----------------------------

-- ----------------------------
-- Table structure for `json_store`
-- ----------------------------
DROP TABLE IF EXISTS `json_store`;
CREATE TABLE `json_store` (
  `name` varchar(255) NOT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of json_store
-- ----------------------------

-- ----------------------------
-- Table structure for `limit_data`
-- ----------------------------
DROP TABLE IF EXISTS `limit_data`;
CREATE TABLE `limit_data` (
  `hostname` varchar(255) DEFAULT NULL,
  `realm` varchar(255) DEFAULT NULL,
  `id` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  KEY `ld_hostname` (`hostname`),
  KEY `ld_uuid` (`uuid`),
  KEY `ld_realm` (`realm`),
  KEY `ld_id` (`id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of limit_data
-- ----------------------------

-- ----------------------------
-- Table structure for `members`
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `queue` varchar(255) DEFAULT NULL,
  `instance_id` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) NOT NULL DEFAULT '',
  `session_uuid` varchar(255) NOT NULL DEFAULT '',
  `cid_number` varchar(255) DEFAULT NULL,
  `cid_name` varchar(255) DEFAULT NULL,
  `system_epoch` int(11) NOT NULL DEFAULT '0',
  `joined_epoch` int(11) NOT NULL DEFAULT '0',
  `rejoined_epoch` int(11) NOT NULL DEFAULT '0',
  `bridge_epoch` int(11) NOT NULL DEFAULT '0',
  `abandoned_epoch` int(11) NOT NULL DEFAULT '0',
  `base_score` int(11) NOT NULL DEFAULT '0',
  `skill_score` int(11) NOT NULL DEFAULT '0',
  `serving_agent` varchar(255) DEFAULT NULL,
  `serving_system` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `system` varchar(255) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of members
-- ----------------------------

-- ----------------------------
-- Table structure for `nat`
-- ----------------------------
DROP TABLE IF EXISTS `nat`;
CREATE TABLE `nat` (
  `sticky` int(11) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `proto` int(11) DEFAULT NULL,
  `hostname` varchar(256) DEFAULT NULL,
  KEY `nat_map_port_proto` (`port`,`proto`,`hostname`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nat
-- ----------------------------

-- ----------------------------
-- Table structure for `recovery`
-- ----------------------------
DROP TABLE IF EXISTS `recovery`;
CREATE TABLE `recovery` (
  `runtime_uuid` varchar(255) DEFAULT NULL,
  `technology` varchar(255) DEFAULT NULL,
  `profile_name` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `metadata` text,
  KEY `recovery1` (`technology`),
  KEY `recovery2` (`profile_name`),
  KEY `recovery3` (`uuid`),
  KEY `recovery4` (`runtime_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recovery
-- ----------------------------

-- ----------------------------
-- Table structure for `registrations`
-- ----------------------------
DROP TABLE IF EXISTS `registrations`;
CREATE TABLE `registrations` (
  `reg_user` varchar(256) DEFAULT NULL,
  `realm` varchar(256) DEFAULT NULL,
  `token` varchar(256) DEFAULT NULL,
  `url` varchar(2000) DEFAULT NULL,
  `expires` int(11) DEFAULT NULL,
  `network_ip` varchar(256) DEFAULT NULL,
  `network_port` varchar(256) DEFAULT NULL,
  `network_proto` varchar(256) DEFAULT NULL,
  `hostname` varchar(256) DEFAULT NULL,
  `metadata` varchar(256) DEFAULT NULL,
  KEY `regindex1` (`reg_user`,`realm`,`hostname`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of registrations
-- ----------------------------

-- ----------------------------
-- Table structure for `sip_authentication`
-- ----------------------------
DROP TABLE IF EXISTS `sip_authentication`;
CREATE TABLE `sip_authentication` (
  `nonce` varchar(255) DEFAULT NULL,
  `expires` bigint(20) DEFAULT NULL,
  `profile_name` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `last_nc` int(11) DEFAULT NULL,
  KEY `sa_nonce` (`nonce`),
  KEY `sa_hostname` (`hostname`),
  KEY `sa_expires` (`expires`),
  KEY `sa_last_nc` (`last_nc`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sip_authentication
-- ----------------------------

-- ----------------------------
-- Table structure for `sip_dialogs`
-- ----------------------------
DROP TABLE IF EXISTS `sip_dialogs`;
CREATE TABLE `sip_dialogs` (
  `call_id` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `sip_to_user` varchar(255) DEFAULT NULL,
  `sip_to_host` varchar(255) DEFAULT NULL,
  `sip_from_user` varchar(255) DEFAULT NULL,
  `sip_from_host` varchar(255) DEFAULT NULL,
  `contact_user` varchar(255) DEFAULT NULL,
  `contact_host` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `direction` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `profile_name` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `presence_id` varchar(255) DEFAULT NULL,
  `presence_data` varchar(255) DEFAULT NULL,
  `call_info` varchar(255) DEFAULT NULL,
  `call_info_state` varchar(255) DEFAULT '',
  `expires` bigint(20) DEFAULT '0',
  `status` varchar(255) DEFAULT NULL,
  `rpid` varchar(255) DEFAULT NULL,
  `sip_to_tag` varchar(255) DEFAULT NULL,
  `sip_from_tag` varchar(255) DEFAULT NULL,
  `rcd` int(11) NOT NULL DEFAULT '0',
  KEY `sd_uuid` (`uuid`),
  KEY `sd_hostname` (`hostname`),
  KEY `sd_presence_data` (`presence_data`),
  KEY `sd_call_info` (`call_info`),
  KEY `sd_call_info_state` (`call_info_state`),
  KEY `sd_expires` (`expires`),
  KEY `sd_rcd` (`rcd`),
  KEY `sd_sip_to_tag` (`sip_to_tag`),
  KEY `sd_sip_from_user` (`sip_from_user`),
  KEY `sd_sip_from_host` (`sip_from_host`),
  KEY `sd_sip_to_host` (`sip_to_host`),
  KEY `sd_presence_id` (`presence_id`),
  KEY `sd_call_id` (`call_id`),
  KEY `sd_sip_from_tag` (`sip_from_tag`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sip_dialogs
-- ----------------------------

-- ----------------------------
-- Table structure for `sip_presence`
-- ----------------------------
DROP TABLE IF EXISTS `sip_presence`;
CREATE TABLE `sip_presence` (
  `sip_user` varchar(255) DEFAULT NULL,
  `sip_host` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `rpid` varchar(255) DEFAULT NULL,
  `expires` bigint(20) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `profile_name` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `network_ip` varchar(255) DEFAULT NULL,
  `network_port` varchar(6) DEFAULT NULL,
  `open_closed` varchar(255) DEFAULT NULL,
  KEY `sp_hostname` (`hostname`),
  KEY `sp_open_closed` (`open_closed`),
  KEY `sp_sip_user` (`sip_user`),
  KEY `sp_sip_host` (`sip_host`),
  KEY `sp_profile_name` (`profile_name`),
  KEY `sp_expires` (`expires`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sip_presence
-- ----------------------------

-- ----------------------------
-- Table structure for `sip_registrations`
-- ----------------------------
DROP TABLE IF EXISTS `sip_registrations`;
CREATE TABLE `sip_registrations` (
  `call_id` varchar(255) DEFAULT NULL,
  `sip_user` varchar(255) DEFAULT NULL,
  `sip_host` varchar(255) DEFAULT NULL,
  `presence_hosts` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `ping_status` varchar(255) DEFAULT NULL,
  `ping_count` int(11) DEFAULT NULL,
  `ping_time` bigint(20) DEFAULT NULL,
  `force_ping` int(11) DEFAULT NULL,
  `rpid` varchar(255) DEFAULT NULL,
  `expires` bigint(20) DEFAULT NULL,
  `ping_expires` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(255) DEFAULT NULL,
  `server_user` varchar(255) DEFAULT NULL,
  `server_host` varchar(255) DEFAULT NULL,
  `profile_name` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `network_ip` varchar(255) DEFAULT NULL,
  `network_port` varchar(6) DEFAULT NULL,
  `sip_username` varchar(255) DEFAULT NULL,
  `sip_realm` varchar(255) DEFAULT NULL,
  `mwi_user` varchar(255) DEFAULT NULL,
  `mwi_host` varchar(255) DEFAULT NULL,
  `orig_server_host` varchar(255) DEFAULT NULL,
  `orig_hostname` varchar(255) DEFAULT NULL,
  `sub_host` varchar(255) DEFAULT NULL,
  KEY `sr_call_id` (`call_id`),
  KEY `sr_sip_user` (`sip_user`),
  KEY `sr_sip_host` (`sip_host`),
  KEY `sr_sub_host` (`sub_host`),
  KEY `sr_mwi_user` (`mwi_user`),
  KEY `sr_mwi_host` (`mwi_host`),
  KEY `sr_profile_name` (`profile_name`),
  KEY `sr_presence_hosts` (`presence_hosts`),
  KEY `sr_contact` (`contact`),
  KEY `sr_expires` (`expires`),
  KEY `sr_ping_expires` (`ping_expires`),
  KEY `sr_hostname` (`hostname`),
  KEY `sr_status` (`status`),
  KEY `sr_ping_status` (`ping_status`),
  KEY `sr_network_ip` (`network_ip`),
  KEY `sr_network_port` (`network_port`),
  KEY `sr_sip_username` (`sip_username`),
  KEY `sr_sip_realm` (`sip_realm`),
  KEY `sr_orig_server_host` (`orig_server_host`),
  KEY `sr_orig_hostname` (`orig_hostname`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sip_registrations
-- ----------------------------

-- ----------------------------
-- Table structure for `sip_shared_appearance_dialogs`
-- ----------------------------
DROP TABLE IF EXISTS `sip_shared_appearance_dialogs`;
CREATE TABLE `sip_shared_appearance_dialogs` (
  `profile_name` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `contact_str` varchar(255) DEFAULT NULL,
  `call_id` varchar(255) DEFAULT NULL,
  `network_ip` varchar(255) DEFAULT NULL,
  `expires` bigint(20) DEFAULT NULL,
  KEY `ssd_profile_name` (`profile_name`),
  KEY `ssd_hostname` (`hostname`),
  KEY `ssd_contact_str` (`contact_str`),
  KEY `ssd_call_id` (`call_id`),
  KEY `ssd_expires` (`expires`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sip_shared_appearance_dialogs
-- ----------------------------

-- ----------------------------
-- Table structure for `sip_shared_appearance_subscriptions`
-- ----------------------------
DROP TABLE IF EXISTS `sip_shared_appearance_subscriptions`;
CREATE TABLE `sip_shared_appearance_subscriptions` (
  `subscriber` varchar(255) DEFAULT NULL,
  `call_id` varchar(255) DEFAULT NULL,
  `aor` varchar(255) DEFAULT NULL,
  `profile_name` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `contact_str` varchar(255) DEFAULT NULL,
  `network_ip` varchar(255) DEFAULT NULL,
  KEY `ssa_hostname` (`hostname`),
  KEY `ssa_network_ip` (`network_ip`),
  KEY `ssa_subscriber` (`subscriber`),
  KEY `ssa_profile_name` (`profile_name`),
  KEY `ssa_aor` (`aor`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sip_shared_appearance_subscriptions
-- ----------------------------

-- ----------------------------
-- Table structure for `sip_subscriptions`
-- ----------------------------
DROP TABLE IF EXISTS `sip_subscriptions`;
CREATE TABLE `sip_subscriptions` (
  `proto` varchar(255) DEFAULT NULL,
  `sip_user` varchar(255) DEFAULT NULL,
  `sip_host` varchar(255) DEFAULT NULL,
  `sub_to_user` varchar(255) DEFAULT NULL,
  `sub_to_host` varchar(255) DEFAULT NULL,
  `presence_hosts` varchar(255) DEFAULT NULL,
  `event` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `call_id` varchar(255) DEFAULT NULL,
  `full_from` varchar(255) DEFAULT NULL,
  `full_via` varchar(255) DEFAULT NULL,
  `expires` bigint(20) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `accept` varchar(255) DEFAULT NULL,
  `profile_name` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `network_port` varchar(6) DEFAULT NULL,
  `network_ip` varchar(255) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `orig_proto` varchar(255) DEFAULT NULL,
  `full_to` varchar(255) DEFAULT NULL,
  KEY `ss_call_id` (`call_id`),
  KEY `ss_multi` (`call_id`,`profile_name`,`hostname`),
  KEY `ss_hostname` (`hostname`),
  KEY `ss_network_ip` (`network_ip`),
  KEY `ss_sip_user` (`sip_user`),
  KEY `ss_sip_host` (`sip_host`),
  KEY `ss_presence_hosts` (`presence_hosts`),
  KEY `ss_event` (`event`),
  KEY `ss_proto` (`proto`),
  KEY `ss_sub_to_user` (`sub_to_user`),
  KEY `ss_sub_to_host` (`sub_to_host`),
  KEY `ss_expires` (`expires`),
  KEY `ss_orig_proto` (`orig_proto`),
  KEY `ss_network_port` (`network_port`),
  KEY `ss_profile_name` (`profile_name`),
  KEY `ss_version` (`version`),
  KEY `ss_full_from` (`full_from`),
  KEY `ss_contact` (`contact`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sip_subscriptions
-- ----------------------------

-- ----------------------------
-- Table structure for `skinny_active_lines`
-- ----------------------------
DROP TABLE IF EXISTS `skinny_active_lines`;
CREATE TABLE `skinny_active_lines` (
  `device_name` varchar(16) DEFAULT NULL,
  `device_instance` int(11) DEFAULT NULL,
  `line_instance` int(11) DEFAULT NULL,
  `channel_uuid` varchar(256) DEFAULT NULL,
  `call_id` int(11) DEFAULT NULL,
  `call_state` int(11) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of skinny_active_lines
-- ----------------------------

-- ----------------------------
-- Table structure for `skinny_buttons`
-- ----------------------------
DROP TABLE IF EXISTS `skinny_buttons`;
CREATE TABLE `skinny_buttons` (
  `device_name` varchar(16) DEFAULT NULL,
  `device_instance` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `label` varchar(40) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `settings` varchar(44) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of skinny_buttons
-- ----------------------------

-- ----------------------------
-- Table structure for `skinny_devices`
-- ----------------------------
DROP TABLE IF EXISTS `skinny_devices`;
CREATE TABLE `skinny_devices` (
  `name` varchar(16) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `instance` int(11) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `max_streams` int(11) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `codec_string` varchar(255) DEFAULT NULL,
  `headset` int(11) DEFAULT NULL,
  `handset` int(11) DEFAULT NULL,
  `speaker` int(11) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of skinny_devices
-- ----------------------------

-- ----------------------------
-- Table structure for `skinny_lines`
-- ----------------------------
DROP TABLE IF EXISTS `skinny_lines`;
CREATE TABLE `skinny_lines` (
  `device_name` varchar(16) DEFAULT NULL,
  `device_instance` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `line_instance` int(11) DEFAULT NULL,
  `label` varchar(40) DEFAULT NULL,
  `value` varchar(24) DEFAULT NULL,
  `caller_name` varchar(44) DEFAULT NULL,
  `ring_on_idle` int(11) DEFAULT NULL,
  `ring_on_active` int(11) DEFAULT NULL,
  `busy_trigger` int(11) DEFAULT NULL,
  `forward_all` varchar(255) DEFAULT NULL,
  `forward_busy` varchar(255) DEFAULT NULL,
  `forward_noanswer` varchar(255) DEFAULT NULL,
  `noanswer_duration` int(11) DEFAULT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of skinny_lines
-- ----------------------------

-- ----------------------------
-- Table structure for `tasks`
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `task_id` int(11) DEFAULT NULL,
  `task_desc` varchar(255) DEFAULT NULL,
  `task_group` varchar(255) DEFAULT NULL,
  `task_runtime` bigint(20) DEFAULT NULL,
  `task_sql_manager` int(11) DEFAULT NULL,
  `hostname` varchar(256) DEFAULT NULL,
  KEY `tasks1` (`hostname`,`task_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `tiers`
-- ----------------------------
DROP TABLE IF EXISTS `tiers`;
CREATE TABLE `tiers` (
  `queue` varchar(255) DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=MEMORY DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `voicemail_msgs`
-- ----------------------------
DROP TABLE IF EXISTS `voicemail_msgs`;
CREATE TABLE `voicemail_msgs` (
  `created_epoch` int(11) DEFAULT NULL,
  `read_epoch` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `cid_name` varchar(255) DEFAULT NULL,
  `cid_number` varchar(255) DEFAULT NULL,
  `in_folder` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `message_len` int(11) DEFAULT NULL,
  `flags` varchar(255) DEFAULT NULL,
  `read_flags` varchar(255) DEFAULT NULL,
  `forwarded_by` varchar(255) DEFAULT NULL,
  KEY `voicemail_msgs_idx1` (`created_epoch`),
  KEY `voicemail_msgs_idx2` (`username`),
  KEY `voicemail_msgs_idx3` (`domain`),
  KEY `voicemail_msgs_idx4` (`uuid`),
  KEY `voicemail_msgs_idx5` (`in_folder`),
  KEY `voicemail_msgs_idx6` (`read_flags`),
  KEY `voicemail_msgs_idx7` (`forwarded_by`),
  KEY `voicemail_msgs_idx8` (`read_epoch`),
  KEY `voicemail_msgs_idx9` (`flags`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of voicemail_msgs
-- ----------------------------

-- ----------------------------
-- Table structure for `voicemail_prefs`
-- ----------------------------
DROP TABLE IF EXISTS `voicemail_prefs`;
CREATE TABLE `voicemail_prefs` (
  `username` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `name_path` varchar(255) DEFAULT NULL,
  `greeting_path` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  KEY `voicemail_prefs_idx1` (`username`),
  KEY `voicemail_prefs_idx2` (`domain`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of voicemail_prefs
-- ----------------------------
